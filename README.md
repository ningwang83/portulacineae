### The detailed procedure of the Portulacineae project 

## Sample selection and Transcriptome generation

* Sampled groups: 
	* Ingroup **Portulacineae - Halophytaceae**:
		- Montiaceae (9)
		- Basellaceae (2)
		- Didiereaceae (9)
		- Talinaceae (3)
		- Portulacaceae (8)
		- Anacampserotaceae (3)
		- Cactaceae (34)
	* Outgroups: 
		- Amaranthaceae (_Beta Vulgaris_)
		- Limeaceae (_Limeum aethiopicum_)
		- Stegnospermataceae (_Stegnosperma halimifolium_)
		- Nyctaginaceae (_Guapira obtusata_)
		- Phytolaccaceae (_Anisomeria littoralis_)
		- Aizoaceae (2)
		- Molluginaceae (7)

* RNA extraction method:
	*  Ambion PureLink Plant RNA Reagent (ThermoFisher Scientific Inc, Waltham, MA, USA)
	*  TRIzol® Reagent (Life Technologies, Thermo Fisher Scientific, Waltham, MA, USA)
* Quantification of RNA:
	* Agilent 2100 Bioanalyzer (Agilent Technologies, Santa Clara, CA, USA). 
* Librarie preparation:
	* KAPA Stranded mRNA Library Preparation Kit (KAPA Biosystems, Wilmington, MA, USA)
	* KAPA mRNA HyperPrep Kit (KK8580, 2017/09)
	* TruSeq Stranded mRNA Sample Prep Kit (Illumina Inc., San Diego, CA, USA, before 2016)
* Sequencing:
	* Illumina HiSeq platform (HiSeq2000, or HiSeq2500, 10 samples/lane, pair-ended)
	* **Note:** HiSeq4000 and HiSeqX will result more errors, not good for _de novo_ assembly

## Error correction and Sequence Assembly

* Visualize sequence quality:
	* FastQC: put all your *.fq.gz file in the same directory with fastqc, then run  
	`./fastqc -t 5 *.faq.gz` # -t is the number of thereads.
	* this will generate *.html files that can be viewed in a browser.
* Use `reads_calculate.py` to check out the number of lines for each *.fq.gz file.  
   **lines/4** is the number of sequences  
   `python reads_calculate.py DIRlist file_end` #DIRlist is DIRs separated by ","
* Error correction:
	* Rcorrector (a kmer-based error correction method for RNA-seq data, Song and Florea 2015).
	* **Note** Rcorrector is not necessary if you latter trim the sequences
* Low quality trimming and assemly of reads
	* build-in Trimmomatic in Trinity v2.2.0 (Grabherr et al., 2011)
	* `Illuminaclip 2:30:10 sliding window 4:5, leading 5, trailing 5 and min length 25`
	* 99.7% reads were recovered
	* Trinity to assemble pair-ended reads
	* ~250k contigs were generated for each sample with N50 to be ~1500bp

**use `assemble.py` to trim and assemble reads automatically**:

`python PATH/assemble.py DIRlist num_CPU max_memory_GB Num_thread file_end`

**Note**: DIRlist is a list of DIR separated by ",", each DIR refer to one species and contain all `\*.fq.fz` or `\*.fastq.gz` files of this species. 

## Use Transdecoder to translate transcriptomes and return ORF

1. Make concatenated proteome database using _Arabidopsis thaliana_ and _Beta vulgaris_ proteome (Dohm et al., 2014) [~/Repo/phylogenomic.../data/blastp_database/]
	* download proteins from EnsemblPlants, save as Arabidopsis\_thaliana.fasta and Beta\_vulgaris.fasta
	* `cat *.fasta > plant_proteome.db`
	* `makeblastdb -in plant_proteome.db -parse_seqids -dbtype prot -out plant_proteome.db`  
2. Use Transdecoder v2.0 (Haas et al., 2013) find cds and pep for each species
	* `TransDecoder.LongOrfs -t *.Trinity.fasta -S`
	* `blastp -query PATH/*.Trinity.fasta.transdecoder_dir/longest_orfs.pep -db PATH/plant_proteome.db -max_target_seqs 1 -outfmt 6 -evalue 10 -num_threads 9 > taxonID.blastp.outfmt6`  
	* `TransDecoder.Predict -t PATH/*.Trinity.fasta --retain_blastp_hits PATH/taxonID.blastp.outfmt6 --cpu 9`
	* **use `run_TransDecoder.py` run multiple .Trinity.fasta file automatically**
		- put all *.Trinity.fasta file into a single directory
		- `python run_TransDecoder.py WORKDIR file_ending Num_thread evalue Num_cpu`
		- This will generate `taxonID.Trinity.fasta.transdecoder.cds` and `.pep` files
3. Fix and check names from TransDecoder
	* `python fix_check_names.py inDIR outDIR file_ending`
	* This will result in `taxonID.pep.fa` and `taxonID.cds.fa` files. 
	* the sequence name is like `>taxonID@seqID`
	
## Reduce redundancy with (CD-HIT v4.6) & All-by-all Blastn/p

* `cd-hit -i taxonID.fa.pep -o taxonID.fa.cdhit -c 0.995 -n 5 -T 9`
* `cd-hit-est -i taxonID.fa.cds -o code.fa.cds.cdhitest -c 0.99 -n 10 -r 0 -T 9`

**for peptide** (conduct blastp one after one)

* `cat *.cdhit > pepall.fa`
* `makeblastdb -in pepall.fa -parse_seqids -dbtype prot -out pepall.fa`
* `blastp -db PATH/pepall.fa -query PATH/taxonID.fa.cdhit ...`

**for cds** (conduct all-by-all blastn)  
(word-size = 28, if want smaller word-size = 11, put '-task blastn' in blasn command)

* `cat *.cdhitest >cdsall.fa`
* `makeblastdb -in cdsall.fa -parse_seqids -dbtype nucl -out cdsall.fa`

```
blastn -db cdsall.fa -query cdsall.fa -evalue 10 -num_threads <num_cores> -max_target_seqs 1000 -out cdsall.rawblast -outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'

```

**Use `python cdhit_blast.py WORKDIR num_threads datatype(pep/cds)` to automate the above step**  

## Homology Inference through Cluster using mcl and phylogenetic filtering  

* Obtaining putative homolog groups using **Markov clustering (MCL 14-137, Van Dongen 2000)**

 `python Ya/blast_to_mcl.py cdsall.rawblast <hit_fraction_cutoff>`  
 "hit fraction cutoff = 0.4" (hit_fraction = query/subject coverage divided by query/subject length) 
 
* the resulting file is "cdsall.rawblast.hit-frac0.4.minusLogEvalue"  

	       CFYBgtRC@140090    YYgtRC@52302    81.0457574906  
	       CFYBgtRC@140090    LJXtRC@87910    42.6989700043  
	       CFYBgtRC@134059    XYgtRC@91075    94.2218487496  
	       CFYBgtRC@21464    CWgtRC@129692    97.0457574906  
	       
* using MCL to cluster potential homologs:

`mcl cdsall.rawblast.hit-frac0.4.minusLogEvalue --abc -te 5 -tf 'gq(5)' -I 1.4 -o hit-frac0.4_I1.4_e5`  
 **Note:** one line is one cluster in `hit-frac0.4_I1.4_e5` file.

* **use `mclhitcluster2fasta.py` to extract clusters and output fastafile**
	- open `mclhitcluster2fasta.py`
	- set up clade list for each family with species ID
	- change the `if condition` to set up the min number of species included in each family  
	E.g., [Cacnum >= **minimal_taxa** and outnum >= 1 and Montnum >= 7 and Portnum >= 2 and Talnum >= 2 and Mollnum >= 2 and Didinum >= 7 and Ananum >= 1]. 
	This will retrieve 26 Cactaceae, seven Montiaceae, seven Didiereaceae, two of each Portulacaceae, Talinaceae and Molluginaceae, one of each Anacampserotaceae and outgroup. 

	- run `python mclhitcluster2fasta.py cdsall.fa hit-frac0.4_I1.4_e5 minimal_taxa outDIR`
	- In total, 8538 clusters were obtained 

## Make alignment and build raxml tree for each homologs

* Alignment for homologs MAFFT v7 (Katoh and Standley 2013) or Pasta v1.6.4 (Mirarab et al. 2015)
	- \< 1000 sequences: `mafft --genafpair --maxiterate 1000 --thread 9 --nuc/--amino ...`
	- \> 1000 sequences: `run_pasta.py --input= infile --datatype=DNA/Protein`
* Filtering alignments using Phyutility v2.2.6 (Smith and Dunn 2008) 
	- \< 1000 sequences: `phyutility (-aa) -clean 0.1 -in alignment -out alignment_pht`
	- \> 1000 sequences: `phyutility (-aa) -clean 0.05 -in alignment -out alignment_pht`
* Build maximum likelihood (ML) trees (**no iterations!**) [RAxML v8.1.22 (Stamatakis 2015)]
	- \< 1000 sequences: `raxml -T 9 -p 1234 -s input_cln -n output -m GTRCAT/PROTCATWAG -w Pathway`
	- \> 1000 sequences: `fasttree --quiet (-nt -gtr)/(-wag) input_cln` #[FastTree v2.1.7]
	
* **Automate from mcl clustered fasta file to raxml tree**  
  put all your cleaned alignments into one directory and run:  
	`python cluster_totree.py DIR num_cores seqtype(dna/aa) bootstrap(y/n)`
	
## Trim long external and internal branches (Filter tree)

* Check the termial and internal branch length  
 `python brlength_distribution.py DIR file_end leftcutoff rightcutoff inter/tip`
* Trim long tip branches and cut long internodes. 
	* `python trim_tips.py <input tree dir> <tree_file_ending> <relative_cutoff> <absolute_cutoff>`  
		- **Note:** Ya's previous code have bug in line 30 `temp,curroot = remove_kink(curroot,curroot)` 
		- relative_cutoff means longer than cutoff and also ten times greater than their sister taxon 
		- output .tt files  
	* `python mask_tips_by_taxonID_transcripts.py <.tt dir> <aln-cln dir> mask_paraphyletic(y)`  
		- mask mono- and para- phyletic group, will chang long internodes to be tip branches
		- removal of possible isoforms, in-paralogs or recent gene duplications
		- keep the sequence with the highest number of continuous characters in the trimmed alignment
		- output .mm files
	* `python cut_long_internal_branches.py <.mm dir> file_end <cutoff> <minimal no. taxa> <outDIR>`
		- output .subtree files
		- subtrees with tip number > 5 will be saved
	* `python trim_tips.py <input tree dir> <tree_file_ending> <relative_cutoff> <absolute_cutoff>`
		- input tree file end with .subtree
		- output .tt files
* write fasta files from trees
	* `python write_fasta_files_from_trees.py PATH/cdsall.fa treDIR tree_file_ending outDIR`
	* generate rr.fa or _rr.fa files
 
**the whole process should be repeated several time.**  
In my case, I repeated three times before extract orthologs: 

1. First round: 
	* trim tips first time: Relative: 0.4, absolute: 0.5
	* mask taxa (both mono and paraphyletic groups)
	* internode cutoff: cutoff: 0.6, min taxa: 5, output Homo0.6_5, **8557 clusters**
	* trim tips second time: Relative: 0.7, absolute: 0.7
	* write fasta using cdsall80.fa, output dir cdsall80_Sec_fasta
2. Second round:
	* trim tips first time: Relative: 0.5, absolute: 0.8
	* mask taxa (both mono and paraphyletic groups)
	* internode cutoff: cutoff: 0.6, min taxa: 5, output Homo0.6_5, **8563 clusters**
	* trim tips second time: Relative: 0.7, absolute: 0.8
	* write fasta using cdsall80.fa, output dir cdsall80_Third_fasta
3. Add two species using **Pyphlawd** (_Anacampseros filamentosa_ and _Basella alba_)
	* (https://github.com/FePhyFoFum/pyphlawd)
	* modify the `conf.py`: `DI = PATH to src of Pyphlawd, takeouttaxondups = False, for blast bits: length_limit = 0.6, evalue_limit = 5, perc_identity = 20`
	* prepare the cds files of the two species (see TransDecoder above)
	* change add_clade_cluster.py: `blast_file_against_db => -max_target_seqs 1000`
	* run `add_internal_seqs_to_clusters.py` one species after another. (1 by 1)
		- e.g. `python add_internal...py PATH/new2speices/ PATH/adding2species/fasta/ logfile`
		- put 8563 clusters \*.fa and \*.aln files into one directory, this is the "outclu/".
		- in add_internal_seqs_to_clusters.py change line 41 to your cds (e.g., "CYTH/Anfi.cds.fas")
		- `python add_internal_seqs_to_clusters.py indir outclu/ logfile`
		- **Note**: indir is the directory contain the cds files of the two species.
			- blast target species’ cds sequences (e.g., CYTH.cds.fa) against each cluster bait (8563.db)
			- **Note**: the database is the cat *.fa of the 8563 clusters generated form above steps
			- added the hits that meet the above criteria to the existing clusters (.fa)
			- merge them into the existing alignments (.aln).
			- 8442/8563 clusters were added in with sequences from the two additional species.
4. Third round:
	* trim tips first time: Relative: 0.5, absolute: 0.7
	* mask taxa (both mono and paraphyletic groups)
	* internode cutoff: cutoff: 0.6, min taxa: 5, output Homo82_0.6_5, **8592 clusters**
	* trim tips second time: Relative: 0.4, absolute: 0.6
	* write fasta using cdsall82.fa, output dir cdsall82_Forth_fasta

## Orthology Inference and Species Tree Estimation

1. Infer orthologs using the rooted tree (RT) method **think about a proper modification**
	* make a taxonIDs file "taxonIDs.txt" (e.g., ID \t Name \n)
	* `python prune_paralogs_RT.py PATH/Homo82_0.6_5/ .tt RT82_ortho_30min_Beta/ 30 taxonIDs.txt`
	* Beta vulgaris was used as an outgroup
	* mininal_ingroup_taxa: 30
	* gene duplication events were inferred from root to tip. 
	* prune smaller clade sides when duplicated taxa (>2) are found between the two sides of a bifurcating node.
2. Examine the taxon occupancy statistics of ortholog groups and extract ortholog sequences
	* `python ortholog_occupancy_stats.py RT82_ortho_30min_Beta`
		- R script to output a pdf file for taxon occupancy statistics.
		
		``` 
		a <- as.numeric(read.table("ortho_stats")[,1])
		a <- sort(a, decreasing=TRUE)
		pdf(file="taxon_occupancy.pdf")
		plot(a, type="l", lwd=3, ylab="Number of Taxa in Each Ortholog")
		dev.off()
		```
	* `python ~/Documents/Transcriptome_bird/Transcriptome_code/write_ortholog_fasta_files.py ../../cdsall82.fa . RT82_76min_fasta/ 76`
	* this result in 1237 orthologs
	* `adding2species/cln/Homo82_0.6_5/RT82_ortho_30min_Beta$ mv RT82_76min_fasta/ ../../..`

3. Filtering the orthologs
	* aligned sequences in MAFFT v7 (genafpair –maxiterate 1000) and filter with phyutility (-clean 0.3)
		- `python mafft_wrapper.py DIR num_cores seqtype(dna/aa)`
		- `python phyutility_wrapper.py DIR min_column_occupancy dna/aa`
	* run raxml to build ML tree (RAxML v8.1.22 with GTR+GAMMA model)
		- `python raxml_wrapper.py DIR number_cores dna/aa` (make sure \*.aln_cln is in the PATH)
	* remove taxa with terminal branch length longer than 0.1 and 10 times greater than sister clade
		- taxa removed from the original sequence file
		- `python ortho_brsearch_correct.py INDIR fastaDIR file_end leftcutoff(max_brlen)`
		- 58 out of the 1237 orthologous sequence files were modified (file_end with: .edit.fa)
		- `python move_file.py inDIR outDIR .edit.fa` # move the modified clusters to a new outDIR
		- `mv outDIR/*.edit.fa inDIR` # move the .edit.fa back to the cluster DIR.

4. Construct supermatix and build trees
	* Prank v.140110 (Löytynoja and Goldman, 2008) to align with default parameters
		- `python prank_wrapper.py <inDIR> <outDIR> <file ending> DNA/aa`
	* Phyutility (-clean 0.3) to filter alignments.
		- `python phyutility_wrapper.py <inDIR> <MIN_COLUMN_OCCUPANCY> DNA/aa`
	* Concatenate ortholog alignments and build concatenated ML tree. 
		- python concatenate_matrices.py <aln-clnDIR> <numofsites> <numoftaxa> DNA/aa <outfile>
		- numofsites = 500 numoftaxa = 77
		- 841 orthologs passed the filter (see RT82_77_500.concat.model)
		- `raxml -T 9 -f a -x 12345 -# 200 -p 12345 -m GTRCAT -q RT82_77_500.concat.model...`
		- result tree is "RAxML_bestTree.RT82_77_500"
		- move these 841 orthologs into a new directory for Astral analyses
	* run raxml for each orthologs and conduct ASTRAL 4.10.12 (Mirarab et al. 2014). 
		- `python raxml_runortho.py DIR number_cores dna/aa (_cln in PATH)`
		- GTRCAT replicates=200
		- nano a file named "astral", with the following line in it and put it in /usr/bin
		- `java -Xmx9g -jar /home/PATH/astral.jar $*`, make it excutable: `chmod +x astral`
		- cat all best trees in **best841.tre**, put all bootstrap trees' **PATH** in **boottree.txt
		- run `astral -i best841.tre -b boottree.txt -r 200 -o RT82_77_841.astral`
		- the 201 tree is the maximum quartet support species tree (MQSST)

## Assessing gene tree-species tree conflict

* rooted the 841 orthologous gene trees and the concatenated ML tree
	- `python reroot_ortho_pxrr.py inputDIR outrerootDIR .tre` # inputDIR contain MLtrees of orthologs
		* e.g., `PATH/cdsall80_Third_fasta/adding2species/RT82_76min_fasta/fasta/conflict_analyses`
	- **rooting criteria**[pxrr code in the Phyx program (Brown et al., 2016)]: 
		1. Beta vulgaris in priority
		2. Limeum aethiopicum and Stegnosperma halimifolium if no Beta
		3. Sesuvium portulacastrum, Delosperma echinatum, Anisomeria littoralis and Guapira obtusata
	- reroot species tree: `pxrr -t species.tree -g Beta -o RAxML_RT82_77_500_best.rr.tre`
	- conflict analyses using Phyparts (Smith et al. 2015)
		* `phyparts -a 1 -v -d reroot_besttree/ -m RAxML_RT82_77_500_best.rr.tre -o Best_conflict70 -s 70`
		* bootstrap support > 70 will be counted.
		
* specific edge conflict comparison
	- the early diverge of Cactaceae
	- the relationships among Cactaceae,Portulacaceae, and Anacampserotaceae
	- the position of Baselaceae and Didiereaceae
	

## Inferring gene duplication events using homologs  

* Build tree with 8592 homologs and assess SH-like support
	- mafft align *.fa, phyutility trim alignment (min_column_occupancy is 0.1)
		- `python PATH/cluster_totree.py DIR num_cores seqtype(dna/aa) bootstrap(y/n)`
	- put .tre and aln_cln files in the same directory and run
	- `python PATH/SHlike_test.py WORKDIR num_cores tree_end (.tre) aln_end (.aln_cln) seqtype (dna)`
	- Change SH support from [note] to "node.label":`python SHnote_move.py inDIR .SHsupport`
	- output "SHsupport.edit.tre" (mv to **SH_edit_note** folder)
* extract rooted clades with Beta (as outgroup) [build a TAXON_CODE file: taxonIDs.txt]
	- `python extract_clades.py inDIR treefileending outDIR MIN_INGROUP_TAXA(30) TAXON_CODE output_name`
	- E.g., `python extract_clades.py SH_edit_note .tre extract_clades 30 taxonIDs.txt Inmin30.tre`
	- output_name is "Inmin30.tre", so all trees contain this name. 8332 clusters returned [extractall]
* Mapping duplication events using phyparts.
	- `phyparts -a 2 -d [8332 extractall] -m RAxML_RT82_77_500_best.rr.tre -o all8332 -s 80 -v`
	- a duplication event was recorded at a node if its two children clades shared two or more taxa
	- mapped to the corresponding node or the corresponding MRCA on the species tree
	

## Inferring genome duplication using Ks plot  

* within species Ks (synonymous substitution rate) plot (141.211.236.91, PATH/analyses/ksplot/runks/)
	- put bin/ and synonymous_calc.py in the same directory with \*.pep.fa and \*.cds.fa (82 species)
	- put script ks_plot.py in the same directory and make a taxon_name_table.txt [taxon_list]
	- run `python ks_plot.py num_cores taxon_list`, this will do:
		1. reduce the highly similar sequences for each .pep.fa by CD-HIT (-c 0.99 -n 5)
		2. conducted 1_to_1 BLASTP analyses within species (-evalue = 10, -max_target_seq = 20)
		3. filter rawblastp results: 
			* remove distant sequences (i.e., pident < 20% or nident < 50 amino acids) 
			* remove sequences with ten or more hits.
		4. set up the paralogous pairs for peptides and cds into separate files
		5. calculate Ks values using **synonymous_calc.py** 
			* the pipeline https://github.com/tanghaibao/biopipeline/tree/master/synonymous_calculation
			* aligns with Clustalw
			* infers Ks using codeml in Paml (Yang 2007) with ng correction (Nei and Gojobori 1986).
			* result in *.ks file
		6. organize \*.ks result in \*.ks.yn and \*.ks.ng files (0.01<yn/ng<3.0)
		7. output an R script for ks plot (Rscript_scripts_ks_plots)
	- put all .ks.ng into one directory and organize the R script to "R_scripts_ks_plots2.ng.r"
		1. `python PATH/R_script_for_ks.py DIR taxon.list file_end`
		2. run R in my Macmini (ks_analyses/ks_result/R_scripts_ks_plots2.ng.r)
* Between species Ks plot (for confirm the position in Montiaceae)
	- put CagrSFB, MJM1156, MJM3167, MJM3168, TRS2027 .pep.fa, .cds.fa .cd-hit in the same directory
	- put multispecies_ks2.py, synonymous_calc.py and bin/ in this directory as well. 
	- run `python multispecies_ks.py num_cores target_taxon subject_taxa_list`
	- e.g., `python multispecies_ks.py 10 MJM1156 TRS2027,MJM3167,MJM3168,CagrSFB`
	- put target_species.ks.ng and betweenspecies.ks.ng in the same directory
	- organize a R_plot_multi.r script using `python PATH/R_script_for_ks.py DIR taxon.list file_end`
	- Edit the R script manually (in Macmini)
	- run R in my Macmini (ks_analyses/multiresult/R_plot_multi.r)

## Gene Ontology and Function Annotation
* Blast gene function for top expanded clusters
	- summarized each gene (8592) size with count_taxon_repeats.py 
		- `python PATH/count_taxon_repeats.py PATH/donemafft/Raxml_tree summarize_8592_fourth.txt`
		- the total number of tips (num_tips)
		- the total number of taxa (maxtaxon_count)
		- the most expanded clades (clade_ave_repeats)
	- open this '*.txt' file, copy and paste the summary into an excel file, sorted lines according to num_tips, maxtaxon_count, or clade_ave_repeats. examined the top20 clusters with highest "num_tips" 
	- Manual annotations through BLASTX against the nonredundant protein database in NCBI by using a long cds sequence for each of the top 20 genes with the highest total number of tips.
	- the same BLASTX/BLASTP method can be used for the top2 clusters in each most expanded clades

* GO (gene ontology terms) overrepresentation on lineage-specific expanded gene families
	- Obtain cds genes of Arabidopsis thaliana (release-34)
		1. download cds from EnsemblPlants (http://plants.ensembl.org/info/website/ftp/index.html)
		2. rename each sequences in the fasta file: TaxonID@locusID using fix_Atha_names.py
			* result e.g., >Arabidopsis@AT3G05780.1
			* `python fix_Atha_names.py inDIR filename`
		3. make blastn database with A.thaliana's cds.
			* `makeblastdb -in Arabidopsis_thaliana.cds.rename.fa -parse_seqids -dbtype nucl -out Atha.cds.database.fa`
	- prepare blastn query files
		* we used the 8332 clusters to conduct gene duplication analyses, so we extract AthaID from them
		* write fasta file from the 8332 clusters
			- `python PATH/tree2fasta_genedup.py adding2species/cln/cdsall82.fa extractall .tre extract_fas`
			- **note** tree2fasta_genedup.py can shorten the output file name, but I can still use:
			- `python write_fasta_from_trees.py ...`
		* extract the longest or multiple sequences from each cluster fasta and write to an outfile
			- `python extract_1orMul_seq2fasta.py inDIR(with fasta file) file_end(.fa) number`
			- results in *\_outcombine.fas; name of each sequences is clusterID.speciesID@sequenceID
	- run blastn and extract AthaID for overrepresentation test
		* Using this *_outcombine.fas file as query, blastn against the Arabidopsis database
			- `blastn -db Atha.cds.database.fa -query extract_fas_outcombine.fas -evalue 10 -num_threads 6 -max_target_seqs 1 -out Dupl8332_multi.blastn -outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'`
			- **Dupl8332_multi.blastn** will be used as sourcefile to extract AthaID for clusters
	- get duplicated clusters for each node and transverse to ID
		* `python get_dup_clade.py DIR(phypart_dup) outname(Dupclades79.out)`
			* resulting file format "nodeID clusterID1,clusterID2,..."
		* `python PATH/get_ids_from_out.py inDIR(with '.out' file) source.blastn file_end(.out)`
			* resulting file format "nodeID AthaID1, AthaID2,..."
		* Re-formating AthaID list and separate nodes into different file(.outID)
			* `python format_AthaID_list.py inputname(\*.AthaID) outputname(\*.format.AthaID)`
				- format ">nodeID\nAthaID1\AthaID2\n..."
			* `python separate_outID.py infile(*.format.AthaID from format_AthaID_list.py)`
	- using node13 as background, focusing on nodes 15, 17 and 19, node 27, node 35, 39, 50, and node 71.
	- GO overrepresentation test using the online version of PANTHER (http://www.pantherdb.org). 
	- The significant level was set to 0.05 with Bonferroni correction for multiple testing. 
	
## Testing for lineage specific episodic positive selection on stress-responding homologs  

Some plant families within Portulaceneae are famous of adapting to severe environmental condition [cold (e.g., Montiaceae), hot and dry (e.g., Cactaceae, Didieraceae)]. 
We tested whether genes responding to abiotic stress would be under positive selection on lineages that are adapted to corresponding environmental conditions. 
To test this, we conducted selection analyses on homologs (Table S1) chosen from following methods:
   
1. prepare database and .blastn files for latter use
	* extract 30 sequences from each of the 8592 clusters
		- `python ~/Transcriptome_code/extract_1orMul_seq2fasta.py cdsall82_fourth_fasta/fasta_aln/ .fa 30`
		- output changed name to "8592_mulseq.fas" file
	* `blastn -db Atha.cds.database.fa -query 8592_mulseq.fas -evalue 10 -num_threads 6 -max_target_seqs 1 -out 8592_mulseq.blastn -outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'`
	* make database for 8592_mulseq.fas
		- `makeblastdb -in 8592_mulseq.fas -parse_seqids -dbtype nucl -out 8592_mulseq.database.fa`

2. identify adaptation genes and extract clusters
	* from literature, we identified some genes that adapted to Cold or dry
	* we extracted these genes form clusters (42 genes at first, reduced to 29 latter,Table 4) 

3. Build tree (see below for alignement cleanup) and conduct HYPHY selection test
	* cd pep/
	* `python ~/Documents/Transcriptome_bird/Transcriptome_code/mafft_wrapper.py . 16 aa`
	* `python PATH/align_cds_from_aa.py inDIR(with Pepaln) outDIR(with cds.fa) file_ending(mafft.aln)`
	* cd cds/
	* `python ~/Documents/Transcriptome_bird/Transcriptome_code/phyutility_wrapper.py . 0.1 dna`
	* `python PATH/raxml_wrapper.py DIR number_cores dna`
	* conduct SHlike supporting analyses and modify note (for latter assessing duplication use): 
		- `python PATH/SHlike_test.py WORKDIR num_cores tree_end aln_end seqtype`
		- `python SHnote_move.py inDIR file_end`
	* `python reroot_homolog_pxrr.py inDIR outDIR file_end` 
		- using pxrr code in the phyx program. 
		- followed the three level criterion mentioned above
	* put \*.aln and \*.rr in the same directory (e.g., runfile), then run:
	* `python PATH/hyphy_run.py inDIR outDIR` 
		- change the file name of .aln and .rr (**note** we use *.aln cds file)
			- cluster8_1rr_1rr_1_inclade2_ortho1.aln => cluster8.fa
			- cluster8_1rr_1rr_1_inclade2_ortho1.rr  => cluster8.tre
		- modify sequence name from speciesID@sequenceID to speciesID_sequenceID
		- write a control file for hyphy run (e.g., cluster56.tre.hyphy.ctl)
			- this will chose: 10) positive selection; 1) Use random effects branch-site model...
			- do not allow branch-site variation in synonymous rates
		- run HYPHYMP < ctl for all the internal nodes. 

4. mechanism of the hyphy analyses (Hyphy v2.2.4)
	* An adaptive branch-site random effects likelihood test (aBSREL, Smith et al. 2010) was used 
	* find lineages subject to episodic selection
	* aBSREL automatically infers an appropriate model among branches
	* We only allow non-synonymous substitution to vary across sites, while alpha (sysnonymous rate) can vary from branch to branch (so that ω varies among branch-site combinations). 
		- aBSREL first fits the standard MG94xREV model to estimate a single ω for each branch
		- then greedily add ω categories to each branch after sorting them by length. 
		- The optimal number of ω categories was justified by AICc scores. 
	* all internal branches were tested to see if there is a significant proportion of sites with ω>1 along any of them using branch-specific Likelihood ratio tests. 
	* The branch with episodic positive selection will be chosen with p < 0.05 (Holm-Bonferroni correction)

5. organize results
	* `python organize_selection_results.py inDIR(with labeled treefile *.edit and selection result) .edit`  
		- `assign_node_label.py`
			- label positive selection on the tree (original tree with "@" in sequences name and "_" in file name).
			- summarize the number of lineages under selection in total and for each focal clades 
	* copy the results into excel, sorted the results by different column (e.g., Cac35, Didier25, Monti15)
	* using the rerooted trees to conduct duplication analyses for each gene cluster.
		- SH-like support > 80 in the phyparts run (see above bullet 3)
		- no setting for SH-like support in phyparts
	* extract duplication nodes for each gene
		* copy the clusternumber into the script, put in gene_list
		* run `python extract_gene_dup_position_on_tree.py DIR outname(get_dup_node.txt)`
	* Genes that are overlap with the target genes (~42 genes) chosen were excluded from final results. 


## alignment cleanup (extra step for chosen gene selection)

refine alignments for chosen genes (see above)
	* `cd /home/ningwum/Documents/RNA_seq_UM/Portulacineae_process/Sample_pep_cds/analyses/Selection_analyses/313_cluster_selection/chosen_gene`
	* `for i in *.aln;do python ~/Documents/Transcriptome_bird/Transcriptome_code/check_align_overlap.py $i /home/ningwum/Desktop/chosen_gene_reSelectin_after_align_cleanup/$i; done`
	* `cd /home/ningwum/Desktop/chosen_gene_reSelectin_after_align_cleanup`
	* `grep "______" *.aln > log.out`
	* 33 clusters have changes out of the original 42 genes, rm the rest cluster, only redo selection for these clusters.
	* `python ~/Documents/Transcriptome_bird/Transcriptome_code/phyutility_wrapper.py . 0.1 dna`
	* `python ~/Documents/Transcriptome_bird/Transcriptome_code/raxml_wrapper.py . 20 dna`
	
**some more check-up**  

<---
hint: Stephen found that there are sequences from the same species that have prefect overlap but were separated in the original 8592 clusters. 
Those sequences were found to have different TR number from trinity output, so were treated separated in TransDecoder analyses. 
However, alignment comparison with other taxa (i.e., have long sequence corss the overlap region) implied that those sequences from the same species should be combined together. 
If not, they may influenced the gene duplication analyses (e.g., inflate the number of duplications, but only rare). 
So I rerun the duplication analyses after cleanup the aligments of the 8592 clusters (not show in the study). 
The results after cleanup are similar to what we got before, so still present the original results. 
the PATH is `~/Documents/RNA_seq_UM/Portulacineae_process/Sample_pep_cds/cds/cdsclustering/cds_Cac26/cdsall82_fourth_fasta/fasta_aln/checkaln` for the new duplication analyses.
--->

* step follow:
	*  `for f in *.mafft.aln; do python ~/Documents/Transcriptome_bird/Transcriptome_code/check_align_overlap.py $f checkaln/$f.checkaln; done`
	* cd checkaln/
	* mmv "*.aln.checkaln" "#1.check.aln"
	* `python ~/Documents/Transcriptome_bird/Transcriptome_code/phyutility_wrapper.py . 0.1 dna`
	* mkdir raxml1 raxml2 raxml3 raxml4
	* mv cluster1\*.aln\_cln cluster2\*.aln\_cln raxml1/...
	* cd raxml1
	* screen
	* `python ~/Documents/Transcriptome_bird/Transcriptome_code/raxml_wrapper.py . 8 dna`
	* `python ~/Documents/Transcriptome_bird/Transcriptome_code/SHlike_test.py . 8 .tre .aln_cln dna`
	* `python ~/Documents/Transcriptome_bird/Transcriptome_code/SHnote_move.py raxml1 .SHsupport` (repeat for raxml2,3,4 folder)
	* `mkdir SH_edit_note`
	* `mv raxml1/*.edit.tre SH_edit_note/` (repeat for raxml2,3,4 folder)
	* `cd SH_edit_note/` `mkdir extract_clades`
	* `python extract_clades.py SH_edit_note .tre extract_clades 30 taxonIDs.txt Inmin30.tre`
	* 8320 clusters returned [`extract_clades` folder]
	* `phyparts -a 2 -d [8332 extractall] -m RAxML_RT82_77_500_best.rr.tre -o dup8320 -s 80 -v`

* organize combined overlap information
	* `cd ../raxml1` `grep "______" *.aln_cln > combine1.out`
	* `cd ../raxml2` `grep "______" *.aln_cln > combine2.out`
	* `cd ../raxml3` `grep "______" *.aln_cln > combine3.out`
	* `cd ../raxml4` `grep "______" *.aln_cln > combine4.out`
	* `cat raxml1/combine1.out raxml2/combine2.out raxml3/combine3.out raxml4/combine4.out > combine_all.out`
	* copy to excel and organize.


# Revision process

## Contamination check
we chose rbcl and matK genes to check for potential contanmination in the Transcriptome data. The rbcl and matK query gene were randomly chosen from a cactus then used to Blastn against the cds from all 82 species.   

* the querys
	- rbcl: `M83543.1 Schlumbergera truncata chloroplast ribulose-1,5-bisphosphate carboxylase/oxygenase large subunit (rbcL) gene, partial cds`
	- matK: `AY015313.1:689-2212 Cereus alacriportanus maturase K (matK) gene, complete cds`

* BLASTN search and mcl cluster:
	- `blastn -db cdsall82.fa -query rbcl_gene_query -evalue 1 -num_threads 8 -max_target_seqs 1000 -task blastn -out rbcl.rawblast -outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore`
	- `python blast_to_mcl.py rbcl.rawblast 0.4`
	- `mcl rbcl.rawblast.hit-frac0.4.minusLogEvalue --abc -te 5 -tf 'gq(5)' -I 1.4 -o rbcl.hit-frac0.4_I1.4_e5`
	- `python write_fasta_from_mcl.py cdsall82.fa rbcl.hit-frac0.4_I1.4_e5 50 rbcl/`
	- `python cluster_to_tree.py . 8 dna n`
	- `python mask_tips_by_taxonID_transciprts.py . . y`
	- `python write_fasta_files_from_trees.py rbcl.fa . .mm round2/`
	- `python cluster_to_tree.py . 8 dna n`
	- check out the tree file and alignment, remove duplicated sequences, check_overlap
	- `python check_align_overlap.py rbclrr3.fa.mafft.aln rbclrr3_checkoverlap.aln`
	- `python phyutility_wrapper.py . 0.1 dna`
	- `python raxml_wrapper.py . 8 dna`
	- The tree (`rbclrr3_checkoverlap.raxml.tre`) showed resonable relationship, with no potential contanmination observed. 

* similar process repeated for matK gene and no contamination observed from tree relationships. (`matKrr.raxml.tre`), no overlapping sequence was found.  


## selection analyses for PEPC and NADP-ME

* use the cluster12_1rr_1rr_1rr.fa and cluster22_1rr_1rr_1rr.fa 
* write pep from cds fasta
* align pep and cds using mafft respectively
* use cds.mafft.aln conduct raxml analyses (phyutility 0.1, raxml_ML)
* check out *.tre files for long tips and trim off cluster22---GrakuSFB@22636
* align cds based on pep alignment, resulting *_pep.aln
* check out overlaps in mafft.aln and \_pep.aln for cds
* for cluster12, there are differences, edit *\_pep.check.aln based on mafft.check.aln
* use mafft.check.aln build raxml trees, used in hyphy analyses
* used \_pep.check.aln for hyphy analyses. 
* run the analyses in 141.211.236.141. 


## significant test for cold adaptive genes

* write a script called `significant_test.py`
* use the 8590 trees built after "check_overlap" alignment (in order to be consistent with the treatment of the focal 29 genes)
* after adding SHsupport and reroot, 8544 trees left for siginificant test
* **note: reroot the tree based on `reroot_homolog_pxrr.py`** so that duplication on node0 can also be detected.
* using phyparts to assess duplication (SH>80, or no SH constraint)
* run `significant_test.py`
	- randomly select 29 genes from the 8544 genes
	- repeat the process 1000 times
	- for target nodes (13, 15, 17, 27, 35, 39. 50), calculate the distribution of duplication number on each node, using either SH>80 or no constraint results.
	- put the results in SH80 and NO_constraint

## Testing whether WGD associated with some ecological traits or diversification

### the dataset we followed in these association analyses are from Smith et al. (2018, New Phytologist)

**climate niche analyses conducted by Stephen using Brownian motion single rate model** (present in the paper)

* Both diversification rate analyses and the climate niche shift analyses needs time tree so we build time tree (including 81 species without `Beta`)
	- use SortaData chose five ortholog genes that most clock like and similar to the species tree topology (still not exactly the same) 
	- chose 13 secondary calibration from Arakaki et al. (2011)
	- use BEAST 1.8.3 to date our transcriptomic tree using the above 5 genes 
	- combine both runs from BEAST and use Treeannotator to summarize trees.

* build a species-level phylogeny using NCBI data
	- use Pyphlawd to extract data from NCBI for both Potulacineae (Cactineae) and Mulluginaceae, constrained the major family level relationships based on our transcriptome tree and build large phylogenies, and bootstraps (93 are consistent with family level relatinonships)
	- use the dated small tree (choose some calibration point) to date the species-level phylogeny (and bootstraps) using treePL

* diversification rate analyses 
	- use MEDUSA (R script) to conduct diversification analyses for the small transcriptome tree (from BEAST) and the dated species-level phylogenies (big tree and bootstraps).
	- the small tree obtained different results from the big tree (fig S5)
* climate shift analyses
	- obtained the Bioclimatic data (Bioclimatic 1-19) from Smith et al.(2018) for Portulacineae (they have obtained the data already for whole Caryophalleles)
	- conducted principal componant analyses and use the first asix of PC (PCA1)
	- focus on mean annual temperature (MAT, BIO1) and mean annual precipitation (MAP, BIO12)
	- reconstruct the ancestral traits for MAP, MAT, and PCA1 for all the species-level phylogenies (including bootstraps) using Brownian motion and single rate model.
	- Compare the nodes with WGD/gene duplication (e.g., Cactaceae) with the corresponding sister clades without these duplication events. 
	- use the bootstrap trees to build the box plot for comparison. 

### **Below: I previously conducted the analyses based on Smith et al.(2018) results**

* download the diversification analyses resulting trees (MEDUSA) `diversification folder`
	- there are two trees `BS_summ_div.tre` and `ML_summ_div.tre`
	- on the tree for each node, there will be infomation as to date and diversification parameters
	- e.g., `Cactaceae_Echinopsis_crassicaulis:11.1951[&r.mean=0.0570026069363819,r.median=0.0570026069363819,r.sd=0,eps.mean=2.50452894065675e-16,eps.median=2.50452894065675e-16,eps.sd=0,freq=1]`
	- r.mean is the useful diversification rate that estimated from Medusa for certain edge. 
	- change the tree file format from Nexus to Newick
	- prune the tree to only include Portulacineae, Molluginaceae and Limeaceae and Stegnospermataceae
	- `python prune_tree_based_on_family_name.py WORKDIR ML_summ_div_newick.tre ML_summ_pruned_div.tre`
	- change the ML_summ_pruned_div.tre to **nexus** format so to see the node.note in bracket (before node.label)
	- open the pruned nexus file in figtree and can color branches based on diversification note. 
		**only the nexus file can exhibit the note in Figtree correctly**

* download the climate data trees in `climate_character_trees`, there are 19 climate occupancy data and two principal components (PCA1 and PCA2). 
	- change the nexus tree to newick format
	- pruned the tree `into pruned_newick_trees` 
	- extract climate data for certain nodes (compare to sister clades with no duplication)
		1. Paired compared clades:
			* Molluginaceae vs. Portulacineae
			* Montiaceae_17 vs. Montiaceae_17sis (climate data has no Montiaceae_17)
			* Montiaceae_19 vs. Montiaceae_19sis  
			* Cactaceae vs. Anacampserotaceae or Portulacaceae (Ana group with Cactaceae in Simth data)
			* Didiereaceae27 vs. Didiereaceae26
		2. The chosen species to trace MRCA of each clade for climate data
			* `MRCA_Dict["Molluginaceae"] = ["Molluginaceae_Mollugo_molluginis", "Molluginaceae_Pharnaceum_elongatum"]`
			* `MRCA_Dict["Portulacineae"] = ["Didiereaceae_Ceraria_fruticulosa", "Montiaceae_Montiopsis_andicola"]`
			* **`#MRCA_Dict["Montiaceae_17"] = ["Montiaceae_Montiopsis_andicola", "Montiaceae_Montiopsis_umbellata"]`**
			* **`#MRCA_Dict["Montiaceae_17sis"] = ["Montiaceae_Cistanthe_grandiflora", "Montiaceae_Cistanthe_arenaria"]`**
			* `MRCA_Dict["Montiaceae_19"] = ["Montiaceae_Lewisia_nevadensis", "Montiaceae_Montia_chamissoi"]`
			* `MRCA_Dict["Montiaceae_Calandrinia"] = ["Montiaceae_Calandrinia_compacta", "Montiaceae_Calandrinia_affinis"]`
			* `MRCA_Dict["Cactaceae"] = ["Cactaceae_Pereskia_aureiflora", "Cactaceae_Cereus_aethiops"]`
			* `MRCA_Dict["Anacam"] = ["Anacampserotaceae_Talinopsis_frutescens", "Anacampserotaceae_Anacampseros_filamentosa"]`
			* `MRCA_Dict["Portulacaceae"] = ["Portulacaceae_Portulaca_quadrifida", "Portulacaceae_Portulaca_grandiflora"]`
			* `MRCA_Dict["Didi27"] = ["Didiereaceae_Alluaudia_comosa", "Didiereaceae_Didierea_trollii"]`
			* `MRCA_Dict["Didi26"] = ["Didiereaceae_Portulacaria_afra", "Didiereaceae_Ceraria_fruticulosa"]`
		3. compare certain clades with parent nodes
	- `python extract_climate_data.py . .tre climate_withparent.out`

* extract diversification shift from the pruned diversifcation tree, only keep r.mean in the note.
	- `python extract_diversification_shift.py ML_summ_pruned_div.tre rateshift.out`
	- the results are the same as Simth et al. (2018) Table 4 a-j. 
	- No diversification shift change found in the Didiereaceae clade. 



## detecting introgression/hybridization for the gene tree-species tree conflict.

1. using **HyDe** to detect potential hybrids in our data. (based on allele frequency)
	- git clone https://github.com/pblischak/HyDe.git
	- install required module using pip
		* sudo pip install seaborn
		* sudo pip install multiprocess
	- install Hyde followed the guide
		* `sudo python setup.py install`
		* `make test`
		* `make test_threads`
	- `mkdir MBE_ortho1`
	- For the relationship conflicts among familes
		- using the `RT82_77_500.concat.py` data to conduct the run_hyde analyses (for family level relationship)
		- make a mapping file based on family level relationship (taxa is families)
		- `run_hyde.py -i RT82_77_500.concat.phy -m mapping_family.txt -o OUT -n 82 -t 9 -s 1239871`
		- `individual_hyde.py -i RT82_77_500.concat.phy -m mapping_family.txt -o OUT -tr hyde-out-filtered.txt -n 82 -t 9 -s 1239871`
	- for the Early derived relationship within Cactaceae
		- using the pruned dataset only contain Cactaceae and Molluginaceae
		- `mkdir Cactaceae_basal`
		- mapping file contain genus level/subfamily level relationship
		- `run_hyde.py -i Cactaceae.phy -m mapping_Cactaceae.txt -o OUT -n 41 -t 6 -s 1239871`
		- `individual_hyde.py -i Cactaceae.phy -m mapping_Cactaceae.txt -o OUT -tr hyde-out-filtered.txt -n 41 -t 6 -s 1239871`
		- `bootstrap_hyde_mp.py -i Cactaceae.phy -m mapping_Cactaceae.txt -o OUT -tr hyde-out-filtered.txt -n 41 -t 6 -s 1239871`

2. using **dfoil** to detect introgression among selected five species trees

	- **the major clode used in this analyses is `extract_focal_align_from_fas.py`**
	- the Cactaceae, Portulacaceae, and Anacampserotaceae (PCA) test:
		* chose Ceyu18,Opbr17/Maau13 from Cactaceae, LDEL (Portulacaceae), and GrakuSFB (Anacampserotaceae), PhaexSFB (outgroup, Molluginaceae)
		* the corresponding phylogeny under test is (((Ceyu18,Opbr17),(LDEL,GrakuSFB)),PhaexSFB). 
		* I filtered the orthologs by including loci with all these five species present (799 out of 841 loci)
		* use fasta2dfoil.py to generate count file for these loci
		* `python '/home/ningwum/Documents/Repo/dfoil/fasta2dfoil.py' cluster* --out Cac_count.out --names Ceyu18,Opbr17,LDEL,GrakuSFB,PhaexSFB`
		* use dfoil.py to calculate Dfoil statistics and check out the introgression inference.
		* `python ~/Documents/Repo/dfoil/dfoil.py --infile Cac_count.out --out Cac_stat.out`
		* **We detect hibridizaion between Cactaceae ancestor and Anacampserotaceae in 262 genes**
	 
	- The Basellaceae and Didiereaceae test
		* the chosen taxa are: "Ceyu18","GrakuSFB/Maau13","DBG19880583021G","AncoSFB","PhaexSFB"
		* the total number of genes under test are 818
		* **We detect hybridization between Basellaceae and the ancestor of P1P2 (Ceyu18 and GrakuSFB) in 34 genes**
		
	- The early divergence of Cactaceae
		* the chosen taxa are: "Ceyu18","Opbr17/Maau13","Lely","Pegr","PhaexSFB"
		* the total number of genes under test are 770
		* **hybridization occured between Pegr and Core cacti in 81 genes**

3. using **phylonet** to detect potential introgression (phylogenetic network) among lineages
	- install phylonet by nano phylonet and type `java -Xmx6g -jar /home/ningwum/Documents/Repo/PhyloNet_3.6.4.jar $*` in it.
	- make phylonet executable: `chmod +x phylonet`
	- prune the 841 gene trees by including the following taxa
	- "GrakuSFB", "AncoSFB", "Mapo24", "Ceyu18", "Lely", "Opbr17", "Pegr", "DBG19880583021G", "MJM2214", "LDEL", "MJM1789", "LimaeSFB"
	- `for tre in *.tre; do python ~/Documents/Transcriptome_code/prune_tree_taxa.py . $tre $tre.pruned >> Incomplete.out; done`
	- only keep complete trees with all taxa included (754 gene trees without brlen)
	- infer phylogenetic network using MPL method (20 duplication runs, bootstrap support cutoff 50)
	- `InferNetwork_MPL (all) 3 -b 50 -x 20 -pl 8 -po -di "./Infernet_MPL3.out";`
	- `phylonet MPL_withbs.nex`
	- we set number of maximum reticulations to 3, 5, 7, or 9 and examined the loglikelihood scores for each optimum network by using CalGTPorp method
	- `phylonet CalGTProp.nex`
	- choose the best network estimation run CalIntroRate.nex
	- **It turns out when there are five reticulations, the network received the best likelihood score.** 
	- use **Dendroscope** or **phylopot** to view the network
	- `phylonet CalIntroRate.nex`
	- the CalIntroRate can estimate the probability of a gene locus to support the reticulate node (>0.5), discern whether the gene tree topology is caused by introgression or other process like ILS.

**overall, all three methods detect hybridization among certain lineages, which could contribute to the gene tree conflicts in certain scale.**

#### Use "PhyloPlot" to visulize the network (inplemented in PhyloNetworks)

* install Julia
	* download julia
	* export PATH/julia/bin in .bashrc
	* open Julia
	* Pkg.add("PhyloNetworks")
	* Pkg.add("PhyloPlots")
	* Pkg.add("Gadfly")
	* using PhyloNetworks;
	* net1 = readTopology(tree_string);
	* using PhyloPlots
	* p = plot(net1, :R, showGamma=true)
	
	
## mapping conflict pie chart on constaint or species tree

* use 841 orthologs to conduct conflict assessment
* Constraint trees for assessing conflict
	- AC\_P\_constraint.tre [((A,C),P)]
	- PC\_A\_constraint.tre [((P,C),A)]
	- BD\_basalCac\_constraint.tre
		1. (Basellaceae,Didiereaceae)+ATCP
		2. (Pereskia, Leuenbergeria)+ Other Cactaceae
* conduct conflict assessment using phyparts
	- `phyparts -a 1 -v -d reroot_besttree/ -m BD_basalCac_constraint.tre -o BD_basalCac70 -s 70`
	- `phyparts -a 1 -v -d reroot_besttree/ -m AC_P_constraint.tre -o AC_P70 -s 70`
	- `phyparts -a 1 -v -d reroot_besttree/ -m PC_A_constraint.tre -o PC_A70 -s 70`
* mapping pie chart
	* install ete2 module for python pie chart mapping
		- pip install ete2
		- sudo apt-get install python-qt4
	* `python Code/make_conflict_pie_charts_ontree.py . BD_basalCacmapping.tre BD_basalCac70.node.key BD_basalCac70.hist 841 BD_basalCac70.concon.tre ../taxonID.txt`
	* repeat the same analyses for other constraint tree and the species tree
