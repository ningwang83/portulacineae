#! /usr/bin/env python

"""
Use this script to correct RNA-seq reads and then use Trinity to assemble reads
RNA-seq reads are pair ended in this case and all fq files from one species should be in one directory
each species could have more than two fq files, e.g., RNA-seq from tongue and gut for birds, or maybe 
sequences from one tissue were separated into several fq files numbering with 0001,0002...

In the current code, we are trying to assemble read for one species using all the fq files, no matter what
tissues they are from. In this case, it may result in more complete contigs. however, due to alternative spliting, 
we may want to assemble reads from different tissue separately, and I'm gonna define different functions to deal with
these two cases.

No matter what cases, rcorrector should run for all fq files.

"""

import os,sys
import subprocess
from subprocess import call

TruSeq_ADAPTER = "/home/ningwum/Documents/Repo/phylogenomic_dataset_construction/data/TruSeq_adapters" # my path for plant adapter
Trinity = "/home/ningwum/Documents/Repo/trinityrnaseq/Trinity"
rcorrector = "rcorrector"
trimmomatic = "trimmomatic"

# bigmem2 used variables
#TruSeq_ADAPTER = "/export/people/ningwum/Repo/phylogenomic_dataset_construction/data/TruSeq_adapters"
#Trinity = "Trinity"
#rcorrector = "perl /export/people/ningwum/Repo/rcorrector/run_rcorrector.pl"
#trimmomatic = "java -jar /export/people/ningwum/Repo/trimmomatic/classes/trimmomatic.jar"

def fq_dictionary(DIRlist,read1_identifier,read2_identifier,infile_end):
	"""DIRlist is a list of directories; when read_identifier = "_1" or "_R1_", 
	then all fq files from different tissues of the same species will be assembled together. 
	If you only want to assemble gut, then put identifier ='g_1' or 'g_2' """
	assert len(DIRlist) >= 1, "directory list more than 1"
	print DIRlist
	forward_fqs, reverse_fqs = [],[]
	for inDIR in DIRlist:
		fq1,fq2 = [],[] #list of forward and reverse read files
		taxon = inDIR
		inDIR = os.path.abspath(inDIR) + "/"
		for i in sorted(os.listdir(inDIR)): # inDIR name must have no "_"
			if i.endswith(infile_end):
				if read1_identifier in i and read2_identifier not in i:
					fq1.append(inDIR+i)
					print "Adding forward read file",i
				elif read2_identifier in i and read1_identifier not in i:
					fq2.append(inDIR+i)
					print "Adding reverse read file",i
		assert len(fq1) > 0, "No forward read file found"
		assert len(fq2) > 0, "No reverse read file found"
		assert len(fq1) == len(fq2), "Unequal number of forward and reverse files"
		if len(fq1) > 1:
			fq1str = " ".join(fq1)
			fq2str = " ".join(fq2)
			R1 = inDIR+taxon+"_R1.fq"
			R2 = inDIR+taxon+"_R2.fq"
			cmd1 = "zcat "+fq1str+" > "+R1
			cmd2 = "zcat "+fq2str+" > "+R2
			call(cmd1,shell=True)
			call(cmd2,shell=True)
			#call("gzip R1",shell=True)
			#call("gzip R2",shell=True)
			forward_fqs.append(R1)
			reverse_fqs.append(R2)
		forward_fqs.append("".join(fq1))
		reverse_fqs.append("".join(fq2))
	assert len(forward_fqs) == len(reverse_fqs), "Unequal number of forward and reverse lists"
	Dic_fastq = dict(zip(forward_fqs,reverse_fqs))
	return Dic_fastq  # can also return forward_fqs,reverse_fqs

def trim_adapter(Dic_fastq,thread,TruSeq_ADAPTER):
	trim_R1_fqs,trim_R2_fqs = [],[]
	for forward in Dic_fastq:
		folder = "/".join(forward.split("/")[:-1])+"/"
		taxon = forward.split("/")[-1]
		taxon =  taxon[:5] # this is the ID number of each sample for Cary project
		reverse = Dic_fastq[forward]
		outputFile1P = folder+taxon+"_R1.P.fq"
		outputFile1U = folder+taxon+"_R1.U.fq"
		outputFile2P = folder+taxon+"_R2.P.fq"
		outputFile2U = folder+taxon+"_R2.U.fq"
		R1 = folder+taxon+"_R1.fq"
		R2 = folder+taxon+"_R2.fq"
		cmd = trimmomatic+" PE -threads "+thread+" -phred33 "+forward+" "+reverse+" "+outputFile1P+" "+outputFile1U+\
		" "+outputFile2P+" "+outputFile2U+" ILLUMINACLIP:"+TruSeq_ADAPTER+":2:30:10"
		call(cmd,shell=True)
		cmdcat1 = "cat "+outputFile1P+" "+outputFile1U+" > "+R1
		cmdcat2 = "cat "+outputFile2P+" "+outputFile2U+" > "+R2
		call(cmdcat1,shell=True)
		call(cmdcat2,shell=True)
		# make a gzip file
		cmdzip1 = "gzip "+R1
		cmdzip2 = "gzip "+R2
		call(cmdzip1,shell=True)
		call(cmdzip2,shell=True)
		R1zip = R1+".gz"
		R2zip = R2+".gz"
		trim_R1_fqs.append(R1zip)
		trim_R2_fqs.append(R2zip)
		os.remove(outputFile1P)
		os.remove(outputFile1U)
		os.remove(outputFile2P)
		os.remove(outputFile2U)
	Dic_trim_fastq = dict(zip(trim_R1_fqs,trim_R2_fqs))
	return Dic_trim_fastq


def run_rcorrector(Dic_fastq, Num_thread):
	""" run rcorrector on each of the sample files """
	Rcorrect_R1_fqs, Rcorrect_R2_fqs = [],[]
	for forward in Dic_fastq:
		folder = "/".join(forward.split("/")[:-1])+"/"
		taxon = forward.split("/")[-1]
		taxon =  taxon[:5]
		reverse = Dic_fastq[forward]
		cmd = rcorrector+" -1 "+forward+" -2 "+reverse+" -od "+folder+" -t "+Num_thread
		print cmd
		call(cmd,shell=True)
		#correct_forward = forward.split(",")
		#correct_forward = [element.replace("fq","cor.fq") for element in correct_forward]
		correct_forward = forward.replace("fastq","cor.fq")
		correct_reverse = reverse.replace("fastq","cor.fq")
		Rcorrect_R1_fqs.append(correct_forward)
		Rcorrect_R2_fqs.append(correct_reverse)
	assert len(Rcorrect_R1_fqs) == len(Rcorrect_R2_fqs), "Unequal number of corrected forward and reverse lists"
	Dic_correct_fastq = dict(zip(Rcorrect_R1_fqs,Rcorrect_R2_fqs))
	return Dic_correct_fastq


def run_Trinity(Dic_fastq,num_CPU,max_memory_GB,tissue_type="",clip=False): # tissue_type should be gut or tongue or gt or ""
	"""Assemble using trinity"""
	if clip:
		assert os.path.exists(TruSeq_ADAPTER),"Cannot fine the adapter file "+TruSeq_ADAPTER
		trim_setting = '"ILLUMINACLIP:'+TruSeq_ADAPTER+\
				':2:30:10 SLIDINGWINDOW:4:5 LEADING:5 TRAILING:5 MINLEN:25"'
	else: 
		trim_setting = '"SLIDINGWINDOW:4:5 LEADING:5 TRAILING:5 MINLEN:25"'
		#trim_setting = ""
	for forward in Dic_fastq:
		folder = "/".join(forward.split("/")[:-1])+"/"
		taxon = forward.split("/")[-1]
		taxon =  taxon[:5]
		outname = folder+taxon
		rename = outname+tissue_type+".Trinity.fasta"
		reverse = Dic_fastq[forward]
		cmd = Trinity+" --trimmomatic --quality_trimming_params "+trim_setting+\
		" --seqType fq --left "+forward+" --right "+reverse+" --output "+outname+\
		tissue_type+".trinity --max_memory "+max_memory_GB+"G"+" --CPU "+num_CPU+" --full_cleanup --SS_lib_type RF"
		call(cmd,shell=True)
		os.rename(outname+tissue_type+".trinity.Trinity.fasta",rename) # shorten name


if __name__ =="__main__":
	if len(sys.argv) != 6:
		print "usage: python assemble.py DIRlist num_CPU max_memory_GB Num_thread file_end"
		print "DIRlist separated by ','"
		sys.exit()
	
	DIRs,num_CPU,max_memory_GB,Num_thread,file_end = sys.argv[1:6]
	# Collect all the fastq.gz files and return the rcorrecor input dictionary
	Dic_fastq = fq_dictionary(DIRs.split(","),\
			read1_identifier="R1",\
			read2_identifier="R2",\
			infile_end=file_end)
	# trim adapter
	# Trim_Dic = trim_adapter(Dic_fastq,Num_thread,TruSeq_ADAPTER)

	# rcorrect
	# Rcorrect_Dic = run_rcorrector(Dic_fastq, Num_thread)

	# assemble
	run_Trinity(Dic_fastq,num_CPU,max_memory_GB,clip=True) # default arguments should placed the end
