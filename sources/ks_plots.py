"""
cd into the directory where taxonID.pep.fa and taxonID.cds.fa are located
make sure that cd-hit is in the path and is executable
make sure that synonymous_calc.py and bin from
https://github.com/tanghaibao/bio-pipeline/tree/master/synonymous_calculation
are in the current directory.

blast output file columns (separated by tab):
0-qseqid 1-qlen 2-sseqid 3-slen 4-frames 5-pident 6-nident 7-length 
8-mismatch 9-gapopen 10-qstart 11-qend 12-sstart 13-send 14-evalue 15-bitscore

nident >= 150 nucleotides
pident >= 30

Create a tabular file that each line contains
code	taxon_name
separated by tab
"""

import sys,os
from Bio import SeqIO

def get_taxon(seqid):
	return seqid.split("@")[0]
	
if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python ks_plots.py num_cores taxon_name_table"
		sys.exit(0)
	
	num_cores,taxon_table = sys.argv[1:]
	
	DIR = "./"
	for cds in os.listdir(DIR):
		if not cds.endswith(".cds.fa"): continue
		taxon = cds.replace(".cds.fa","")
		rawpep = taxon+".pep.fa"
		pep = taxon+".cd-hit"

		if not os.path.exists(pep):
			cmd = "cd-hit -i "+rawpep+" -o "+pep+" -c 0.99 -n 5 -T "+str(num_cores)
			print cmd
			os.system(cmd)

		#all-by-all blastp within each taxon
		if not os.path.exists(pep+".rawblastp"):
			cmd = "makeblastdb -in "+pep+" -parse_seqids -dbtype prot -out "+pep
			print cmd
			os.system(cmd)
			cmd = "blastp -db "+pep+" -query "+pep+" -evalue 10 -num_threads "+str(num_cores)
			cmd += " -max_target_seqs 20 -out "+pep+".rawblastp "
			cmd += "-outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'"
			print cmd
			os.system(cmd)
			os.system("rm "+pep+".p*")

		#remove larger gene families
		if not os.path.exists(pep+".blastp.filtered"):
			block = [] # store a list of query,hit pairs with the same query
			infile = open(pep+".rawblastp","r")
			outfile = open(pep+".blastp.filtered","w")
			last_query,last_hit = "",""
			for line in infile:
				spls = line.split("\t")
				if len(spls) < 3: continue
				query,hit,pident,nident = spls[0],spls[2],float(spls[5]),int(spls[6])
				if query == hit or pident < 20.0 or nident < 50: continue
				if query == last_query or block == []:
					if (query,hit) not in block:
						block.append((query,hit)) # add tuple to block
				else:
					if len(block) < 10:
						for i in block:
							outfile.write(i[0]+"\t"+i[1]+"\n")
					block = []#reset and initiate the block
					block.append((query,hit))	
				last_query,last_hit = query,hit
			# process the last block
			if len(block) < 10:
				for i in block:
					outfile.write(i[0]+"\t"+i[1]+"\n")
			block = []#reset and initiate the block
			infile.close()
			outfile.close()

		if not os.path.exists(taxon+".cds.pairs.fa"):
			cdsDICT = {} #key is seqid, value is seq
			with open(cds,"r") as handle:
				for record in SeqIO.parse(handle,"fasta"):
					cdsDICT[str(record.id)] = str(record.seq)
			pepDICT = {} #key is seqid, value is seq
			with open(pep,"r") as handle:
				for record in SeqIO.parse(handle,"fasta"):
					pepDICT[str(record.id)] = str(record.seq)
			pairs = [] #store tuples that have been processed to avoid repeating
			infile = open(pep+".blastp.filtered","r")
			outfile1 = open(taxon+".pep.pairs.fa","w")
			outfile2 = open(taxon+".cds.pairs.fa","w")
			for line in infile:
				query,hit = line.strip().split("\t")
				pair = (query,hit) if query > hit else (hit,query)
				if pair not in pairs:
					pairs.append(pair)
					outfile1.write(">"+query+"\n"+pepDICT[query]+"\n")
					outfile1.write(">"+hit+"\n"+pepDICT[hit]+"\n")
					outfile2.write(">"+query+"\n"+cdsDICT[query]+"\n")
					outfile2.write(">"+hit+"\n"+cdsDICT[hit]+"\n")
			infile.close()
			outfile1.close()
			outfile2.close()
			print len(pairs),"sequence pairs wirtten"
		
		if not os.path.exists(taxon+".ks"):
			cmd = "python synonymous_calc.py "+taxon+".pep.pairs.fa "+taxon+".cds.pairs.fa >"+taxon+".ks"
			print cmd
			os.system(cmd)
			#os.system("python report_ks.py "+taxon+".ks")

		#parse the ks output
		#0-name,1-dS-yn,2-dN-yn,3-dS-ng,4-dN-ng
		if not os.path.exists(taxon+".ks.yn") or not os.path.exists(taxon+".ks.ng"):
			first = True
			infile = open(taxon+".ks","r")
			outfile1 = open(taxon+".ks.yn","w")
			outfile2 = open(taxon+".ks.ng","w")
			for line in infile:
				if first:
					first = False
					continue
				spls = line.strip().split(",")
				yn,ng = float(spls[1]),float(spls[3])
				if yn > 0.01 and yn < 3.0:
					outfile1.write(spls[1]+"\n")
				if ng > 0.01 and ng < 3.0:
					outfile2.write(spls[3]+"\n") 
			infile.close()
			outfile1.close()
			outfile2.close()

	# output looks like taxonID.ks.ng and taxonID.ks.yn
	# write the scripts to be run in R to plot the output
	DICT = {} # key is taxon, value is long species name
	with open(taxon_table, "rU") as infile:
		for line in infile:
			spls = line.strip().split("\t")
			if len(spls) > 1:
				DICT[spls[0]] = spls[1]
	
	out = ""
	for name in os.listdir(DIR):
		if name.endswith(".ng") or name.endswith(".yn"):
			taxon = name.split(".")[0]
			out += "pdf=pdf(file='"+name+".pdf',width=7,height=7)\n"
			out += "a<-read.table('"+name+"')\n"
			out += "hist(a[,1],breaks=60,col='grey',xlab='',ylab='',main='"+DICT[taxon]+"',axes=FALSE)\n"
			out += "axis(1,pos=0)\naxis(2,pos=0)\ndev.off()\n\n"
			
	with open("R_scripts_ks_plots","w") as f: f.write(out)

