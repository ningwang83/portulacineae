#! /usr/bin/env python

"""
use this code to format alignment file, tree files and run hyphy for positive selection 
using branch-site model. detect specific episodic selection for certain lineages
original alignment file end with .aln, original tree file end with .rr (rerooted)
edited alignment file end with .fa, edited tree file end with .tre, which are used in hyphy analyses
"""

import os,sys
import tree_reader
from seq import read_fasta_file



def change_tip_name(name):
	namelist = name.split("@")
	return "_".join(namelist)


def run_hyphy(alnfile,treefile,output):
	ctl = treefile+".hyphy.ctl"
	with open(ctl,"w") as outfile:
		outfile.write("10\n1\n1\n1\n2\n")
		outfile.write(alnfile+"\n"+treefile+"\n")
		outfile.write("3\nd\n")
		outfile.write(output)
	os.system("HYPHYMP < "+ctl)
		

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python "+sys.argv[0]+" inDIR outDIR"
		sys.exit(0)
	inDIR,outDIR = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	outDIR = os.path.abspath(outDIR)+"/"

	# change tip name from Name@ID to Name_ID, even if there is no @ at tiplabel
	for f in os.listdir(inDIR):
		if f.endswith(".aln"):
			clusterID = f.split("_")[0]
			outfile = open(outDIR+clusterID+".fa","w")
			with open(inDIR+f,"rU") as infile:
				for l in infile:
					if l.startswith(">"):
						name = l.strip()[1:]
						name = change_tip_name(name)
						outfile.write(">"+name+"\n")
					else:
						outfile.write(l)
			infile.close()
			outfile.close()
		elif f.endswith(".rr"):
			clusterID = f.split("_")[0]
			outtree = open(outDIR+clusterID+".tre","w")
			intree = open(inDIR+f,"r")
			oneline = intree.readline().strip()
			tree = tree_reader.read_tree_string(oneline)
			for i in tree.iternodes():
				if not i.istip: continue
				tipname = i.label
				i.label = change_tip_name(tipname)
			edittree = tree.get_newick_repr(True)+";"
			outtree.write(edittree)
			intree.close()
			outtree.close()
	# run hyphy
	for runf in os.listdir(outDIR):
		if runf.endswith(".fa"):
			clusterID = runf.split(".")[0]
			alnfile = outDIR+runf
			treefile = outDIR+clusterID+".tre"
			output = outDIR+clusterID
			run_hyphy(alnfile,treefile,output)

