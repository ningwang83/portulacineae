#! usr/bin/env python

import newick3,phylo3,os,sys,math
from tree_utils import *
from cluster_totree import read_fasta

if len(sys.argv) != 5:
    print "usage: python ortho_brsearch_correct.py INDIR fastaDIR file_end leftcutoff"
    sys.exit()

INDIR,fastaDIR,file_end,leftcutoff = sys.argv[1:]
INDIR = os.path.abspath(INDIR)+"/"
fastaDIR = os.path.abspath(fastaDIR)+"/"

treelist = []

for i in os.listdir(INDIR):
    if not i.endswith(file_end): continue
    with open(INDIR+i,"r") as infile:
        intree = newick3.parse(infile.readline())
    tiplist = []
    for node in intree.iternodes():
        leavenum = len(node.leaves())
        if node.istip or leavenum != 2 : continue
        child0,child1 = node.children[0],node.children[1]
        len0,len1 = child0.length,child1.length
        if child0.istip and len0 > float(leftcutoff):
            if len1 == 0.0 or len0/len1 > 10:
                tiplist.append(child0.label)
        if child1.istip and len1 > float(leftcutoff):
            if len0 == 0.0 or len1/len0 > 10 :
                tiplist.append(child1.label)
    if len(tiplist) > 0:
        clusterID = i.split(".")[0]
        treelist.append(clusterID)
        for fasta in os.listdir(fastaDIR):
            if fasta.startswith(clusterID):
                seqDict = read_fasta(fastaDIR+fasta)
                outfasta = open(fastaDIR+clusterID+".edit.fa","a")
                for s in seqDict.keys():
                    if not s in set(tiplist):
                        outfasta.write(">"+s+"\n"+seqDict[s]+"\n")
                outfasta.close()
    

