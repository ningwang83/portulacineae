"""
Read a concatenated pep fasta file
write individual pep from cds fasta
"""

import sys,os
from seq import read_fasta_file

def clusterdic(fasDIR):
	#fasDIR = os.path.abspath(fasDIR) + "/"
	seqdic = {}
	for i in os.listdir(fasDIR):
		if not i.endswith(".edit.fa.aln_cln"): continue
		seqID_list = []
		clusterID = i.split(".")[0]
		infile = open(fasDIR+i,"rU")
		for l in infile:
			if not l.startswith(">"): continue
			seqID = l.strip()[1:]
			seqID_list.append(seqID)
		seqdic[clusterID] = seqID_list
		infile.close()
	return seqdic

if __name__ =="__main__":
	if len(sys.argv) != 3:
		print "usage: python "+sys.argv[0]+" fasDIR outDIR"
		sys.exit()
	
	fasDIR,outDIR = sys.argv[1:]
	fasDIR = os.path.abspath(fasDIR) + "/"
	outDIR = os.path.abspath(outDIR) + "/"
	Clusterdic = clusterdic(fasDIR)

	for i in os.listdir(fasDIR):
		if not i.endswith(".fa"): continue
		clusterID = i.split(".")[0]
		if clusterID in Clusterdic.keys():
			seqDICT = {} #key is seqID, value is seq
			for s in read_fasta_file(fasDIR+i):
				seqDICT[s.name] = s.seq
			outname = outDIR+i
			outfile = open(outname,"w")
			with open(fasDIR+i,"rU")as infile:
				for line in infile:
					line = line.strip()
					if line[0] == ">":
						indiID = line[1:]
						speciesID = indiID.strip().split("@")[0]
						if speciesID in Clusterdic[clusterID]:
							outfile.write(line+"\n"+seqDICT[indiID]+"\n")


