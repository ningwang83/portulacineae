#! /usr/bin/env python

import sys,os
from tree_reader import read_tree_string
import string, sys
from node import Node

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python ID2taxon.py taxon_table treefile"
		sys.exit()
	
	taxon_table, treefile = sys.argv[1:]
	#treefile_type = "homolog"
	treefile_type = "species"
	
	DICT = {} # key is taxon, value is long species name
	with open(taxon_table, "rU") as infile:
		for line in infile:
			spls = line.strip().split("\t")
			if len(spls) > 1:
				DICT[spls[0]] = spls[1]
	infile.close()			
	
	outtreefile = treefile+".rn.tre"
	outfile = open(outtreefile,"w")
	with open(treefile, "rU") as handle:
		treeline = handle.readline()
		root = read_tree_string(treeline)
		
		for l in root.leaves():
			if treefile_type == "homolog":
				Id = l.label.split("@")[0]
				num = l.label.split("@")[1]
				l.label = DICT[Id]+"@"+str(num)
			else:
				Id = l.label
				l.label = DICT[Id]
		outfile.write(root.get_newick_repr(True))
		
		
		
			
