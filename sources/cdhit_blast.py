#!/usr/bin/python 

"""
run cd-hit-est for *.cds.fa files or cd-hit for .pep.fa files
to culster most similar sequenceing and reduce redundancy.

"""

import os,sys
#import subprocess
#from subprocess import call

if __name__ == "__main__":
  if len(sys.argv) != 3:
    print "python cdhit_blast.py WORKDIR num_threads datatype(pep/cds)"
    print "Usage: provide path of working directory and number of threads"
    sys.exit(0)

  WORKDIR = sys.argv[1]
  WORKDIR = os.path.abspath(WORKDIR)+"/"
  NUM_THREADS = sys.argv[2]
  datatype = sys.argv[3]

# now read through the working directory and find the files name ended with FILE_END
# Then execute commmand of cd-hit-est to cluster similar sequences and reduce redundancy
# comments also need to have proper indentation!

  for file in os.listdir(WORKDIR): 
    taxonID = file.split(".")[0]
    print file, taxonID
    if file.endswith(".cds.fa"): 
      cmd = "cd-hit-est -i "+file+" -o "+taxonID+".fa.cds.cdhitest -c 0.99 -n 10 -r 0 -T "+NUM_THREADS
    elif file.endswith(".pep.fa"):
      cmd = "cd-hit -i "+file+" -o "+taxonID+".fa.pep.cdhit -c 0.995 -n 5 -T "+NUM_THREADS
    else: continue
    print cmd
    os.system(cmd)

  # Doing cds clustering
  if datatype == "cds":
  # make new directory in WORKDIR
    NEWDIR_cds = WORKDIR+'cdsclustering'  # must define a string first
    os.mkdir(NEWDIR_cds)
  # concatenate all the .fa.cds.cdhitest files
    print "cat all .fa.cds.cdhitest files, output to cdsall.fa"
    cmd1 = "cat "+WORKDIR+"*.fa.cds.cdhitest > "+NEWDIR_cds+"/cdsall.fa"
    print cmd1
    os.system(cmd1)
  # create blastn database
    print "Creating blastn database"
    cmd1 = "makeblastdb -in "+NEWDIR_cds+"/cdsall.fa -parse_seqids -dbtype nucl -out "+NEWDIR_cds+"/cdsall.fa"
    print cmd1
    os.system(cmd1)
  # blastn against the database all by all
  # setting the max_target_seqs to 1000 (large enough) to avoid braking up large gene families in MCL
    print "all by all blastn for all fasta data"
    cmd_blastn = "blastn -db "+NEWDIR_cds+"/cdsall.fa -query "+NEWDIR_cds+"/cdsall.fa -evalue 10 -num_threads "+NUM_THREADS+" -max_target_seqs 1000"
    cmd_blastn += " -out "+NEWDIR_cds+"/cdsall.rawblast -outfmt '6 qseqid qlen sseqid slen frames pident nident \
                   length mismatch gapopen qstart qend sstart send evalue bitscore'"
    print cmd_blastn
    os.system(cmd_blastn)


  elif datatype == "pep": # Doing pep clustering
  # make new directory
    NEWDIR_pep = WORKDIR+'pepclustering'
    os.mkdir(NEWDIR_pep)
  # concatenate all .fa.pep.cdhit files
    print "cat all .fa.pep.cdhit files, output to pepall.fa into new directory"
    cmd2 = "cat "+WORKDIR+"*.fa.pep.cdhit > "+NEWDIR_pep+"/pepall.fa"
    print cmd2
    os.system(cmd2)
  # creat blastp database
    print "Creating blastp database"
    cmd2 = "makeblastdb -in "+NEWDIR_pep+"/pepall.fa -parse_seqids -dbtype prot -out "+NEWDIR_pep+"/pepall.fa"
    print cmd2
    os.system(cmd2)
  # blastp against the database one by one
  # setting the max_target_seqs to 1000 (large enough) to avoid braking up large gene families in MCL
    for file in os.listdir(WORKDIR):
      if file.endswith(".fa.pep.cdhit"):
        print "conducting blastp on "+file
        cmd = "blastp -db "+NEWDIR_pep+"/pepall.fa -query "+WORKDIR+file
        cmd += " -evalue 10 -num_threads "+NUM_THREADS
        cmd += " -max_target_seqs 1000 -out "+NEWDIR_pep+"/"+file+".rawblastp "
        cmd += "-outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'"
        print cmd
        os.system(cmd)
      else: continue
    print "blastp is done"
  
    os.system("cat "+NEWDIR_pep+"/*.rawblastp > "+NEWDIR_pep+"/pepall.rawblast")

