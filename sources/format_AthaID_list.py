#! /usr/bin/env python

import os,sys

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python "+sys.argv[0]+" inputname outputname"
		sys.exit()

	DIR = os.getcwd()+"/"
	inputname,outputname = sys.argv[1:]
	outfile = open(DIR+outputname,"w")

	alllist = []
	with open(DIR+inputname,"rU") as infile:
		for l in infile:
			cladeID = l.strip().split("\t")[0]
			AthaIDlist = l.strip().split("\t")[-1].split(",")
			alllist += AthaIDlist
			outfile.write(">"+cladeID+"\n"+"\n".join(set(AthaIDlist))+"\n")
	infile.close()
	outfile.write(">background\n"+"\n".join(set(alllist))+"\n")



