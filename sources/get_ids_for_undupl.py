#! /usr/bin/env python

"""
the out file is the compare node duplication clusters outfile, output from compare_clade_dup.py
convert the clusterID to AthaID so can be used in the Panther Go analyses
"""

import os,sys

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python "+sys.argv[0]+" inDIR source_file file_end"
		print "file_end with .AthaID, source_file is the blastn file"
		sys.exit(0)
	
	inDIR,source_file,file_end = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	source = os.path.join(os.getcwd(),source_file)
	
	#read in source file and built cluster to AthaID dictionary
	sourcefile = open(source,"rU")
	clusterIDdic = {} # key is the clusterID, and value is the Athaid
	for l in sourcefile:
		clusterID = l.strip().split("\t")[0].split(".")[0]
		Athaid = l.strip().split("\t")[2]
		Athaid = Athaid.split("@")[1]
		if not clusterID in clusterIDdic.keys():
			clusterIDdic[clusterID] = Athaid
		else: continue
	sourcefile.close()
	#AthaID name with extension
	AllIDlist = clusterIDdic.values()
	#same a background pop for overrepresentation analyses
	"""out = open(inDIR+"AthaIDfor8332dupl.txt","w")
	out.write(">AllIDlist_for_8332\n")
	out.write("\n".join(set(AllIDlist)))"""

	# AthaID name without .extension
	"""AllIDlist = []
	for value in clusterIDdic.values():
		value_name = value.split(".")[0]
		AllIDlist.append(value_name)"""

	for i in os.listdir(inDIR):
		if not i.endswith(file_end): continue
		fileid = i.split('.')[0]
		outname = inDIR+fileid+".undupl"
		outfile = open(outname,"a")
		infile = open(inDIR+i,"rU")
		AthaIDlist = {}
		First = True
		# get undupl id from cluster
		"""
		for line in infile:
			#if len(line.strip()) < 1: continue
			lineID = line.strip().split("\t")[0]
			clusterstr = line.strip().split("\t")[-1]
			clusterlist = clusterstr.split(",")
			AthaIDlist = clusterIDdic.values()
			print AthaIDlist
			print lineID
			for c in clusterlist:
				if not c in clusterIDdic.keys(): 
					#print c
					continue
				AthaIDlist.remove(clusterIDdic[c])
			outfile.write(">"+lineID+"\n"+",".join(set(AthaIDlist))+"\n")
		infile.close()
		outfile.close()"""
		# get undupl athaID directly by filtering out dupl athaID
		for line in infile:
			if line.startswith(">"):
				#print line			
				if First:
					First = False
					name = line.strip()[1:]
					#print name
					IDlist = []
				else:
					#print name
					AthaIDlist[name]=IDlist
					name = line.strip()[1:]
					IDlist = []
				
			else:
				IDlist.append(line.strip())
		AthaIDlist[name]=IDlist
		#print AthaIDlist.keys()
		
		for key in AthaIDlist:
			unduplIDlist = []
			for i in set(AllIDlist):
				if i not in AthaIDlist[key]:
					unduplIDlist.append(i)
			outfile.write(">"+key+"\n"+"\n".join(unduplIDlist)+"\n")
		outfile.close()

