#! /usr/bin/env python

import os,sys
import tree_reader


def add_node(treefile):
    inf = open(treefile,"r")
    oneline = inf.readline().strip()
    tree = tree_reader.read_tree_string(oneline)
    count = 0
    for i in tree.iternodes():
        #if i == tree: continue # build the node labeled figure
        if not i.istip:
            i.label = str(count)
            #count += 1
        count += 1
    inf.close()
    return tree.get_newick_repr(True)+";"
    
if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "python assign_node_label.py inDIR file_end"
        sys.exit(0)
    inDIR,file_end = sys.argv[1:]
    inDIR = os.path.abspath(inDIR)+"/"

    for tre in os.listdir(inDIR):
        if not tre.endswith(file_end): continue
        outname = inDIR+tre+".edit"
        edittree = add_node(inDIR+tre)
        with open(outname,"a") as outfile:
            outfile.write(edittree+"\n")
        outfile.close()
