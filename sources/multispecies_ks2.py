#! /usr/bin/env python

"""
assum you already have cd-hit reduced repeatative for .pep.fa files
named taxon.cd-hit

make sure that synonymous_calc.py and bin from
https://github.com/tanghaibao/bio-pipeline/tree/master/synonymous_calculation
are in the current directory.

blast output file columns (separated by tab):
0-qseqid 1-qlen 2-sseqid 3-slen 4-frames 5-pident 6-nident 7-length 
8-mismatch 9-gapopen 10-qstart 11-qend 12-sstart 13-send 14-evalue 15-bitscore

nident >= 50 number of identity
pident >= 20

#Create a tabular file that each line contains
#code	taxon_name
#separated by tab
"""

import sys,os
from Bio import SeqIO

def get_taxon(seqid):
	return seqid.split("@")[0]


def blastp_2by2(pepfasta1,pepfasta2,num_cores):
	taxon1 = pepfasta1.split(".")[0]
	taxon2 = pepfasta2.split(".")[0]
	out2_cdhit = taxon1+"_"+taxon2+".cd-hit"
	cmd_mkdb1 = "makeblastdb -in "+pepfasta1+" -parse_seqids -dbtype prot -out "+pepfasta1
	os.system(cmd_mkdb1)
	
	cmd_blast1 = "blastp -db "+pepfasta1+" -query "+pepfasta2+" -evalue 10 -num_threads "+str(num_cores)
	cmd_blast1 += " -max_target_seqs 20 -out "+taxon2+".rawblastp "
	cmd_blast1 += "-outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'"
	os.system(cmd_blast1)
	os.system("rm "+pepfasta1+".p*")

	cmd_mkdb2 = "makeblastdb -in "+pepfasta2+" -parse_seqids -dbtype prot -out "+pepfasta2
	os.system(cmd_mkdb2)
	
	cmd_blast2 = "blastp -db "+pepfasta2+" -query "+pepfasta1+" -evalue 10 -num_threads "+str(num_cores)
	cmd_blast2 += " -max_target_seqs 20 -out "+taxon1+".rawblastp "
	cmd_blast2 += "-outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'"
	os.system(cmd_blast2)
	os.system("rm "+pepfasta2+".p*")
	#cat rawblast
	cmd_cat = "cat "+taxon1+".rawblastp "+taxon2+".rawblastp > "+out2_cdhit+".rawblastp"
	os.system(cmd_cat)
	
	#clean blastp delete same-taxon hit and remove overlap A-B and B-A
	wholelist = []
	block = []
	infile = open(out2_cdhit+".rawblastp","r")
	outfile = open(out2_cdhit+".blastp.filtered","w")
	last_query = ""
	for line in infile:
		spls = line.strip().split("\t")
		if len(spls) < 3: continue
		query,hit,pident,nident = spls[0],spls[2],float(spls[5]),int(spls[6])
		query_taxon = get_taxon(query)
		hit_taxon = get_taxon(hit)
		if query_taxon == hit_taxon: continue
		if pident < 20.0 or nident < 50: continue
		wholelist.append((query,hit))
		if query == last_query or block == []:
			if (query,hit) not in block and (hit,query) not in wholelist:
				block.append((query,hit)) # add tuple to block
		else:
			if len(block) < 10:
				for i in block:
					outfile.write(i[0]+"\t"+i[1]+"\n")
			block = []#reset and initiate the block
			block.append((query,hit))	
		last_query = query
	# process the last block
	if len(block) < 10:
		for i in block:
			outfile.write(i[0]+"\t"+i[1]+"\n")
	block = []#reset and initiate the block
	infile.close()
	outfile.close()



if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python multispecies_ks.py num_cores target_taxon subject_taxa_list"
		print "subject_list are IDs separated by ','"
		sys.exit(0)
	
	num_cores,target,subject_list = sys.argv[1:]
	target_cds = target+".cds.fa"
	target_pep = target+".pep.fa"
	target_cdhit = target+".cd-hit"
	subject = subject_list.split(",")
		
	#target = "MJM1156"
	#subject = ["TRS2027","MJM3167","MJM3168","CagrSFB"]
	# assume we already have cd-hit for each species pep.fa file named taxon.cd-hit
	DIR = "./"
	for pep in os.listdir(DIR):
		taxonID = pep.split(".")[0]
		if not taxonID in subject: continue
		if not pep.endswith(".cd-hit"): continue
		out2 = target+"_"+taxonID
		out2_cds = target+"_"+taxonID+".cds.fa"# combined cds.fa
		out2_pep = target+"_"+taxonID+".pep.fa"# combined pep.fa 
		out2_cdhit = target+"_"+taxonID+".cd-hit"
		
		if not os.path.exists(out2_cds):
			cmd1 = "cat "+target_cds+" "+taxonID+".cds.fa > "+out2_cds
			os.system(cmd1)

		if not os.path.exists(out2_pep):
			cmd2 = "cat "+target_pep+" "+taxonID+".pep.fa > "+out2_pep
			os.system(cmd2)

		if not os.path.exists(out2_cdhit+".blastp.filtered"):
			blastp_2by2(target_cdhit,pep,num_cores)
		
		if not os.path.exists(out2+".cds.pairs.fa"):
			cdsDICT = {} #key is seqid, value is seq
			with open(out2_cds,"r") as handle:
				for record in SeqIO.parse(handle,"fasta"):
					cdsDICT[str(record.id)] = str(record.seq)
			pepDICT = {} #key is seqid, value is seq
			with open(out2_pep,"r") as handle:
				for record in SeqIO.parse(handle,"fasta"):
					pepDICT[str(record.id)] = str(record.seq)
			pairs = [] #store tuples that have been processed to avoid repeating
			infile = open(out2_cdhit+".blastp.filtered","r")
			outfile1 = open(out2+".pep.pairs.fa","w")
			outfile2 = open(out2+".cds.pairs.fa","w")
			for line in infile:
				query,hit = line.strip().split("\t")
				pair = (query,hit)
				#pair = (query,hit) if query > hit else (hit,query)
				if pair not in pairs:
					pairs.append(pair)
					outfile1.write(">"+query+"\n"+pepDICT[query]+"\n")
					outfile1.write(">"+hit+"\n"+pepDICT[hit]+"\n")
					outfile2.write(">"+query+"\n"+cdsDICT[query]+"\n")
					outfile2.write(">"+hit+"\n"+cdsDICT[hit]+"\n")
			infile.close()
			outfile1.close()
			outfile2.close()
			print len(pairs),"sequence pairs wirtten"
		
		if not os.path.exists(out2+".ks"):
			cmd = "python synonymous_calc.py "+out2+".pep.pairs.fa "+out2+".cds.pairs.fa >"+out2+".ks"
			print cmd
			os.system(cmd)
			#os.system("python report_ks.py "+out2+".ks")

		#parse the ks output
		#0-name,1-dS-yn,2-dN-yn,3-dS-ng,4-dN-ng
		if not os.path.exists(out2+".ks.yn") or not os.path.exists(out2+".ks.ng"):
			first = True
			infile = open(out2+".ks","r")
			outfile1 = open(out2+".ks.yn","w")
			outfile2 = open(out2+".ks.ng","w")
			for line in infile:
				if first:
					first = False
					continue
				spls = line.strip().split(",")
				yn,ng = float(spls[1]),float(spls[3])
				if yn > 0.01 and yn < 3.0:
					outfile1.write(spls[1]+"\n")
				if ng > 0.01 and ng < 3.0:
					outfile2.write(spls[3]+"\n") 
			infile.close()
			outfile1.close()
			outfile2.close()


