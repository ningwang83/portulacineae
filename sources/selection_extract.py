#! /usr/bin/env python

import os,sys

""" read the .table file and extract selection results """

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python "+sys.argv[0]+" WORKDIR file_end"
		sys.exit(0)
	WORKDIR,file_end= sys.argv[1:]
	WORKDIR = os.path.abspath(WORKDIR)
	outname = os.path.join(WORKDIR,"summary_selection.txt")
	outfile = open(outname,"w")
	outfile.write("cluster"+"\t"+"Positive_N"+"\t"+"Negative_N"+"\t"+"dN-dS"+"\t"+"scaled_dN-dS"+"\t"+"sitesN"+"\n")
	for f in os.listdir(WORKDIR):
		if not f.endswith(file_end): continue
		clusterID = f.split("_")[0]
		site = 0
		count_PP = 0 # number of positive selection with p<0.05
		count_PN = 0
		Total_dNmdS = 0
		Total_scaled_dNmdS = 0
		infile = open(f,"rU")
		First = True
		for l in infile:
			if First: 
				First = False
				continue
			line = l.strip().split(",")
			# print line
			dNmdS,PP,PN,scaled_dNmdS = line[-4:]
			Total_dNmdS += round(float(dNmdS),3)
			Total_scaled_dNmdS += round(float(scaled_dNmdS),3)
			if float(dNmdS) != 0: site += 1
			if float(PP) > 0 and float(PP) < 0.05:
				count_PP += 1
			if float(PN) > 0 and float(PN) < 0.05:
				count_PN += 1
		outfile.write(clusterID+"\t"+str(count_PP)+"\t"+str(count_PN)+"\t"+str(Total_dNmdS)+"\t"+str(Total_scaled_dNmdS)+"\t"+str(site)+"\n")
		infile.close()
	outfile.close()
		
