"""
Output total number of tips, number of taxa, or in each major clades and the taxa with max repeat
"""

import phylo3,newick3,os,sys

Cacall35 = ["Lely", "Lebl", "JLOV", "Pegr", "CPKP", "Ecpe", "Noco", "MJM2911", "MJM2938", "Tali11", "Gymi06", "Tuco07", "Asmy08", "Ecau09", "Tear12", "Maau13", "Stco14", "Asas15", "Opbr17", "Ceyu18", "Arre20", "Pttu21", "Paga22", "Mapo24", "Sasa26", "Coma28", "Pecu29", "Erwa30", "Legu31", "Fela32", "Grbr1013", "Code1013", "Maco1013", "Pubo1013"]
Cac39 = ["Pubo1013", "Tear12", "Maco1013", "Pttu21", "Tali11", "Sasa26", "Tuco07", "MJM2911", "Noco", "Opbr17", "Grbr1013"]
Cac50 = ["Gymi06", "Asmy08", "Ecau09", "Maau13", "Stco14", "Asas15", "Ceyu18", "Arre20", "Paga22", "Coma28", "Pecu29", "Erwa30", "Fela32", "Code1013", "CPKP", "Ecpe", "MJM2938"]

Outgroup = ["AniSFB", "LimaeSFB", "DBGStha", "HZTS", "BJKT", "SFB27", "Beta"]

Montiaceae14 = ["CigrSFB", "CagrSFB", "MJM3167", "MJM3168", "MJM3165", "MJM2214", "MJM1156", "TRS2027", "MJM3142"]
Montia15 = ["CigrSFB", "CagrSFB", "MJM3167", "MJM3168", "MJM3165", "MJM1156", "TRS2027", "MJM3142"]


Portulacaceae71 = ["LDEL", "LLQV", "CPLT", "UQCB", "EZGR", "IWIS", "GCYL", "KDCH"]

Talinaceae = ["Tafr", "MJM1789", "Tapo"]

Molluginaceae7 = ["SuecaSFB", "PhaexSFB", "RNBN", "SCAO", "HURS", "NXTS", "GliloSFB"]

Didiereaceae25 = ["DBG19565744023G", "DBG294800240202G", "MJM2944", "DBG198703010201Z", "DBG19880583021G", "DBG19740229011", "AlmarSFB", "CerpySFB", "DBGDima"]
Didier26 = ["CerpySFB", "DBG19880583021G"]
Didier27 = ["DBG19565744023G", "DBG294800240202G", "MJM2944", "DBG198703010201Z", "DBG19740229011", "AlmarSFB", "DBGDima"]

Anacampserotaceae = ["MJM2441", "GrakuSFB","Anfi"] 

Basellaceae = ["AncoSFB","CTYH"]


#if pattern changes, change it here
#given tip label, return taxon name identifier
def get_name(label):
	return label.split("@")[0]

def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]
	
def get_front_names(node): #may include duplicates
	labels = get_front_labels(node)
	return [get_name(i) for i in labels]

	

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python count_taxon_repeats.py cdsall82_fourth outFILE"
		sys.exit(0)

	#score taxon counts from all inclades and write to a summary file
	DIR = sys.argv[1]+"/"
	outfile = open(sys.argv[2],"w")
	outfile.write("clusterID\tnum_tips\tnum_taxa\tmaxtaxon\tcount\tcladename\tave_repeat\ttaxanum\n")
	for treefile in os.listdir(DIR):
		if not treefile.endswith(".tre"):continue
		print treefile
		with open(DIR+treefile,"r") as infile:
			 intree = newick3.parse(infile.readline())#one tree in each file
		names = get_front_names(intree)
		infile.close()
		#uniqenames = set(names)
		nameDICT = {} #key is name, value is count
		for name in names:
			if name not in nameDICT:
				nameDICT[name] = 1
			else: nameDICT[name] += 1
		outfile.write(treefile+"\t"+str(len(names))+"\t"+str(len(nameDICT))+"\t")
		# tip,taxa numbers
		Cac35num = [0,0]
		Cac39num = [0,0]
		Cac50num = [0,0]
		outnum = [0,0]
		Mont14num = [0,0]
		Mont15num = [0,0]
		Port71num = [0,0]
		Talnum = [0,0]
		Moll7num = [0,0]
		Didier25num = [0,0]
		Didier26num = [0,0]
		Didier27num = [0,0]
		Ananum = [0,0]
		Basenum = [0,0]
		max_count = ["",0] #keep track of the highest taxon repeats in any gene
		for name in nameDICT:
			count = nameDICT[name]
			#outfile.write(name+":"+str(count)+",")
			if name in Cacall35:
				Cac35num[0] += count
				Cac35num[1] += 1
				if name in Cac39:
					Cac39num[0] += count
					Cac39num[1] += 1
				elif name in Cac50:
					Cac50num[0] += count
					Cac50num[1] += 1
			elif name in Outgroup:
				outnum[0] += count
				outnum[1] += 1
			elif name in Montiaceae14:
				Mont14num[0] += count
				Mont14num[1] += 1
				if name in Montia15:
					Mont15num[0] += count
					Mont15num[1] += 1
			elif name in Portulacaceae71:
				Port71num[0] += count
				Port71num[1] += 1
			elif name in Talinaceae:
				Talnum[0] += count
				Talnum[1] += 1
			elif name in Molluginaceae7:
				Moll7num[0] += count
				Moll7num[1] += 1
			elif name in Didiereaceae25:
				Didier25num[0] += count
				Didier25num[1] += 1
				if name in Didier26:
					Didier26num[0] += count
					Didier26num[1] += 1
				elif name in Didier27:
					Didier27num[0] += count
					Didier27num[1] += 1
			elif name in Anacampserotaceae:
				Ananum[0] += count
				Ananum[1] += 1
			elif name in Basellaceae:
				Basenum[0] += count
				Basenum[1] += 1
			if count > max_count[1]:
				max_count[0] = name
				max_count[1] = count
		outfile.write(max_count[0]+"\t"+str(max_count[1])+"\t")
		cladedict = {}
		for i in ["Cac35num","Cac39num","Cac50num","outnum","Mont14num","Mont15num","Port71num","Talnum","Moll7num","Didier25num","Didier26num","Didier27num","Ananum","Basenum"]:
			cladedict[i] = locals()[i]# key is the list items, values is the corresponding list for each item
		max_clade_ave = ["",0,0]
		for key in cladedict:
			if cladedict[key][1] > 0:
				aver_repeat = round(float(cladedict[key][0]/cladedict[key][1]),3)
				if aver_repeat > max_clade_ave[1]:
					max_clade_ave[0] = key
					max_clade_ave[1] = aver_repeat
					max_clade_ave[2] = cladedict[key][1]
		outfile.write(max_clade_ave[0]+"\t"+str(max_clade_ave[1])+"\t"+str(max_clade_ave[2])+"\n")
	outfile.close()
	
