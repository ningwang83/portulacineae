#! /usr/bin/env python


import os,sys

db = "/home/ningwum/Desktop/Sample_pep_cds/Go_analyses/Atha.cds.database.fa"

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python "+sys.argv[0]+" inDIR file_end num_thread"
		print "file_end with .fas"
		sys.exit(0)
	inDIR,file_end,thread_num = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	for i in os.listdir(inDIR):
		if not i.endswith(file_end): continue
		idname = i.split("_")[0]
		cmd = "blastn -db "+db+" -query "+inDIR+i+" -evalue 10 -num_threads "+str(thread_num)+" -max_target_seqs 1 -out "+\
			inDIR+idname+".blastn -outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart \
			qend sstart send evalue bitscore'"
		print cmd
		os.system(cmd)

