#! /usr/bin/env python

import os,sys

#gene_list = ["cluster3964", "cluster6138", "cluster1037", "cluster2530", "cluster579", "cluster4238", "cluster4106", "cluster1418", "cluster1248", "cluster5640", "cluster5930", "cluster7152", "cluster3042", "cluster2531", "cluster6422", "cluster943", "cluster286", "cluster4535", "cluster989", "cluster8059", "cluster584", "cluster708", "cluster3368", "cluster1349", "cluster1181", "cluster837", "cluster1131", "cluster1930", "cluster7414"]

gene_list = ["cluster3299", "cluster3952", "cluster4", "cluster4229", "cluster4408", "cluster5187", "cluster5284", "cluster572", "cluster6245", "cluster6306", "cluster6484", "cluster6538", "cluster6790", "cluster6843", "cluster6902", "cluster7152", "cluster7257", "cluster7481", "cluster8358", "cluster1007", "cluster1082", "cluster2509", "cluster2622", "cluster3455", "cluster4518", "cluster7042", "cluster7069", "cluster7708", "cluster777", "cluster8011", "cluster887", "cluster1202", "cluster2942", "cluster3261", "cluster3535", "cluster3552", "cluster5317", "cluster6641", "cluster7895", "cluster6766", "cluster3700", "cluster5698", "cluster910", "cluster8231", "cluster8305", "cluster5208", "cluster8059", "cluster704", "cluster5753", "cluster6864", "cluster1864", "cluster5012", "cluster5836", "cluster8111", "cluster1730", "cluster3537", "cluster7025", "cluster7953", "cluster8400", "cluster90", "cluster5126", "cluster5711", "cluster10", "cluster1697", "cluster3", "cluster3127", "cluster5178", "cluster7877", "cluster4535", "cluster1532", "cluster6218", "cluster7227", "cluster8261", "cluster3548", "cluster3183", "cluster1169", "cluster3863", "cluster3502", "cluster733", "cluster939", "cluster1572", "cluster2368", "cluster3461", "cluster4092", "cluster4460", "cluster4728", "cluster5503", "cluster584", "cluster7138", "cluster1986", "cluster708", "cluster1014", "cluster308", "cluster525", "cluster6901", "cluster2530", "cluster1545", "cluster869", "cluster7867", "cluster1521", "cluster2531", "cluster3475", "cluster5683", "cluster579", "cluster6422", "cluster2383", "cluster3349", "cluster8234", "cluster1105", "cluster1599", "cluster2865", "cluster3865", "cluster5692", "cluster5929", "cluster799", "cluster839", "cluster1964", "cluster1192", "cluster4031", "cluster6783", "cluster780", "cluster91", "cluster3574", "cluster818", "cluster4775", "cluster7225", "cluster8", "cluster4238", "cluster1646", "cluster8072", "cluster3241", "cluster3528", "cluster6553", "cluster2380", "cluster4631", "cluster2323", "cluster3550", "cluster6798", "cluster989", "cluster1226", "cluster6475", "cluster3084", "cluster491", "cluster81", "cluster2754", "cluster3072", "cluster2769", "cluster2920", "cluster589", "cluster221", "cluster3253", "cluster153", "cluster7809", "cluster3351", "cluster2584", "cluster3931", "cluster3364", "cluster4470", "cluster561", "cluster4382", "cluster1346", "cluster1172", "cluster1167", "cluster335", "cluster2110", "cluster23", "cluster8310", "cluster4577", "cluster7391", "cluster788", "cluster3368", "cluster4909", "cluster6087", "cluster228", "cluster170", "cluster1555", "cluster308", "cluster6972", "cluster4201", "cluster1541", "cluster6189", "cluster5811", "cluster2643", "cluster3711", "cluster1189", "cluster4555", "cluster6305", "cluster8093", "cluster523", "cluster2230", "cluster5651", "cluster59", "cluster7730", "cluster3457", "cluster5356", "cluster5640", "cluster3053", "cluster1928", "cluster2318", "cluster8421", "cluster7814", "cluster3966", "cluster1785", "cluster4362", "cluster1143", "cluster700", "cluster384", "cluster4106", "cluster2215", "cluster5674", "cluster2421", "cluster1828", "cluster1349", "cluster7703", "cluster2946", "cluster2888", "cluster5079", "cluster1914", "cluster2699", "cluster3094", "cluster112", "cluster3826", "cluster854", "cluster810", "cluster582", "cluster2262", "cluster5486", "cluster1418", "cluster5930", "cluster5455", "cluster120", "cluster731", "cluster2540", "cluster270", "cluster28", "cluster7914", "cluster4606", "cluster5894", "cluster4663", "cluster1952", "cluster3612", "cluster1230", "cluster570", "cluster5868", "cluster4921", "cluster3964", "cluster943", "cluster5374", "cluster1733", "cluster1364", "cluster5833", "cluster286", "cluster2157", "cluster6138", "cluster1074", "cluster548", "cluster1930", "cluster4490", "cluster2391", "cluster1238", "cluster7341", "cluster187", "cluster6979", "cluster756", "cluster355", "cluster6829", "cluster1037", "cluster2", "cluster547", "cluster2378", "cluster5", "cluster98", "cluster944", "cluster632", "cluster721", "cluster1181", "cluster2903", "cluster54", "cluster5054", "cluster3485", "cluster3845", "cluster2546", "cluster1917", "cluster577", "cluster643", "cluster3233", "cluster6882", "cluster3144", "cluster3042", "cluster567", "cluster2539", "cluster1248", "cluster2012", "cluster441", "cluster2289", "cluster2017", "cluster743", "cluster931", "cluster5911", "cluster1164", "cluster4046", "cluster179", "cluster342", "cluster21", "cluster656", "cluster1225", "cluster458", "cluster192", "cluster1", "cluster37"]

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: "+sys.argv[0]+" DIR outfile(get_dup_node_for_gene)"
		sys.exit()
	DIR,outname = sys.argv[1:]
	DIR = os.path.abspath(DIR)+"/"
	Cluster = {}
	for f in os.listdir(DIR):
		if not "dupl" in f: continue
		if f.endswith(".tre"): continue
		nodeID = f.split(".")[-1]
		nodenum = "Node"+str(nodeID)
		with open(f,"rU") as infile:
			for line in infile:
				clusterID = line.split(".")[0].split("/")[-1]
				clu = clusterID.split("_")[0]
		                if not clu in Cluster.keys():
					Cluster[clu] = []
				if clu in gene_list:
					Cluster[clu].append(nodenum)
	outfile = open(DIR+outname,"w")
	for k in Cluster.keys():
		if len(set(Cluster[k])) > 0:
			outfile.write(k+"\t"+",".join(set(Cluster[k]))+"\n")
	outfile.close()


