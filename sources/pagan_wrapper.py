import os,sys

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "usage: python pagan_wrapper.py inDIR outDIR infile_ending thread"
		sys.exit()
	
	inDIR,outDIR,file_end,thread = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	outDIR = os.path.abspath(outDIR)+"/"
	done = os.listdir(outDIR)
	
	for i in os.listdir(inDIR):
		if not i.endswith(file_end): continue
		if i+".aln" in done: continue
		clusterID = i.split(".")[0]
		
		out = outDIR+clusterID+".aln"
		cmd = "pagan -s "+inDIR+i+" -o "+out+" --threads "+thread
		print cmd
		os.system(cmd)
		os.system("mv "+outDIR+clusterID+".aln.fas "+out)
		try:
			os.remove(inDIR+"warnings")
			os.remove(out+".tre")
		except: pass 
