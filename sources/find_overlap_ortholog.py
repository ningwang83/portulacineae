#! /usr/bin/env python

import os,sys

def overlap(backgroundlist,comparelist):
	overlaplist = []
	for i in comparelist:
		if i in backgroundlist: # background list is the list of all ortholog tree file name
			overlaplist.append(i)
	return overlaplist

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python "+sys.argv[0]+" INDIR background filename"
		print "filename separate with comma, files are concord.node.#"
		print "background.txt is in the selection folder, given full path of that file"
		sys.exit(0)
	INDIR = sys.argv[1]
	INDIR = os.path.abspath(INDIR)+"/"
	background_file = sys.argv[2]
	filename = sys.argv[3]
	filelist = filename.split(",")
	# get the background list of trees, call it overlap_list
	overlap_list = []
	back_file = open(background_file,"rU")
	for l in back_file:
		treename = l.strip().split("/")[-1]
		treeID = treename.split(".")[0]
		overlap_list.append(treeID)
	back_file.close()

	# get comparelist
	for f in os.listdir(INDIR):
		if not f in filelist: continue
		comparelist = []
		with open(f,"rU") as infile:
			for li in infile:
				treename = li.strip().split("/")[-1]
				treeID = treename.split(".")[0]
				comparelist.append(treeID)
		infile.close()
		overlap_list = overlap(overlap_list,comparelist)
	print overlap_list

"""
example
python ~/Documents/Transcriptome_bird/Transcriptome_code/find_overlap_ortholog.py . '/home/ningwum/Desktop/Sample_pep_cds/Selection_analyses/background.txt' Best_conflict70.concord.node.2,Best_conflict70.concord.node.6,Best_conflict70.concord.node.7,Best_conflict70.concord.node.13,Best_conflict70.concord.node.14,
Best_conflict70.concord.node.15,Best_conflict70.concord.node.22,Best_conflict70.concord.node.24,Best_conflict70.concord.node.25,Best_conflict70.concord.node.27,
Best_conflict70.concord.node.33,Best_conflict70.concord.node.34,Best_conflict70.concord.node.35,Best_conflict70.concord.node.68,Best_conflict70.concord.node.69,
Best_conflict70.concord.node.71,Best_conflict70.concord.node.39,Best_conflict70.concord.node.50
result in: ['cluster5788_1rr_1rr_1', 'cluster5644_1rr_1rr_1']

python ~/Documents/Transcriptome_bird/Transcriptome_code/find_overlap_ortholog.py . '/home/ningwum/Desktop/Sample_pep_cds/Selection_analyses/background.txt' Best_conflict70.concord.node.6,Best_conflict70.concord.node.7,Best_conflict70.concord.node.13,Best_conflict70.concord.node.14,Best_conflict70.concord.node.15,
Best_conflict70.concord.node.25,Best_conflict70.concord.node.27,Best_conflict70.concord.node.34,Best_conflict70.concord.node.35,Best_conflict70.concord.node.68,
Best_conflict70.concord.node.69,Best_conflict70.concord.node.71,Best_conflict70.concord.node.39,Best_conflict70.concord.node.50,Best_conflict70.concord.node.33
result in: ['cluster6010_1rr_1rr_1', 'cluster7549_1rr_1rr_1', 'cluster8202_1rr_1rr_1', 'cluster8077_1rr_1rr_1', 'cluster5788_1rr_1rr_1', 'cluster6136_1rr_1rr_1', 'cluster5607_1rr_1rr_1', 'cluster3275_1rr_1rr_1', 'cluster2613_1rr_1rr_1', 'cluster5770_1rr_1rr_1', 'cluster4531_1rr_1rr_1', 'cluster6933_1rr_1rr_1', 'cluster4545_1rr_1rr_1', 'cluster7077_1rr_1rr_1', 'cluster5041_1rr_1rr_1', 'cluster7624_1rr_1rr_1', 'cluster5126_1rr_1rr_1', 'cluster4722_1rr_1rr_1', 'cluster5644_1rr_1rr_1', 'cluster3712_1rr_1rr_1', 'cluster7432_1rr_1rr_1', 'cluster7832_1rr_1rr_1', 'cluster5528_1rr_1rr_1']
"""
