#! /usr/bin/env python

import os,sys
import tree_reader
from node import Node
import tree_utilities

"""use this to extract the diversification shift node on the pruned diversification tree
node.note is like &r.mean=number"""


if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python extract_diversification_shift.py div_tree outname"
		sys.exit(0)
	
	divtre,outname = sys.argv[1:]
	curDIR = os.getcwd()+"/"
	
	# read pruned trees and write node.note to files
	outfile = open(curDIR+outname,"w")
	outfile.write("cladetaxa\tMeanshift\n")
	infile = open(curDIR+divtre,"rU")
	curroot = tree_reader.read_tree_string(infile.readline().strip())
	for node in curroot.iternodes("postorder"):
		if node.istip or node == curroot: continue
		divrate = float(node.note.split("=")[-1])
		parnote = node.parent.note
		parrate = float(parnote.split("=")[-1])
		if divrate != parrate:
			rate_shift = divrate - parrate
			nodeleave = [n.label for n in node.leaves()]
			outfile.write(",".join(nodeleave)+"\t"+str(rate_shift)+"\n")
	outfile.close()



