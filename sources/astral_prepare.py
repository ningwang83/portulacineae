#! /usr/bin/env python

from seq import read_fasta_file
import sys,os

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "usage: python astral_prepare.py aln_clnDIR astralDIR numofsitesFilter numoftaxaFilter"
		sys.exit()	
	clnDIR,astralDIR,numofsitesFilter,numoftaxaFilter= sys.argv[1:]
	sites_filter = int(numofsitesFilter)
	taxa_filter = int(numoftaxaFilter)
	clnDIR = os.path.abspath(clnDIR) + "/"
	astralDIR = os.path.abspath(astralDIR) + "/"
	for i in os.listdir(clnDIR):
		if i.endswith(".aln_cln"):
			seqlist = read_fasta_file(clnDIR+i)
			num_seq = len(seqlist)
			num_col = len(seqlist[0].seq)
			print i+"\t"+str(num_seq)+"\t"+str(num_col) # statistics to see
			if num_seq >= taxa_filter and num_col >= sites_filter:
				cmd = "cp "+clnDIR+i+" "+astralDIR
				os.system(cmd)

