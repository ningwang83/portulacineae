#! /usr/bin/env python

"""
read in .blastn file and correspond clusterID to AthaID
convert the clusterID to AthaID so can be used in the Panther Go analyses
"""

import os,sys

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python "+sys.argv[0]+" inDIR source_file file_end"
		print "file_end with .out, source_file is the blastn file"
		sys.exit(0)
	
	inDIR,source_file,file_end = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	source = os.path.join(os.getcwd(),source_file)
	
	#read in source file and built cluster to AthaID dictionary
	sourcefile = open(source,"rU")
	clusterIDdic = {} # key is the clusterID, and value is the Athaid
	for l in sourcefile:
		clusterID = l.strip().split("\t")[0].split(".")[0]
		Athaid = l.strip().split("\t")[2]
		Athaid = Athaid.split("@")[1]
		if not clusterID in clusterIDdic.keys():
			clusterIDdic[clusterID] = [Athaid]
		else:
			if Athaid not in clusterIDdic[clusterID]:
				clusterIDdic[clusterID].append(Athaid)
	sourcefile.close()

	for i in os.listdir(inDIR):
		if not i.endswith(file_end): continue
		fileid = i.split('.')[0]
		outname = inDIR+fileid+".AthaID"
		outfile = open(outname,"a")
		infile = open(inDIR+i,"rU")
		for line in infile:
			#if len(line.strip()) < 1: continue
			cluID = line.strip().split("\t")[0]
			cluID = cluID.strip().split(".")[0]
			AthaIDstr = ""
			if cluID in clusterIDdic:
				AthaID = clusterIDdic[cluID]
				AthaIDstr = ",".join(AthaID)
			else: AthaIDstr = ""
			outfile.write(line.strip()+"\t"+AthaIDstr+"\n")
		infile.close()
		outfile.close()	

