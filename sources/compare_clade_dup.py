#! /usr/bin/env python

import os,sys

def overlap(list1,list2):
	overlap = []
	for i in list1:
		if i in list2:
			overlap.append(i)
	return overlap

def differ(list1,list2):
	differ1 = []
	differ2 = []
	for i in list1:
		if not i in list2:
			differ1.append(i)
	for j in list2:
		if not j in list1:
			differ2.append(j)
	return differ1, differ2
	

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: compare_clade_dup.py files_list outfile.out"
		print "cd into current working directory"
		sys.exit()
	DIR = os.getcwd()+"/"
	files,outname = sys.argv[1:]
	files_list = files.split(",")
	Cluster = {}
	for f in files_list:
		nodeID = f.split(".")[-1]
		listcluster = []
		with open(f,"rU") as infile:
			for line in infile:
				clusterID = line.split(".")[0].split("/")[-1]
				treeID = line.split(".")[-3]
				listcluster.append(clusterID+"_"+treeID)
		Cluster["node"+nodeID] = (listcluster)
		infile.close()
	overlap_clu = []
	First = "TRUE"
	for k in Cluster:
		if First == "TRUE":
			overlap_clu = Cluster[k]
			First = "FALSE"
		else:
			overlap_clu = overlap(Cluster[k],overlap_clu)
	clusterstr = ",".join(overlap_clu)
	outfile = open(DIR+outname,"w")
	#outfile.write("overlap\t"+clusterstr+"\n")

	#if Cluster only have two keys
	Sort_keys = sorted(Cluster)
	for k in range(len(Sort_keys)):
		for l in range(len(Sort_keys)):
			if l > k:
				differ1,differ2 = differ(Cluster[Sort_keys[k]],Cluster[Sort_keys[l]])
				Ovlap = overlap(Cluster[Sort_keys[k]],Cluster[Sort_keys[l]])
				outfile.write(Sort_keys[k]+"_unique_"+Sort_keys[l]+"\t"+",".join(differ1)+"\n")
				outfile.write(Sort_keys[k]+"_"+Sort_keys[l]+"_unique\t"+",".join(differ2)+"\n")
				outfile.write(Sort_keys[k]+"_"+Sort_keys[l]+"_overlap\t"+",".join(Ovlap)+"\n")

	outfile.close()



