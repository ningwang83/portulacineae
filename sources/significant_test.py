#! /usr/bin/env python

import os,sys
import random
import pygal

nodes = [13,15,17,27,35,39,50,71] # the examine nodes

def check_num_overlap(list1,mainlist):
	count = 0
	for i in list1:
		if i in mainlist:
			count += 1
	return count


if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "usage: python significant_test.py treeDIR dupDIR tree_end dup_start"
		print "tree_end with .rr dup_start with all8544_80.dupl or all8544.dupl"
		sys.exit()
	
	treeDIR,dupDIR,tree_end,dup_start = sys.argv[1:]
	treeDIR = os.path.abspath(treeDIR)+"/"
	dupDIR = os.path.abspath(dupDIR)+"/"
	curDIR = os.getcwd()+"/"
	
	# setup the gene list
	gene_list = []
	for gene in os.listdir(treeDIR):
		if not gene.endswith(tree_end): continue
		gene_list.append(gene)
	
	# open an outfile and write randomly selected genes, 1000 times
	outfile = open(curDIR+"selected_genes.out","w")
	select_dic = {}
	for i in range(1000):
		genes29 = random.sample(gene_list,29)
		outfile.write(str(i)+"\t"+"\t".join(genes29)+"\n")
		select_dic[i] = genes29
	outfile.close()
	
	# read duplication results, check the number of overlap and made distribution
	node_dup_dic = {}
	for node in os.listdir(dupDIR):
		if not node.startswith(dup_start): continue
		if node.endswith(".tre"): continue
		node_num = node.split(".")[-1]
		if int(node_num) in nodes:
			count_list = []
			list_tree = []
			with open(dupDIR+node,"rU") as handle:
				for l in handle:
					gene_name = l.strip().split("/")[-1]
					list_tree.append(gene_name)
			for k in select_dic.keys():
				count_k = check_num_overlap(select_dic[k],list_tree)
				count_list.append(count_k)
			node_dup_dic[node_num]=count_list
	with open(curDIR+"nodedup_count.out","w") as outfile:
		for node in node_dup_dic.keys():
			outfile.write(str(node)+"\t"+"\t".join(map(str,node_dup_dic[node]))+"\n")
	
	# use pagal to build distribution histogram
	for node in node_dup_dic.keys():
		counts = node_dup_dic[node]
		frequencies = []
		for value in range(30):
			frequency = counts.count(value)
			frequencies.append(frequency)
		hist = pygal.Bar()
		hist.title = "Results from 1000 repeats"
		hist.x_labels = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29"]
		hist.x_title = "Number of genes"
		hist.y_title = "frequency"
		hist.add(node,frequencies)
		hist.render_to_file(curDIR+node+".dupfreq.svg")
		
			

