#! /usr/bin/env python

import os,sys
from subprocess import call
from assemble import *


TruSeq_ADAPTER = "/home/ningwum/Documents/Repo/phylogenomic_dataset_construction/data/TruSeq_adapters" # my path for plant adapter


if __name__ =="__main__":
	if len(sys.argv) != 3:
		print "usage: python trim_adapter.py DIRlist Num_thread"
		print "DIRlist seperate by ','"
		sys.exit()
	
	DIRs,Num_thread = sys.argv[1:3]

	Dic_fastq = fq_dictionary(DIRs.split(","),\
			read1_identifier="R1",\
			read2_identifier="R2",\
			infile_end=".fq")
	# trim adapter
	trim_adapter(Dic_fastq,Num_thread,TruSeq_ADAPTER)


