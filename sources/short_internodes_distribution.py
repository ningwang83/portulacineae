#! /usr/bin/env python 

""" use this script to calculate internodes distribution for certain clades """

import os,sys
import tree_reader

Clade_dict = {}

Clade_dict["Cac35"] = ["Lely", "Lebl", "JLOV", "Pegr", "CPKP", "Ecpe", "Noco", "MJM2911", "MJM2938", "Tali11", "Gymi06", "Tuco07", "Asmy08", "Ecau09", "Tear12", "Maau13", "Stco14", "Asas15", "Opbr17", "Ceyu18", "Arre20", "Pttu21", "Paga22", "Mapo24", "Sasa26", "Coma28", "Pecu29", "Erwa30", "Legu31", "Fela32", "Grbr1013", "Code1013", "Maco1013", "Pubo1013"]
Clade_dict["Cac39"] = ["Pubo1013", "Tear12", "Maco1013", "Pttu21", "Tali11", "Sasa26", "Tuco07", "MJM2911", "Noco", "Opbr17", "Grbr1013"]
Clade_dict["Cac50"] = ["Gymi06", "Asmy08", "Ecau09", "Maau13", "Stco14", "Asas15", "Ceyu18", "Arre20", "Paga22", "Coma28", "Pecu29", "Erwa30", "Fela32", "Code1013", "CPKP", "Ecpe", "MJM2938"]

Clade_dict["Didier27"] = ["DBG19565744023G", "DBG294800240202G", "MJM2944", "DBG198703010201Z", "DBG19740229011", "AlmarSFB", "DBGDima"]

# for comparison with short internodes
Clade_dict["Montia14"] = ["CigrSFB", "CagrSFB", "MJM3167", "MJM3168", "MJM3165", "MJM2214", "MJM1156", "TRS2027", "MJM3142"]

Clade_dict["Portulacaceae71"] = ["LDEL", "LLQV", "CPLT", "UQCB", "EZGR", "IWIS", "GCYL", "KDCH"]

Clade_dict["Mollug7"] = ["SuecaSFB", "PhaexSFB", "RNBN", "SCAO", "HURS", "NXTS", "GliloSFB"]


def get_name(label):
	return label.split("@")[0]
	
def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]

def get_front_names(node):
	labels = get_front_labels(node)
	return [get_name(i) for i in labels]

def dif_list(treenode_leaves,clade_list):
	dif = []
	for i in treenode_leaves:
		if not i in clade_list:
			dif.append(i)
	return len(dif)

def node_length(node):
	length = 0
	count = 0
	for n in node.iternodes():
		if n == node or n.istip: continue
		length += round(float(n.length),4)
		count += 1
	if count != 0:
		ave_length = round(length/count, 4)
	else: ave_length = 0.0
	return ave_length

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python "+sys.argv[0]+" inDIR file_end"
		sys.exit()	
	inDIR,file_end = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	outresult = inDIR+"summarize.out"
	outfile = open(outresult,"w")
	outfile.write("orthologID\tMeanTotal\tCac35\tCac39\tCac50\tDid27\tMollug7\tMont14\tPortu71\n")
	for f in os.listdir(inDIR):
		if not f.endswith(file_end): continue
		#print f
		clusterID = f.split(".")[0]
		outfile.write(clusterID+"\t")
		infile = open(inDIR+f,"rU")
		treeline = infile.readline().strip()
		root = tree_reader.read_tree_string(treeline)
		root_length = node_length(root)
		outfile.write(str(root_length)+"\t")
		Clade_avelen_dict = {}
		for n in root.iternodes():
			if n.istip or n == root: continue
			node_leaves = [i.label for i in n.leaves()]
			#node_leaves = get_front_names(n)
			#print node_leaves
			p = n.parent
			parent_leaves = [i.label for i in p.leaves()]
			#print parent_leaves
			#parent_leaves = get_front_names(p)
			for k in sorted(Clade_dict.keys()):
				len_n = dif_list(node_leaves,Clade_dict[k])
				len_p = dif_list(parent_leaves,Clade_dict[k])
				if len_n == 0 and len_p != 0:
					ave_length = node_length(n)
					Clade_avelen_dict[k] = ave_length
					#print k+"\t"+str(ave_length)
					break

			if len(Clade_avelen_dict.keys()) == 7:
				break
		for ke in Clade_dict.keys():
			if not ke in Clade_avelen_dict.keys():
				print ke
				Clade_avelen_dict[ke] = 0
		for key in sorted(Clade_avelen_dict.keys()):
			outfile.write(str(Clade_avelen_dict[key])+"\t")
		outfile.write("\n")
	outfile.close()
