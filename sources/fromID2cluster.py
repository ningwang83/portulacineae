#! /usr/bin/env python

import os,sys

#IDlist = ["AT2G27210", "AT2G45670", "AT2G27030", "AT1G29320", "AT4G17270", "AT2G27030", "AT2G29400", "AT2G27030", "AT1G56340", "AT3G19980", "AT3G08510", "AT1G59830", "AT3G50360", "AT5G05540", "AT1G64040", "AT5G48960", "AT3G02750", "AT3G07400", "AT1G55870", "AT3G52390", "AT1G07630", "AT1G50840", "AT5G37710", "AT3G16785", "AT1G08510", "AT1G31480", "AT1G07110"]
IDlist = ["AT1G01560", "AT1G04120", "AT1G04410", "AT1G04640", "AT1G07110", "AT1G07630", "AT1G08510", "AT1G08550", "AT1G08570", "AT1G08960", "AT1G09340", "AT1G09640", "AT1G09780", "AT1G10570", "AT1G10940", "AT1G12420", "AT1G15360", "AT1G18890", "AT1G21630", "AT1G21750", "AT1G29320", "AT1G31480", "AT1G31930", "AT1G35515", "AT1G49300", "AT1G49820", "AT1G50010", "AT1G50840", "AT1G54450", "AT1G55870", "AT1G56340", "AT1G56600", "AT1G58440", "AT1G59830", "AT1G60490", "AT1G63940", "AT1G64040", "AT1G65930", "AT1G66200", "AT1G67280", "AT1G74520", "AT2G04030", "AT2G05830", "AT2G13560", "AT2G14720", "AT2G16280", "AT2G16500", "AT2G17290", "AT2G18790", "AT2G25140", "AT2G26650", "AT2G27030", "AT2G27210", "AT2G29400", "AT2G34860", "AT2G38470", "AT2G39460", "AT2G39760", "AT2G40950", "AT2G43090", "AT2G43360", "AT2G43790", "AT2G45670", "AT2G46700", "AT3G02750", "AT3G02870", "AT3G03110", "AT3G05530", "AT3G07400", "AT3G08510", "AT3G09600", "AT3G11170", "AT3G12580", "AT3G16785", "AT3G19100", "AT3G19980", "AT3G46820", "AT3G50360", "AT3G51850", "AT3G52390", "AT3G54050", "AT3G55990", "AT3G56490", "AT3G59760", "AT3G59970", "AT3G63350", "AT4G03430", "AT4G04720", "AT4G11150", "AT4G13940", "AT4G14710", "AT4G16280", "AT4G17270", "AT4G24280", "AT4G28080", "AT4G29040", "AT4G32410", "AT4G33950", "AT4G35790", "AT4G37910", "AT5G03905", "AT5G05540", "AT5G10230", "AT5G17920", "AT5G21150", "AT5G22000", "AT5G23540", "AT5G27380", "AT5G28540", "AT5G37710", "AT5G38470", "AT5G38640", "AT5G39610", "AT5G46180", "AT5G48960", "AT5G49720", "AT5G52640", "AT5G54390", "AT5G60410", "AT5G62470", "AT5G65720"]
# this IDlist contain 121 AthaID's that come from duplicated genes in node 15, 17, 19, 27, 35, 39, 50, 71
# this list is used to compliment the functional gene selection list


if __name__ == "__main__":
    if len(sys.argv) != 4:
        print "python fromID2cluster.py WORKDIR infile outfile"
        print "infile should be 8332.blastn or 8592.blastn"
        sys.exit(0)
    WORKDIR,infilename,outfilename = sys.argv[1:]
    WORKDIR = os.path.abspath(WORKDIR)+"/"
    infile = open(WORKDIR+infilename,"rU")
    outfile = open(WORKDIR+outfilename,"a")
    for line in infile:
        clusterID = line.strip().split("\t")[0]
        evalue = line.strip().split("\t")[-2]
        AthaID = line.strip().split("\t")[2]
        AthaID = AthaID.split("@")[1]
        AthaID = AthaID.split(".")[0]
        if AthaID in IDlist:
            outfile.write(clusterID+"\t"+evalue+"\t"+AthaID+"\n")
    infile.close()
    outfile.close()


