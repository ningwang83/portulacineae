#! /usr/bin/env python

# use this to rename species label to specie name

import sys,os
import tree_reader

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "usage: "+sys.argv[0]+" inDIR outDIR fas_end tree_end"
		print "fas_end with .fa, .aln, .aln_cln tree_end with .tre"
		print "inDIR should be different from outDIR"
		sys.exit()

	inDIR,outDIR,fas_end,tree_end = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	outDIR = os.path.abspath(outDIR)+"/"
	
	for i in os.listdir(inDIR):
		if i.endswith(fas_end):
			outname = outDIR + i + ".ed"
			outfile = open(outname,"w")
			infile = open(inDIR+i,"rU")
			for line in infile:
				if line.startswith(">"):
					speciesID = line.strip().split("@")[0]
					outfile.write(speciesID+"\n")
				else: outfile.write(line)
			infile.close()
			outfile.close()
		elif i.endswith(tree_end):
			outtree = open(outDIR+i+".ed","w")
			intree = open(inDIR+i,"rU")
			oneline = intree.readline().strip()
			root = tree_reader.read_tree_string(oneline)
			for i in root.iternodes():
				if not i.istip: continue
				IDname = i.label.split("@")[0]
				i.label = IDname
			newtree = root.get_newick_repr(True)
			outtree.write(newtree+";")
			intree.close()
			outtree.close()
