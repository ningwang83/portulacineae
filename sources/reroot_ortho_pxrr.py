"""reroot trees with pxrr program"""


import sys,os
import tree_reader


Outgroups = "Beta,AniSFB,LimaeSFB,DBGStha,HZTS,BJKT,SFB27"


if len(sys.argv) != 4:
	print "usage: python reroot_ortho_pxrr.py inDIR outDIR file_end"
	sys.exit()

inDIR,outDIR,file_end = sys.argv[1:]

inDIR = os.path.abspath(inDIR)+"/"
outDIR = os.path.abspath(outDIR)+"/"

for tree in os.listdir(inDIR):
	if not tree.endswith(file_end): continue
	infile = open(inDIR+tree,"r")
	oneline = infile.readline().strip()
	root = tree_reader.read_tree_string(oneline)
	tipnodes = root.leaves()
	tiplabel = []
	for i in tipnodes:
		tiplabel.append(i.label)
	infile.close()
	outfile = outDIR+tree.split(".")[0]
	if "Beta" in tiplabel:
		cmd = "pxrr -t "+inDIR+tree+" -g Beta -o "+outfile+".rr"
	elif "LimaeSFB" in tiplabel or "DBGStha" in tiplabel:
		cmd = "pxrr -t "+inDIR+tree+" -g LimaeSFB,DBGStha -s -o "+outfile+".rr"
	else:
		cmd = "pxrr -t "+inDIR+tree+" -g AniSFB,SFB27,HZTS,BJKT -s -o "+outfile+".rr"
	os.system(cmd)
