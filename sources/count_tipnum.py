#! /usr/bin/env python

import os,sys
import tree_reader

""" Calculate tip number"""

if __name__ =="__main__":
	if len(sys.argv) != 3:
		print "usage: python "+sys.argv[0]+" inDIR file_end"
		print "file_end with .edit or .tre"
		sys.exit()	
	inDIR,file_end = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	for f in os.listdir(inDIR):
		if not f.endswith(file_end): continue
		infile = open(inDIR+f,"rU")
		treeline = infile.readline().strip()
		tree = tree_reader.read_tree_string(treeline)
		count = 0
		for n in tree.iternodes():
			if n.istip:
				count += 1
			else: continue
		print f, str(count)
