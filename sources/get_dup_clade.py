#! /usr/bin/env python

# use this code get duplicated clusters (homologs) for each node.

import os,sys

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python get_dup_clade.py DIR Dupclades79.out"
		print "cd into current working directory"
		sys.exit()
	DIR,outname = sys.argv[1:]
	DIR = os.path.abspath(DIR)+"/"
	Cluster = {}
	for f in os.listdir(DIR):
		if not "dupl" in f: continue
		if f.endswith(".tre"): continue
		nodeID = f.split(".")[-1]
		listcluster = []
		with open(f,"rU") as infile:
			for line in infile:
				clusterID = line.split(".")[0].split("/")[-1]
				treeID = line.split(".")[-3]
				listcluster.append(clusterID+"_"+treeID)
		Cluster["node"+nodeID] = (listcluster)
		infile.close()
	outfile = open(DIR+outname,"w")
	#outfile.write("overlap\t"+clusterstr+"\n")

	#if Cluster only have two keys
	Sort_keys = sorted(Cluster)
	for k in range(len(Sort_keys)):
		clusterlist = Cluster[Sort_keys[k]]
		setlist = set(clusterlist)
		outfile.write(Sort_keys[k]+"\t"+",".join(setlist)+"\n")

	outfile.close()



