"""
Read a concatenated pep/cds fasta file
write individual pep sequences from existing cds fasta
or write individual cds sequences from existing pep fasta
"""

import sys,os
from seq import read_fasta_file

def main(fasta,fasDIR,file_ending,outDIR):
	print "Reading pep fasta file",fasta
	seqDICT = {} #key is seqID, value is seq
	for s in read_fasta_file(fasta):
		seqDICT[s.name] = s.seq
	print "Writing fasta files"
	filecount = 0
	for i in os.listdir(fasDIR):
		if not i.endswith(file_ending): continue
		print i
		#method = i.split(".")[1]
		filecount += 1
		#outname = outDIR+"PEPC."+method+".cds.fas"
		outname = outDIR+i
		outfile = open(outname,"w")
		with open(fasDIR+i,"rU")as infile:
			for line in infile:
				line = line.strip()
				if line[0] == ">":
					indiID = line[1:]
					#outfile.write(line+"\n"+seqDICT[indiID]+"\n")
					if "______" in indiID:
						indiID_list = indiID.split("______")
						for indi in indiID_list:
							outfile.write(">"+indi+"\n"+seqDICT[indi]+"\n")
					else:
						outfile.write(line+"\n"+seqDICT[indiID]+"\n")
	assert filecount > 0,\
		"No file ends with "+file_ending+" found in "+fasDIR

if __name__ =="__main__":
	if len(sys.argv) != 5:
		print "usage: python "+sys.argv[0]+" pep82fasta cdsfasDIR file_ending outDIR"
		print "pep82fasta is the concatenated fasta file of all pep or all cds without cd-hit"
		sys.exit()
	
	fasta,fasDIR,file_ending,outDIR = sys.argv[1:]
	fasDIR = os.path.abspath(fasDIR) + "/"
	if not os.path.exists(outDIR):
		os.mkdir(outDIR)
	outDIR = os.path.abspath(outDIR) + "/"
	main(fasta,fasDIR,file_ending,outDIR)

