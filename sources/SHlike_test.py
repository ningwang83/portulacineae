#! /usr/bin/env python

import os,sys
from subprocess import call
import random

"""example tree file name: cluster2745_1rr.raxml.tre
example algnment file name: cluster2745_1rr.fa.mafft.aln_cln
"""

if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "python SHlike_test.py WORKDIR num_cores tree_end aln_end seqtype"
		print "tree_end could be .tre, aln_end should be aln_cln seqtype=dna/aa"
		sys.exit(0)
	WORKDIR,num_cores,tree_end,aln_end,seqtype = sys.argv[1:6]
	
	treelist = []
	alnlist = []
	WORKDIR = os.path.abspath(WORKDIR)+"/"
	for i in os.listdir(WORKDIR):
		if i.endswith(tree_end):
			treelist.append(i)
		if i.endswith(aln_end):
			alnlist.append(i) 
	
	treelist.sort()
	alnlist.sort()
	print treelist
	print alnlist
	
	model = "PROTCATWAG" if seqtype == "aa" else "GTRGAMMA"
	for i in range(len(treelist)):
		clusterID = treelist[i].split(".")[0]
		cmd = "raxml -T "+str(num_cores)+" -f J -p "+str(random.randint(1,1000000))+" -s "+\
			alnlist[i]+" -t "+treelist[i]+" -n "+clusterID+" -m "+model+" -w "+WORKDIR
		call(cmd,shell=True)
		try: 
			os.remove("RAxML_info."+clusterID)
			os.rename("RAxML_fastTreeSH_Support."+clusterID, WORKDIR+clusterID+".SHsupport")
			os.rename("RAxML_fastTree."+clusterID, WORKDIR+clusterID+".fastTree")
		except: pass

