#! /usr/bin/env python

""" define a set of functions that might be useful for future coding"""

import os,sys
import subprocess
from subprocess import call
from stat import *
import random
import glob

def walkdirectory(top):
	filelist = []
	for f in os.listdir(top):
		pathname = os.path.join(top,f)
		mode = os.stat(pathname).st_mode
		if S_ISDIR(mode):
			walkdirectory(pathname)
		elif S_ISREG(mode) and pathname[-1] != "~":
			#print pathname
			filelist.append(pathname)
	return filelist


def read_fasta(fastafile):
	infile = open(fastafile,"r")
	seqDict = {} #list of sequence objects
	templab = ""
	tempseq = ""
	First = True
	for i in infile:
		if i[0] == ">":
			if First: First = False
			else:
				seqDict[templab] = tempseq # include an empty key and value.
			templab = i.strip()[1:]
			tempseq = ""
		else:
			tempseq = tempseq + i.strip()
	infile.close()
	seqDict[templab] = tempseq
	return seqDict

def pasta(fasta,seqtype):
	alnfile = fasta.split("/")[-1]
	Pathway = "/".join(fasta.split("/")[:-1])
	Pathway = os.path.abspath(Pathway)
	alignment = fasta+".pasta.aln"
	if os.path.exists(alignment): return alignment
	if seqtype == "dna":
		cmd = "run_pasta.py --input="+fasta+" --datatype=DNA"			
		outfile = Pathway+"/pastajob.marker001."+alnfile+".aln"			
	elif seqtype == "aa":
		seqDict = read_fasta(fasta)
		print len(seqDict)
		tempfile = open(fasta+".tempAA","a")
		for seqID in seqDict:
			seq = seqDict[seqID]
			seq = seq.upper().replace("U","X") # pasta does not recognize "*" or "U"
			if "*" in seq:
				seq = seq[:seq.find("*")]
			tempfile.write(">"+seqID+"\n"+seq+"\n")
		tempfile.close()
		cmd = "run_pasta.py --input="+fasta+".tempAA"+" --datatype=Protein"
		outfile = Pathway+"/pastajob.marker001."+alnfile+".tempAA.aln"
	#else: print "Input data type is not aa or dna"
	call(cmd,shell=True)
	os.rename(outfile,alignment)
	os.system("rm pastajob*")
	if os.path.exists(fasta+".tempAA"): os.remove(fasta+".tempAA")
	return alignment

def mafft(fasta,thread,seqtype):
	alignment = fasta+".mafft.aln"
	if os.path.exists(alignment) and os.stat(alignment).st_size>0:
		return alignment
	seqDict=read_fasta(fasta)
	Numseq=len(seqDict)
	maxlength = 0
	for seqID in seqDict:
		seq = seqDict[seqID]
		maxlength = max(maxlength,len(seq.replace("-","")))

	assert Numseq >= 4, "less than 4 sequences in "+fasta
	if Numseq >= 1000 or maxlength >= 10000:
		cmd = "mafft --auto --thread "+str(thread)
	else: cmd = "mafft --genafpair --maxiterate 1000 --thread "+str(thread)
	# genafpair suitable for sequences containing large unalignable regions

	if seqtype == "dna":
		cmd += " --nuc "+fasta
	elif seqtype == "aa":
		tempfile = open(fasta+".tempAA","a")
		for seqID in seqDict:
			seq = seqDict[seqID]
			seq = seq.upper().replace("U","X")
			if "*" in seq:
				seq = seq[:seq.find("*")]
			tempfile.write(">"+seqID+"\n"+seq+"\n")
		tempfile.close()
		cmd += " --amino "+fasta+".tempAA"		
	#else: print "Input data type is not aa or dna"
	outaln = open(alignment,"a")
	print cmd
	call(cmd,shell=True,stdout=outaln)
	outaln.close()
	if os.path.exists(fasta+".tempAA"): os.remove(fasta+".tempAA")
	return alignment


def phyutility(alignment,min_col_occup,seqtype,min_chr): # min_chr = 10
	"""
	remove columns with occupancy lower than min_clo_occup(MINIMUM_COLUMN_OCCUPANCY)
	remove seqs shorter than min_chr after filter columns
	"""
	cleaned = alignment+"_cln"
	if os.path.exists(cleaned): return cleaned
	#assert alignment.endswith(".aln"),\
	#	"phyutility infile "+alignment+" not ends with .aln"
	assert os.stat(alignment).st_size > 0, alignment+" is empty"
	#assert seqtype == "aa" or seqtype == "dna","Input data type: dna or aa"

	if seqtype == "aa":
		cmd = "phyutility -aa -clean "+str(min_col_occup)+" -in "+\
			   alignment+" -out "+alignment+"_pht"
	else:
		cmd = "phyutility -clean "+str(min_col_occup)+" -in "+\
			   alignment+" -out "+alignment+"_pht"
	print cmd
	call(cmd,shell=True)
	assert os.path.exists(alignment+"_pht"),"Error phyutility"
	
	#remove empty and very short seqs
	outfile = open(cleaned,"w")
	seqDict = read_fasta(alignment+"_pht")
	for seqID in seqDict:
		seq = seqDict[seqID]
		if len(seq.replace("-","")) >= int(min_chr):
			outfile.write(">"+seqID+"\n"+seq+"\n")
	outfile.close()
	os.remove(alignment+"_pht")
	return cleaned

def raxml(cleaned,num_cores,seqtype): # no bootstrap analyses
	assert len(read_fasta(cleaned)) >= 4, "less than 4 sequences in "+cleaned
	alnfile = cleaned.split("/")[-1]
	clusterID = alnfile.split(".")[0]
	Pathlist = cleaned.split("/")[:-1]
	Pathway = os.path.abspath("/".join(Pathlist)) # raxml crashes if input file starts with .
	tree = clusterID+".raxml.tre"
	raw_tree = "RAxML_bestTree."+clusterID
	model = "PROTCATWAG" if seqtype == "aa" else "GTRCAT" # Ya use GTRCAT for dna data
	if not os.path.exists(Pathway+tree) and not os.path.exists(Pathway+raw_tree):		 
		cmd = "raxml -T "+str(num_cores)+" -p "+str(random.randint(1,1000000))+" -s "+\
			os.path.abspath(cleaned)+" -n "+clusterID+" -m "+model+" -w "+Pathway#+" -# 10"
		call(cmd,shell=True)
	os.chdir(Pathway)
	os.rename(raw_tree,tree)
	
	#for file in os.listdir(Pathway):
	#	if file.startswith("RAxML_info.") or file.startswith("RAxML_log."):
	#		os.remove(file)
	#	elif file.startswith("RAxML_parsimonyTree.") or file.startswith("RAxML_result."):
	#		os.remove(file) 

	for file in glob.glob("RAxML_*"):
		os.remove(file)
	try:
		#os.remove("RAxML_info."+clusterID)
		#os.remove("RAxML_log."+clusterID)
		#os.remove("RAxML_parsimonyTree."+clusterID)
		#os.remove("RAxML_result."+clusterID)
		os.remove(alnfile+".reduced")
	except: pass # no need to worry about extra intermediate files
	return os.path.abspath(tree)


def raxml_boot(cleaned,num_cores,seqtype,replicates): # no bootstrap analyses
	assert len(read_fasta(cleaned)) >= 4, "less than 4 sequences in "+cleaned
	alnfile = cleaned.split("/")[-1]
	clusterID = alnfile.split(".")[0]
	Pathlist = cleaned.split("/")[:-1]
	Pathway = os.path.abspath("/".join(Pathlist)) # raxml crashes if input file starts with .
	tree = clusterID+".raxml_bs.tre"
	raw_tree = "RAxML_bipartitions."+clusterID
	model = "PROTCATWAG" if seqtype == "aa" else "GTRGAMMA" # Ya use GTRCAT for dna data
	if not os.path.exists(Pathway+tree) and not os.path.exists(Pathway+raw_tree):		 
		cmd = "raxml -T "+str(num_cores)+" -p "+str(random.randint(1,1000000))+\
			" -f a -x "+str(random.randint(1,1000000))+" -# "+str(replicates)+" -s "+\
			os.path.abspath(cleaned)+" -n "+clusterID+" -m "+model+" -w "+Pathway
		call(cmd,shell=True)
	os.chdir(Pathway)
	os.rename(raw_tree,tree)
	os.rename("RAxML_bootstrap."+clusterID, clusterID+".raxml_bs.trees")

	for file in glob.glob("RAxML_*"):
		os.remove(file)
		
	try:
		#os.remove("RAxML_bestTree."+clusterID)
		#os.remove("RAxML_info."+clusterID)
		#os.remove("RAxML_log."+clusterID)
		#os.remove("RAxML_parsimonyTree."+clusterID)
		#os.remove("RAxML_result."+clusterID)
		#os.remove("RAxML_bipartitionsBranchLabels."+clusterID)
		os.remove(alnfile+".reduced")
	except: pass # no need to worry about extra intermediate files
	return os.path.abspath(tree)

def fasttree(cleaned,seqtype):
	"""read in a cleaned alignment ends with '.aln_cln
	extimate a tree using fasttree'"""
	alnfile = cleaned.split("/")[-1]
	clusterID = alnfile.split(".")[0]
	Pathlist = cleaned.split("/")[:-1]
	Pathway = os.path.abspath("/".join(Pathlist))
	tree = clusterID+".fasttree.tre"
	Treepath = os.path.join(Pathway,tree)
	alg = " -wag " if seqtype == "aa" else " -nt -gtr "
	if os.path.exists(Treepath):return Treepath
	cmd = "fasttree"+alg+"-quiet "+os.path.abspath(cleaned)
	out = open(Treepath, 'a')
	call(cmd,shell=True,stdout=out)
	out.close()
	return Treepath


if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "python cluster_totree.py DIR num_cores seqtype(dna/aa) bootstrap(y/n)"
		print "Usage: fasta sequence data to tree generation"
		print "seqence type needs to be dna or aa (lower case)"
		sys.exit(0)
		# minimum number of sequences should be bigger than the minimum taxa in the cluster files
	WORKDIR,num_cores,seqtype,bs = sys.argv[1:5]

	assert seqtype == "aa" or seqtype == "dna","Input data type: dna or aa"
	assert bs == "y" or bs=="n","bootstrap? (y/n)"
	filelist = walkdirectory(WORKDIR)
	filecount = 0
	for file in filelist:
		if not file.endswith(".fa"): continue
		seqDict = read_fasta(file)
		assert len(seqDict) >= 4, "less than 4 species in the "+file
		maxlength = 0
		for seqID in seqDict:
			seq = seqDict[seqID]
			maxlength = max(maxlength,len(seq.replace("-","")))
		if (maxlength>=10000 and seqtype=="aa") or (maxlength>=30000 and seqtype=="dna"):
			print "%s contain sequences with %d as the longest." % (file, maxlength)
			print "Warning: sequence too long. May crash alignment process"

		if bs == "y":
			alignment = mafft(file,num_cores,seqtype)
			cleaned = phyutility(alignment,0.2,seqtype,10)
			if len(read_fasta(cleaned)) >= 4:
				raxml_boot(cleaned,num_cores,seqtype,200)
		else:
			if len(seqDict) < 1000:
				alignment = mafft(file,num_cores,seqtype)
				cleaned = phyutility(alignment,0.1,seqtype,10)
				raxml(cleaned,num_cores,seqtype)
			else:
				alignment = pasta(file,seqtype)
				cleaned = phyutility(alignment,0.05,seqtype,10)
				fasttree(cleaned,seqtype)
		filecount += 1
	print "%d files have built trees." % (filecount)


