#! /usr/bin/env python
#using pxaa2cdn to align cds according to aa alignments

import sys,os

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: "+sys.argv[0]+" inDIR outDIR file_ending"
		print "file ending with mafft.aln, inDIR is pepalign, outDIR is the cds.fa"
		sys.exit()

	inDIR,outDIR,file_ending = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	outDIR = os.path.abspath(outDIR)+"/"
	
	for i in os.listdir(inDIR):
		if not i.endswith(file_ending): continue
		#clusterID = i.split(".")[0]
		clusterID = ".".join(i.split(".")[:-3])
		cmd = "pxaa2cdn -a "+inDIR+i+" -n "+outDIR+clusterID+".fa -o "+outDIR+clusterID+".aln"
		os.system(cmd)
