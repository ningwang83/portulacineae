#! /usr/bin/env python

import os,sys

""" read the restuls file and extract selection results """

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "python "+sys.argv[0]+" WORKDIR"
		sys.exit(0)
	WORKDIR= sys.argv[1]
	WORKDIR = os.path.abspath(WORKDIR)
	outname = os.path.join(WORKDIR,"summary_selection.out")
	outfile = open(outname,"w")
	title = "Branch,Mean_dNdS,RateClasses,OmegaOver1,WtOmegaOver1,LRT,p,p_Holm,BranchLength"
	title_list = title.split(",")
	outfile.write("\t".join(title_list)+"\n")
	for f in os.listdir(WORKDIR):
		if f.endswith(".out"): continue
		infile = open(f,"rU")
		outfile.write(f+"\n")
		First = True
		for l in infile:
			if First: 
				First = False
				continue
			line = l.strip().split(",")
			print line
			p = line[6]
			p_Holm = line[7]
			if float(p) <= 0.1 or float(p_Holm) <= 0.1:
				outfile.write("\t".join(line)+"\n")
		infile.close()
	outfile.close()
		
