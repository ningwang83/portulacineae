#! /usr/bin/env python

"""
use this script to automatically run TransDecoder to find open reading frame 
from each .Trinity.fasta file collected from each species in one directory

"""

import os,sys
# import subprocess
# from subprocess import call

#database = "~/Documents/Transcriptome_bird/proteome_database/birddb"
database = "/home/ningwum/Documents/Repo/phylogenomic_dataset_construction/data/blastp_database/plant_proteome.db"
#database = "/home/ningwum/Documents/Repo/phylogenomic_dataset_construction/data/blastp_database/Threeproteome.db"

if __name__ == "__main__":
  if len(sys.argv) != 6:
    print "python run_TransDecoder.py WORKDIR file_ending Num_thread evalue Num_cpu"
    # file end with .Trinity.fasta, I use cpu 9, thread 9 evalue 10
    sys.exit(0)

  WORKDIR = sys.argv[1]
  WORKDIR = os.path.abspath(WORKDIR)+"/"
  CURDIR = os.getcwd()+"/"
  # if WORKDIR[-1] != "/": WORKDIR = WORKDIR+"/"
  FILE_ENDING = sys.argv[2]
  Num_Thread = sys.argv[3]
  Evalue = sys.argv[4]
  CPUnumber = sys.argv[5]

  for file in os.listdir(WORKDIR):
    if not file.endswith(FILE_ENDING): continue
    taxonID = file.split(".")[0]
    print taxonID
    cmd1 = "TransDecoder.LongOrfs -t "+WORKDIR+file+" -S"
    print cmd1
#    call(cmd1,shell=True)   # subprocess call can work the same as long as shell=True so command can be taken as a string.
    os.system(cmd1)
    
    TransDecoder_pep = file+".transdecoder_dir/longest_orfs.pep"
    cmd2 = "blastp -query "+CURDIR+TransDecoder_pep+" -db "+database+\
           " -max_target_seqs 1 -outfmt 6 -evalue "+Evalue+" -num_threads "+Num_Thread
    OutName = taxonID+".blastp.outfmt6"
#    out = open(WORKDIR+OutName,'w')
    cmd2 += " > "+WORKDIR+OutName
    print cmd2
#    call(cmd2,stdout=out,shell=True)
    os.system(cmd2)
    
    cmd3 = "TransDecoder.Predict -t "+WORKDIR+file+" --retain_blastp_hits "+WORKDIR+OutName+" --cpu "+CPUnumber
    print cmd3
#    call(cmd3,shell=True)
    os.system(cmd3)

