"""
Read a concatenated fasta file
write individual fasta files for each extracted rooted tree
this output will be used for extract longest seq from each cluster and used for blast 2 go analyses
"""
# need to double check the end of the tree files and make sure the treeID is the third last item in the tree file name
# or need to modify the treeID down bellow

import sys,os,newick3,phylo3
import tree_utils
from seq import read_fasta_file

def main(fasta,treDIR,tree_file_ending,outDIR):
	treDIR = os.path.abspath(treDIR) + "/"
	outDIR = os.path.abspath(outDIR) + "/"
	print "Reading fasta file",fasta
	seqDICT = {} #key is seqID, value is seq
	for s in read_fasta_file(fasta):
		seqDICT[s.name] = s.seq
	print "Writing fasta files"
	filecount = 0
	for i in os.listdir(treDIR):
		if i.endswith(tree_file_ending):
			print i
			filecount += 1
			with open(treDIR+i,"r")as infile:
				intree = newick3.parse(infile.readline())
			clusterID = tree_utils.get_clusterID(i)
			treeID = i.split(".")[-3]# need to modify properly according to the name of the tree files
			outname = outDIR+clusterID+"_"+treeID+".fa"
			with open(outname,"w") as outfile:
				for label in tree_utils.get_front_labels(intree):
					#if not label in seqDICT.keys(): continue # I add it for using cdsall80.fa.cut file
					outfile.write(">"+label+"\n"+seqDICT[label]+"\n")
	assert filecount > 0,\
		"No file ends with "+tree_file_ending+" found in "+treDIR

if __name__ =="__main__":
	if len(sys.argv) != 5:
		print "usage: python tree2fasta_genedup.py fasta treDIR tree_file_ending outDIR"
		sys.exit()
	
	fasta,treDIR,tree_file_ending,outDIR = sys.argv[1:]
	main(fasta,treDIR,tree_file_ending,outDIR)

