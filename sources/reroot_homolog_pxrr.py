"""reroot trees with pxrr program"""


import sys,os
import tree_reader
import random



if len(sys.argv) != 4:
	print "usage: python reroot_homolog_pxrr.py inDIR outDIR file_end"
	sys.exit()

inDIR,outDIR,file_end = sys.argv[1:]

inDIR = os.path.abspath(inDIR)+"/"
outDIR = os.path.abspath(outDIR)+"/"

for tree in os.listdir(inDIR):
	if not tree.endswith(file_end): continue
	#print tree
	infile = open(inDIR+tree,"r")
	oneline = infile.readline().strip()
	root = tree_reader.read_tree_string(oneline)
	tipnodes = root.leaves()
	Beta = []
	Limae = []
	Ani = []
	for i in tipnodes:
		#print i.label
		name = i.label.split("@")[0]
		if name == "Beta":
			Beta.append(i.label)
		elif name in ["LimaeSFB","DBGStha"]:
			Limae.append(i.label)
		elif name in ["AniSFB","SFB27","HZTS","BJKT"]:
			Ani.append(i.label)
	infile.close()
	outfile = outDIR+tree.split(".")[0]
	if len(Beta) != 0:
		cmd = "pxrr -t "+inDIR+tree+" -g "+"".join(random.sample(Beta,1))+" -o "+outfile+".rr"
	elif len(Beta) == 0 and len(Limae) != 0:
		cmd = "pxrr -t "+inDIR+tree+" -g "+"".join(random.sample(Limae,1))+" -s -o "+outfile+".rr"
	elif len(Beta) == 0 and len(Limae) == 0 and len(Ani) != 0:
		cmd = "pxrr -t "+inDIR+tree+" -g "+"".join(random.sample(Ani,1))+" -s -o "+outfile+".rr"
	os.system(cmd)
