#! /usr/bin/env python

""" use this script to prune trees to leave only taxa wanted. """

import os,sys
from node import Node
import tree_reader

"""taxa_list = ["Cactaceae_Coryphantha_maiz-tablasensis", "Cactaceae_Matucana_aurantiaca", "Cactaceae_Echinocereus_pectinatus", \
			"Cactaceae_Opuntia_bravoana", "Portulacaceae_Portulaca_oleracea", "Cactaceae_Eriosyce_wagenknechtii", "Cactaceae_Gymnocalycium_mihanovichii", \
			"Montiaceae_Cistanthe_grandiflora", "Cactaceae_Grusonia_bradtiana", "Cactaceae_Rhipsalis_baccifera", "Cactaceae_Ferocactus_latispinus", \
			"Cactaceae_Stetsonia_coryne", "Molluginaceae_Pharnaceum_exiguum", "Anacampserotaceae_Anacampseros_filamentosa", \
			"Cactaceae_Maihueniopsis_conoidea", "Montiaceae_Phemeranthus_parviflorus", "Cactaceae_Ariocarpus_retusus", "Talinaceae_Talinum_paniculatum", \
			"Cactaceae_Copiapoa_desertorum", "Montiaceae_Claytonia_virginica", "Limeaceae_Limeum_aethiopicum", "Molluginaceae_Mollugo_pentaphylla", \
			"Montiaceae_Lewisia_nevadensis", "Montiaceae_Calandrinia_grandiflora_or_procumbens", "Basellaceae_Anredera_cordifolia", \
			"Cactaceae_Astrophytum_asterias", "Montiaceae_Montia_chamissoi", "Molluginaceae_Mollugo_verticillata", "Molluginaceae_Mollugo_cerviana", \
			"Cactaceae_Tacinga_lilae", "Molluginaceae_Glinus_dahomensis", "Basellaceae_Basella_alba", "Cactaceae_Leuenbergeria_bleo", \
			"Cactaceae_Pereskia_grandifolia", "Didiereaceae_Portulacaria_pygmaea", "Cactaceae_Pereskia_aculeata", "Didiereaceae_Alluaudia_procera", \
			"Cactaceae_Leuenbergeria_guamacho", "Cactaceae_Maihuenia_poeppigii", "Didiereaceae_Alluaudia_dumosa", "Didiereaceae_Alluaudiopsis_marnieriana", \
			"Didiereaceae_Portulacaria_afra", "Anacampserotaceae_Talinopsis_frutescens", "Cactaceae_Leuenbergeria_lychnidiflora", \
			"Didiereaceae_Alluaudia_humbertii", "Didiereaceae_Decarya_madagascariensis", "Didiereaceae_Didierea_trollii", "Didiereaceae_Didierea_madagascariensis"]"""

taxa_list = ["Coma28", "Maau13", "Ecpe", "Opbr17", "EZGR", "Erwa30", "Gymi06", "CigrSFB", "Grbr1013", "MJM2938", \
			"Fela32", "Stco14", "PhaexSFB", "Anfi", "Maco1013", "MJM2214", "Arre20", "MJM1789", "Code1013", "MJM1156", \
			"LimaeSFB", "HURS", "MJM3168", "CagrSFB", "AncoSFB", "Asas15", "MJM3167", "NXTS", "RNBN", "Tali11", "GliloSFB", \
			"CTYH", "Lebl", "Pegr", "CerpySFB", "JLOV", "DBG19565744023G", "Legu31", "Mapo24", "DBG198703010201Z", "AlmarSFB", \
			"DBG19880583021G", "MJM2441", "Lely", "DBG19740229011", "MJM2944", "DBG294800240202G", "DBGDima"]

def get_front_labels(node):
	"""given a node, return a list of front tip labels"""
	leaves = node.leaves()
	return [i.label for i in leaves]

def overlap(target_list,taxa_list):
	ol = 0
	for i in target_list:
		if i in taxa_list:
			ol += 1
	return ol

def diff(target_list,taxa_list):
	dif = 0
	for i in target_list:
		if not i in taxa_list:
			#print i
			dif += 1
	return dif
	
	
def reroot(oldroot, newroot):
	v = [] #path to the root
	n = newroot
	while 1:
		v.append(n)
		if not n.parent: break
		n = n.parent
	v.reverse()
	for i, cp in enumerate(v[:-1]):
		node = v[i+1]
		# node is current node; cp is current parent
		#print node.label, cp.label
		cp.remove_child(node)
		node.add_child(cp)
		cp.length = node.length
		cp.label = node.label
	return newroot

def remove_kink(node,curroot):
	#smooth the kink created by prunning
	if node == curroot and len(curroot.children) == 2:
		#move the root away to an adjacent none-tip
		if curroot.children[0].istip: #the other child is not tip
			curroot = reroot(curroot,curroot.children[1])
		else: curroot = reroot(curroot,curroot.children[0])
	# if root is a kink
	if node == curroot and len(curroot.children) == 1:
		kink = node
		curroot = reroot(curroot,curroot.children[0])
		curroot.remove_child(kink)
		node = curroot
		return node, curroot
	#---node---< all nodes should have one child only now
	length = node.length + (node.children[0]).length
	par = node.parent
	kink = node
	node = node.children[0]
	#parent--kink---node<
	par.remove_child(kink)
	par.add_child(node)
	node.length = length
	return node,curroot

def remove_tips(root,taxa_list):
	for i in root.leaves():
		if not i.label in taxa_list:
			print i.label
			par = i.prune()
	return root


if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python prune_tree.py inDIR input_treefile outtree_name"
		print "make sure the input trees have tip names consistent with the taxa list"
		print "provide the full path of input treefile"
		sys.exit(0)

	inDIR,intree,outtree = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	outfile = open(inDIR+outtree,"w")
	# read the input trees
	infile = open(intree,"rU")
	curroot = tree_reader.read_tree_string(infile.readline().strip())
	# check out if all taxa in the tree
	root_taxa = get_front_labels(curroot)
	differents = diff(taxa_list,root_taxa)
	assert differents == 0, "some taxa are not in the tree"
	# prune tree
	while True and curroot != None:
		print curroot.get_newick_repr()
		totaltaxa = get_front_labels(curroot)
		dif_leave = diff(totaltaxa,taxa_list)
		if dif_leave == 0:
			break
		else: 
			curroot = remove_tips(curroot,taxa_list)
	# remove kinks on the tree!
	while True:
		count = 0
		for i in curroot.iternodes():
			if not i.istip and len(i.children) == 1:
				count += 1
				node,curroot = remove_kink(i,curroot)
				break
		if count == 0:
			break
	newroot = curroot.get_newick_repr(True)+";"
	outfile.write(newroot+"\n")
	outfile.close()
	infile.close()
			
