#! /usr/bin/env python

import sys,os

DictIDs = {}

DictIDs["Cactaceae_herb"] = ["CPKP", "Ecpe", "Noco", "MJM2911", "MJM2938", "Tali11", "Gymi06", "Tuco07", "Asmy08", "Ecau09", "Tear12", "Maau13", "Stco14", "Asas15", "Opbr17", "Ceyu18", "Arre20", "Pttu21", "Paga22", "Mapo24", "Sasa26", "Coma28", "Pecu29", "Erwa30", "Fela32", "Grbr1013", "Code1013", "Maco1013", "Pubo1013"]

DictIDs["Pereskia_wood"] = ["Lely", "Lebl", "JLOV", "Pegr", "Legu31"]

DictIDs["Cactaceae_all"] =  DictIDs["Cactaceae_herb"] + DictIDs["Pereskia_wood"]

DictIDs["Outgroup"] = ["AniSFB", "LimaeSFB", "DBGStha", "HZTS", "BJKT", "SFB27", "Beta"]

DictIDs["Montiaceae"] = ["CigrSFB", "CagrSFB", "MJM3167", "MJM3168", "MJM3165", "MJM2214", "MJM1156", "TRS2027", "MJM3142"]

DictIDs["Portulacaceae"] = ["LDEL", "LLQV", "CPLT", "UQCB", "EZGR", "IWIS", "GCYL", "KDCH"]

DictIDs["Talinaceae"] = ["Tafr", "MJM1789", "Tapo"]

DictIDs["Molluginaceae"] = ["SuecaSFB", "PhaexSFB", "RNBN", "SCAO", "HURS", "NXTS", "GliloSFB"]

DictIDs["SlowDidier"] = ["DBG19565744023G", "DBG294800240202G", "MJM2944", "DBG198703010201Z", "DBG19740229011", "AlmarSFB", "DBGDima"]

DictIDs["FastDidier"] = ["CerpySFB", "DBG19880583021G"]

DictIDs["Didiereaceae_all"] = DictIDs["SlowDidier"] + DictIDs["FastDidier"]

DictIDs["Anacampserotaceae"] = ["MJM2441", "GrakuSFB", "Anfi"] 

DictIDs["Basellaceae"] = ["AncoSFB", "CTYH"]

def collect_IDs(taxon_list,blastn_file):
	AthaIDlist = []
	with open(blastn_file,"r") as infile:
		for line in infile:
			query = line.strip().split("\t")[0]
			query_taxon = query.split("@")[0]
			hit = line.strip().split("\t")[2]
			hitIDs = hit.split("@")[-1]
			if query_taxon in taxon_list:
				AthaIDlist.append(hitIDs)
	infile.close()
	AthaIDstr = ",".join(set(AthaIDlist))
	return AthaIDstr

def compare_list(list1,list2):
	unique_for_list1 = []
	unique_for_list2 = []
	overlap = []
	for i in list1:
		if i in list2:
			overlap.append(i)
		elif not i in list2:
			unique_for_list1.append(i)
	for j in list2:
		if not j in list1:
			unique_for_list2.append(j)
	return unique_for_list1, unique_for_list2, overlap

if __name__ =="__main__":
	if len(sys.argv) != 3:
		print "make a directory first"
		print "usage: "+sys.argv[0]+" inDIR .blastn"
		sys.exit()
	inDIR,blastn_end = sys.argv[1:]
	inDIR = os.path.abspath(inDIR) + "/"
	outfile = open(inDIR+"outID.txt","a")
	for i in os.listdir(inDIR):
		if not i.endswith(blastn_end): continue
		for key in DictIDs:
			#print key
			AthaIDstr = collect_IDs(DictIDs[key], inDIR+i)
			outfile.write(key+"\t"+AthaIDstr+"\n")
			#print key+"\t"+AthaIDstr
	outfile.close()

	AthaIDdict = {}
	with open(inDIR+"outID.txt","rU") as infile:
		for line in infile:
			cladeID = line.strip().split("\t")[0]
			AthaIDlist = line.strip().split("\t")[1].split(",")
			AthaIDdict[cladeID] = AthaIDlist
	infile.close()
	outfile2 = open(inDIR+"compareID.out","a")
	for i in range(len(AthaIDdict.keys())):
		for j in range(len(AthaIDdict.keys())):
			if j > i:# sorted(dic) means the keys of dic have been sorted and a list return
				key1 = sorted(AthaIDdict)[i]
				key2 = sorted(AthaIDdict)[j]
				unique_for_list1, unique_for_list2, overlap = compare_list(AthaIDdict[key1],AthaIDdict[key2])
				str1 = ",".join(unique_for_list1)
				str2 = ",".join(unique_for_list2)
				str3 = ",".join(overlap)
				outfile2.write(key1+"_unique_"+key2+"\t"+str1+"\n")
				outfile2.write(key1+"_"+key2+"_unique\t"+str2+"\n")
				outfile2.write(key1+"_"+key2+"_overlap\t"+str3+"\n\n")
	outfile2.close()
				
	
	
	