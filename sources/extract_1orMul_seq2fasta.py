#! /usr/bin/env python


import os,sys
import random
from cluster_totree import read_fasta

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python "+sys.argv[0]+" inDIR file_end number"
		print "number is the extract number of sequences from fasta"
		sys.exit(0)
	inDIR,file_end,number = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	dirname = inDIR.split("/")[-2]
	outfile = open(inDIR+dirname+"_outcombine.fas","a")
	for fasta in os.listdir(inDIR):
		if not fasta.endswith(file_end): continue
		clusterID = fasta.split(".")[0]
		seqDict = read_fasta(inDIR+fasta)
		#print clusterID+"\t"+str(len(seqDict.keys()))
		keys = seqDict.keys()
		if int(number) > 1:
			if len(keys) > int(number):
				seqN = random.sample(keys,int(number))
			else:
				seqN = keys
			for i in set(seqN): # aviod repeats in the list, but may not equal to number
				outfile.write(">"+clusterID+"."+i+"\n"+seqDict[i]+"\n")
		elif int(number) == 1:
			maxname = ""
			maxseq = ""
			for i in keys:
				if len(seqDict[i]) > len(maxseq):
					maxseq = seqDict[i]
					maxname = i
			outfile.write(">"+clusterID+"."+maxname+"\n"+maxseq+"\n")

		"""count = 10
		for i in seqDict:
			if count > 0:
				outfile.write(">"+clusterID+"."+i+"\n"+seqDict[i]+"\n")
			count -= 1"""

	outfile.close()

		
