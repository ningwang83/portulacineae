#! /usr/bin/env python

import os,sys

""" first read Genbank gene file, extract gene and AthaID
then read the 8952_mulseq.blastn file, extract mapping cluster from AthaID """

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python "+sys.argv[0]+" WORKDIR Genbankfile .blastn_file"
		sys.exit(0)
	WORKDIR, Genbankfile,blastnfile = sys.argv[1:]
	WORKDIR = os.path.abspath(WORKDIR)
	outname = os.path.join(WORKDIR,"Gene_withmapID.out")
	outfile = open(outname,"w")

	# first read Genbank file and save gene name and AthaID in a Dictionary
	Genedic = {}
	Genfile = open(Genbankfile,"rU")
	for line in Genfile:
		#if len(line) < 1: continue
		genename = line.strip().split("\t")[0]
		#print genename
		AthaID = line.strip().split("\t")[1]
		#print AthaID
		Genedic[AthaID] = genename
	Genfile.close()

	# read the .blastn file and extract clusters with mapping AthaID
	Infile = open(blastnfile,"rU")
	for li in Infile:
		clustername = li.strip().split("\t")[0]
		AthaID = li.strip().split("\t")[2]
		AthaID = AthaID.split("@")[1]
		AthaID = AthaID.split(".")[0]
		if AthaID in Genedic:
			outfile.write(Genedic[AthaID]+"\t"+li+"\n")
	Infile.close()
	outfile.close()
		
