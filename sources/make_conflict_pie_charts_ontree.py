#! /usr/bin/env python

# must use ete2 implemented Tree and Node class.
# I must use ete2 as ete3 cannot load module correctly

import os,sys
from ete2 import Tree,faces,TreeStyle,COLOR_SCHEMES,NodeStyle,SVG_COLORS

if __name__ == "__main__":
	if len(sys.argv) != 8:
		print "python "+sys.argv[0]+" WORKDIR species_tree node.keyfile conflict.histfile num_genes phyparttree taxonIDfile"
		sys.exit(0)
	WORKDIR, species_tree,nodekeyfile,hist_file,gene_num,phypart_tre,taxon = sys.argv[1:]
	WORKDIR = os.path.abspath(WORKDIR)+"/"
	dicname = WORKDIR.split("/")[-2]
	gene_num = float(gene_num)
	
	# readin the node.keyfile and generate key for corresponding each nodes
	node_keys = {}
	with open(nodekeyfile,"rU") as infile:
		for l in infile:
			nodenumber = l.strip().split(" ")[0]
			leave_str = l.strip().split(" ")[1].replace("(","").replace(")","")
			node_leaves = leave_str.split(",")
			node_keys[nodenumber] = node_leaves
	
	# read the first two trees in conf_tre and build concord directory and conflict directory
	concord_conf = {}
	with open(phypart_tre, "rU") as tree:
		concon_tree = Tree(tree.readline())
		conf_tree = Tree(tree.readline())
	for node in node_keys:
		MRCA1 = concon_tree.get_common_ancestor(set(node_keys[node]))
		concord = int(MRCA1.support)
		MRCA2 = conf_tree.get_common_ancestor(set(node_keys[node]))
		conf = int(MRCA2.support)
		concord_conf[node] = [concord,conf]

	# readin the conflict.histfile, extract max conflict, generate pie_chart data for each node
	pie_charts = {}
	with open(hist_file,"rU") as handle:
		for l in handle:
			item = l.strip().split(",")
			nodename = item.pop(0)[4:]
			item.pop(0)
			item.pop(-1)
			#first = round(float(item.pop(0)),0)
			#sum_gene = float(item.pop(-1))
			if len(item) > 0:
				most_conflict = max([float(x) for x in item])
			else:
				most_conflict = 0.0
			connum = concord_conf[nodename][0]
			allconf = concord_conf[nodename][1]	
			concord_per = (connum/gene_num) * 100 
			most_conflict_per = (most_conflict/gene_num) * 100
			other_conflict = (allconf - most_conflict) / gene_num * 100
			the_rest = (gene_num - connum - allconf) / gene_num * 100
			
			pie_charts[nodename] = [concord_per,most_conflict_per,other_conflict,the_rest]
	
	# read in species_ID file and make a taxon directory with ID as key and species name as value
	taxon_dic = {}
	with open(taxon,"rU") as infile:
		for li in infile:
			ID = li.strip().split("\t")[0]
			species = li.strip().split("\t")[1]
			taxon_dic[ID] = species
	
	# readin the rooted species tree and draw pie chart on corresponding nodes
	sortnodes =  sorted(node_keys.keys())
	root = Tree(species_tree)	
	#treefile = open(species_tree,"rU")
	#treestr = treefile.readline()
	#root = tree_reader.read_tree_string(treestr)
	for node in sortnodes:
		MRCA = root.get_common_ancestor(set(node_keys[node]))
		pie = faces.PieChartFace(pie_charts[node],colors = ["blue","springgreen","red","silver"],width=50, height=50)
		pie.opacity = 0.5
		MRCA.add_face(pie,column=0,position="branch-right")
		concord_text = faces.TextFace(str(concord_conf[node][0])+'  ',fsize=20)
		conf_text = faces.TextFace(str(concord_conf[node][1])+'  ',fsize=20)
		# add the text to nodes
		MRCA.add_face(concord_text,column=0,position="branch-top")
		MRCA.add_face(conf_text,column=0,position="branch-bottom")
	
	for n in root.traverse():
		if n.is_leaf():
			taxonID = n.name
			spname = taxon_dic[taxonID]
			F = faces.TextFace(' '+spname,fsize=20,fstyle='italic')
			n.add_face(F,column=0,position="aligned")
			
	
	# visualize tree
	nstyle = NodeStyle()
	nstyle["size"] = 0
	for n in root.traverse():
		n.set_style(nstyle)
	
	ts = TreeStyle()
	ts.show_leaf_name = False
	#ts.show_branch_length = True
	ts.extra_branch_line_color = "orange"
	ts.mode = "r"
	ts.draw_guiding_lines = True # Draw guidelines from leaf nodes to aligned faces
	ts.guiding_lines_color = "red"
	ts.guiding_lines_type = 2 # 0=solid, 1=dashed, 2=dotted
	#root.ladderize(direction=1)
	root.render(WORKDIR+dicname+"_conflict_pie_chart.svg",tree_style=ts)

