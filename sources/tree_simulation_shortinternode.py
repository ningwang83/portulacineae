#! /usr/bin/env python

"""
Simulating gene trees with a species tree that different edges have different population size
"""

import random
import dendropy# must be Dendropy 4.2.0
from dendropy.simulate import treesim
from dendropy.model import reconcile
from dendropy.interop import seqgen
from dendropy.datamodel.charmatrixmodel import CharacterMatrix

#taxa = dendropy.TaxonNamespace(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k'])
#t = treesim.birth_death_tree(0.8, 0.1, taxon_namespace=taxa)

sp_tree_str = "[&R] (Grbr1013:6,(((Pubo1013:1,Tear12:1):2,(Maco1013:1,Pttu21:1):2):2,((((noco:1,Opbr17:1):1,MJM2911:2):1,(Sasa26:1,Tuco07:1):2):1,Tali11:4):1):1);"
#sp_tree_str = "[&R] (Grbr1013,(((Pubo1013,Tear12),(Maco1013,Pttu21)),((((noco,Opbr17),MJM2911),(Sasa26,Tuco07)),Tali11)));"
taxa = dendropy.TaxonNamespace()
t = dendropy.Tree.get(data=sp_tree_str, schema="newick", taxon_namespace=taxa)
#print(t.as_ascii_plot())
print(t.as_ascii_plot(plot_metric='length'))

for node in t.preorder_node_iter():
	#if node.parent_node is None or node.is_leaf():
	if node.is_leaf():
		node.edge.pop_size = 10000
	else:
		node.edge.pop_size = 10000

genes_to_species = dendropy.TaxonNamespaceMapping.create_contained_taxon_mapping(
        containing_taxon_namespace=taxa,
        num_contained=1)

#generate multispecies coalescent trees
gene_outfile = open("outgenetrees.tre","w")
for i in range(10):
    gene_tree = treesim.contained_coalescent_tree(containing_tree=t,
    gene_to_containing_taxon_map=genes_to_species)
    print(gene_tree.as_ascii_plot(plot_metric='length'))
    #print(t.as_ascii_plot(plot_metric='length'))
    print(gene_tree.as_string(schema='newick'))
    gene_outfile.write(gene_tree.as_string(schema='newick'))
gene_outfile.close() # must be closed and then reopen, or it cannot read correctly

#use seqgen to generate one alignment per tree
# need to install seq-gen first using: sudo apt-get install seq-gen
# check out the seq-gen help for argument, as well as seqgen.py within dendropy
# dendropy is just a wrapper for seq-gen, it's not a sequence generator

gene_trees = dendropy.TreeList.get(path="outgenetrees.tre",schema="newick")

s = seqgen.SeqGen()

# instruct Seq-Gen to scale branch lengths by factor of 0.01
# note that this does not modify the input trees
s.scale_branch_lens = 0.01

# more complex model
s.char_model = seqgen.SeqGen.GTR
s.gamma_shape = 0.5
s.seq_len = 1500
s.state_freqs = [0.28, 0.23, 0.25, 0.24]
s.general_rates = [1.0, 5.0, 1.0, 1.0, 5.0, 1.0]
d1 = s.generate(gene_trees)
print(len(d1.char_matrices))
concatematrix = CharacterMatrix.concatenate(d1.char_matrices)
concateout = open("concatenate.phy","w")
concateout.write(concatematrix.as_string("phylip"))
"""for i in range(len(d1.char_matrices)):
	matrixout = open("matrix_"+str(i)+".phy","w")
	matrixout.write(d1.char_matrices[i].as_string("phylip"))
	matrixout.close()"""


"""
t = treesim.birth_death_tree(birth_rate=1.0, death_rate=0.5, ntax=10)# if the death_rate too high, use repeat_until_success=True argument.
#t = treesim.birth_death_tree(birth_rate=1.0, death_rate=0.5,birth_rate_sd=0.1,death_rate_sd=0.1, max_time=6.0)
#t.print_plot()

def generate_list(birth_rates, death_rates):# birth_rates and death_rates are list of rates
    assert len(birth_rates) == len(death_rates)
    tree = dendropy.Tree()
    for i, br in enumerate(birth_rates):
        tree = treesim.birth_death_tree(br,
                                   death_rates[i],
                                   max_time=random.randint(1,8),
                                   tree=tree,
                                   assign_taxa=False,
                                   repeat_until_success=True)
        print(tree.as_string(schema='newick'))
    tree.randomly_assign_taxa(create_required_taxa=True)
    return tree

def generate_normal(mean, sd, num_periods):
    tree = dendropy.Tree()
    for i in range(num_periods):
        tree = treesim.birth_death_tree(birth_rate=random.gauss(mean, sd),
                                   death_rate=random.gauss(mean, sd),
                                   max_time=random.randint(1,5),
                                   tree=tree,
                                   assign_taxa=False,
                                   repeat_until_success=True)
    tree.randomly_assign_taxa(create_required_taxa=True)
    return tree

tree = generate_normal(0.1, 0.01, 100)
print(tree.as_string(schema='newick'))


tree = generate_list([0.1, 0.6, 0.1], [0.1, 0.6, 0.1])
print(tree.as_string(schema='newick'))

# build a pure Neutral coalescent trees
taxa = dendropy.TaxonNamespace(["z1", "z2", "z3", "z4", "z5", "z6", "z7", "z8"])
tree = treesim.pure_kingman_tree(
        taxon_namespace=taxa,
        pop_size=10000) #diploid population of size N, pop_size should be N*2, edge lengths will be in generation units.
print(tree.as_string(schema="newick"))
print(tree.as_ascii_plot())

#Simulating Under Different Phylogeographic History Scenarios, this may generate multispceis coalescent tree
t = reconcile.ContainingTree(t,
            contained_taxon_namespace=genes_to_species.domain_taxon_namespace,
            contained_to_containing_taxon_map=genes_to_species)

for rep in range(10):
    gene_tree = t.simulate_contained_kingman()
    print(gene_tree.as_ascii_plot(plot_metric='length'))
    print(gene_tree.as_string(schema='newick'))

"""
