#! /usr/bin/env python

import sys,os
#import collections
#sorting a dictionary by keys: od = collections.OrderedDict(sorted(d.items()))

	
if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python R_script_for_ks.py DIR taxon_name_table file_end"
		sys.exit(0)
	
	DIR, taxon_table,file_end = sys.argv[1:]
	DIR = os.path.abspath(DIR) + "/"
	
	DICT = {} # key is taxon, value is long species name
	with open(taxon_table, "rU") as infile:
		for line in infile:
			spls = line.strip().split("\t")
			if len(spls) > 1:
				DICT[spls[0]] = spls[1]
	
	#out = ""
	#print DICT
	Rscript = {}
	outfile = open("R_scripts_ks_plots"+file_end+".r","a")
	outfile.write("par(mfrow=c(4,3))\n\n")
	for name in os.listdir(DIR):
		#if name.endswith(".ng") or name.endswith(".yn"):
		out = ""
		if name.endswith(file_end):
			taxon = name.split(".")[0]
			species = DICT[taxon]
			#print species
			#out += "pdf=pdf(file='"+name+".pdf',width=7,height=7)\n"
			out += "a<-read.table('"+name+"')\n"
			out += "hist(a[,1],breaks=60,col='grey',xlab='Ks',ylab='Paralogs',main='"+DICT[taxon]+"')\n\n"
			#out += "axis(1,pos=0)\naxis(2,pos=0)\n\n"
			#out += "axis(1,pos=0)\naxis(2,pos=0)\ndev.off()\n\n"
			Rscript[species] = out
	for key in sorted(Rscript):
		outfile.write(Rscript[key])		
	#with open("R_scripts_ks_plots"+file_end,"w") as outfile: outfile.write(out)
	outfile.close()

