"""
fix CDS or Pep names of Arabidopsis to taxonID@seqID
"""

import os,sys

#seq names look like
#>AT3G05780.1 pep chromosome:TAIR10:3:1714941:1719608:-1 gene:AT3G05780...

if __name__ =="__main__":
	if len(sys.argv) != 3:
		print "usage: python fix_Atha_names.py inDIR filename"
		sys.exit()
	
	inDIR = sys.argv[1]+"/"
	inname = sys.argv[2]
	taxonID = inname.split("_")[0]
	type = inname.split(".")[-3]#may change accordingly
	outname = inname.split(".")[0]+"."+type+".rename.fa"
	infile = open(inDIR+inname,"rU")
	outfile = open(inDIR+outname,"w")
	for line in infile:
		if line[0] == ">":
			newid = line.split(" ")[0]
			newid = newid[1:]
			outfile.write(">"+taxonID+"@"+newid+"\n")
		else: outfile.write(line)
	outfile.close
	infile.close()
	
