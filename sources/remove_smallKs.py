#! /usr/bin/env python

import os,sys

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python remove_smallKs.py DIR OutDIR numlimit"
		print "make an out directory first"
		sys.exit(0)
	
	DIR,outDIR,numlim= sys.argv[1:]
	DIR = os.path.abspath(DIR)+"/"
	outDIR = os.path.abspath(outDIR)+"/"
	for f in os.listdir(DIR):
		if not f.endswith(".ng"): continue
		infile = open(DIR+f,"rU")
		outfile = open(outDIR+f,"w")
		for l in infile:
			num = float(l.strip())
			if num > float(numlim):
				outfile.write(l)
		infile.close()
		outfile.close()
		
		