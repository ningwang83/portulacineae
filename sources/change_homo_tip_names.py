import sys,os
import tree_reader


def change_tip_name(treefile):
    inf = open(treefile,"r")
    oneline = inf.readline().strip()
    tree = tree_reader.read_tree_string(oneline)
    for i in tree.iternodes():
        if i.istip:
            i.label = i.label.split("@")[0]
    inf.close()
    return tree.get_newick_repr(True)+";"
    
if __name__ == "__main__":
    if len(sys.argv) != 4:
        print "python SHnote_move.py inDIR outDIR file_end"
        sys.exit(0)
    inDIR,outDIR,file_end = sys.argv[1:]
    inDIR = os.path.abspath(inDIR)+"/"
    outDIR = os.path.abspath(outDIR)+"/"

    for tre in os.listdir(inDIR):
        if not tre.endswith(file_end): continue
        outname = outDIR+tre+".edittip.tre"
        edittree = change_tip_name(tre)
        with open(outname,"a") as outfile:
            outfile.write(edittree+"\n")
        outfile.close()
