"""
Read the concatenated fasta file either with or without ends cut
write individual fasta files for each cluster
"""

import sys,os
#from Bio import SeqIO
from cluster_totree import read_fasta

# limit the ingroup taxa number
Cactaceae = ["Lely", "Lebl", "JLOV", "Pegr", "CPKP", "Ecpe", "Noco", "MJM2911", "MJM2938", "Tali11", "Gymi06", "Tuco07", "Asmy08", "Ecau09", "Tear12", "Maau13", "Stco14", "Asas15", "Opbr17", "Ceyu18", "Arre20", "Pttu21", "Paga22", "Mapo24", "Sasa26", "Coma28", "Pecu29", "Erwa30", "Legu31", "Fela32", "Grbr1013", "Code1013", "Maco1013", "Pubo1013"]

Outgroup = ["AniSFB", "LimaeSFB", "DBGStha", "HZTS", "BJKT", "SFB27", "Beta"]

Montiaceae = ["CigrSFB", "CagrSFB", "MJM3167", "MJM3168", "MJM3165", "MJM2214", "MJM1156", "TRS2027", "MJM3142"]

Portulacaceae = ["LDEL", "LLQV", "CPLT", "UQCB", "EZGR", "IWIS", "GCYL", "KDCH"]

Talinaceae = ["Tafr", "MJM1789", "Tapo"]

Molluginaceae = ["SuecaSFB", "PhaexSFB", "RNBN", "SCAO", "HURS", "NXTS", "GliloSFB"]

Didiereaceae = ["DBG19565744023G", "DBG294800240202G", "MJM2944", "DBG198703010201Z", "DBG19880583021G", "DBG19740229011", "AlmarSFB", "CerpySFB", "DBGDima"]

Anacampserotaceae = ["MJM2441", "GrakuSFB"] 

Basellaceae = ["AncoSFB"]

def mcl_to_fasta(all_fasta,mcl_outfile,minimal_taxa,outdir):
	print "Reading mcl output file"
	SeqDict = read_fasta(all_fasta)
	count = 0
	outdir = os.path.abspath(outdir)+"/"
	with open(mcl_outfile,"rU") as infile:
		for line in infile:
			Cacnum = 0
			outnum = 0
			Montnum = 0
			Portnum = 0
			Talnum = 0
			Mollnum = 0
			Didinum = 0
			Ananum = 0
			Basenum = 0
			if len(line) < 3: continue #ignore empty lines
			spls = line.strip().split('\t')
			unique = set(i.split("@")[0] for i in spls)
			for Sampleid in unique:
				if Sampleid in Cactaceae:
					Cacnum += 1
				elif Sampleid in Outgroup:
					outnum += 1
				elif Sampleid in Montiaceae:
					Montnum += 1
				elif Sampleid in Portulacaceae:
					Portnum += 1
				elif Sampleid in Talinaceae:
					Talnum += 1
				elif Sampleid in Molluginaceae:
					Mollnum += 1
				elif Sampleid in Didiereaceae:
					Didinum += 1
				elif Sampleid in Anacampserotaceae:
					Ananum += 1
				elif Sampleid in Basellaceae:
					Basenum += 1
			#with open(outdir+"occupancy_graph.txt","a") as outfile_graph:
			#	outfile_graph.write(str(Cacnum)+"\t"+str(outnum)+"\t"+str(Montnum)+"\t"+str(Portnum)+"\t"+str(Talnum)+"\t"+str(Mollnum)+"\t"+str(Didinum)+"\t"+str(Ananum)+"\t"+str(Basenum)+"\n")
			if Cacnum >= minimal_taxa and outnum >= 1 and Montnum >= 7 and Portnum >= 2 and Talnum >= 2 and Mollnum >= 2 and Didinum >= 7 and Ananum >= 1:
			#if Cacnum >= minimal_taxa and outnum == 7: # test the behave of outgroup
				count += 1
				clusterID = str(count)
				outname = outdir+"cluster"+clusterID+".fa"
				print outname
				outfile = open(outname,"a")
				for seqID in spls:
					outfile.write(">"+seqID+"\n"+SeqDict[seqID]+"\n")					
	print count,"clusters with at least",minimal_taxa,"taxa read"
					
	

if __name__ =="__main__":
	if len(sys.argv) != 5:
		print "usage: python mclhitcluster2fasta.py all_fasta mcl_outfile minimal_taxa outDIR"
		sys.exit()
	
	mcl_to_fasta(all_fasta=sys.argv[1],mcl_outfile=sys.argv[2],\
				 minimal_taxa=int(sys.argv[3]),outdir=sys.argv[4])
	
