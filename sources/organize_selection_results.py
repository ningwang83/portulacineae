#! /usr/bin/env python 

""" use this script to extract selection results from homolog """

import os,sys
import tree_reader

Clade_dict = {}

Clade_dict["Cac35"] = ["Lely", "Lebl", "JLOV", "Pegr", "CPKP", "Ecpe", "Noco", "MJM2911", "MJM2938", "Tali11", "Gymi06", "Tuco07", "Asmy08", "Ecau09", "Tear12", "Maau13", "Stco14", "Asas15", "Opbr17", "Ceyu18", "Arre20", "Pttu21", "Paga22", "Mapo24", "Sasa26", "Coma28", "Pecu29", "Erwa30", "Legu31", "Fela32", "Grbr1013", "Code1013", "Maco1013", "Pubo1013"]
Clade_dict["Cac39"] = ["Pubo1013", "Tear12", "Maco1013", "Pttu21", "Tali11", "Sasa26", "Tuco07", "MJM2911", "Noco", "Opbr17", "Grbr1013"]
Clade_dict["Cac50"] = ["Gymi06", "Asmy08", "Ecau09", "Maau13", "Stco14", "Asas15", "Ceyu18", "Arre20", "Paga22", "Coma28", "Pecu29", "Erwa30", "Fela32", "Code1013", "CPKP", "Ecpe", "MJM2938"]

Clade_dict["Montia14"] = ["CigrSFB", "CagrSFB", "MJM3167", "MJM3168", "MJM3165", "MJM2214", "MJM1156", "TRS2027", "MJM3142"]
Clade_dict["Montia15"] = ["CigrSFB", "CagrSFB", "MJM3167", "MJM3168", "MJM3165", "MJM1156", "TRS2027", "MJM3142"]


Clade_dict["Portulacaceae71"] = ["LDEL", "LLQV", "CPLT", "UQCB", "EZGR", "IWIS", "GCYL", "KDCH"]

Clade_dict["Talinaceae"] = ["Tafr", "MJM1789", "Tapo"]

Clade_dict["Mollug7"] = ["SuecaSFB", "PhaexSFB", "RNBN", "SCAO", "HURS", "NXTS", "GliloSFB"]

Clade_dict["Didier25"] = ["DBG19565744023G", "DBG294800240202G", "MJM2944", "DBG198703010201Z", "DBG19880583021G", "DBG19740229011", "AlmarSFB", "CerpySFB", "DBGDima"]
Clade_dict["Didier26"] = ["CerpySFB", "DBG19880583021G"]
Clade_dict["Didier27"] = ["DBG19565744023G", "DBG294800240202G", "MJM2944", "DBG198703010201Z", "DBG19740229011", "AlmarSFB", "DBGDima"]

Clade_dict["Anacampserotaceae"] = ["MJM2441", "GrakuSFB","Anfi"] 

Clade_dict["Basellaceae"] = ["AncoSFB","CTYH"]


def get_name(label):
	return label.split("@")[0]
	
def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]

def get_front_names(node): #may include duplicates
	labels = get_front_labels(node)
	return [get_name(i) for i in labels]


def dif_list(treenode_leaves,clade_list):
	dif = []
	for i in treenode_leaves:
		if not i in clade_list:
			dif.append(i)
	return len(dif)

def read_p(selection_file):
	infile = open(selection_file,"rU")
	result_dic = {}
	First = True
	for l in infile:
		if First:
			First = False
			continue
		Node = l.strip().split(",")[0]
		Pvalue = l.strip().split(",")[-2]
		if float(Pvalue) < 0.05:
			result_dic[Node] = str(round(float(Pvalue),3))
	return result_dic


def read_dNdS(selection_file):
	infile = open(selection_file,"rU")
	result_dic = {}
	First = True
	for l in infile:
		if First:
			First = False
			continue
		Node = l.strip().split(",")[0]
		dNdSvalue = l.strip().split(",")[1]
		if float(dNdSvalue) < 0.1:
			result_dic[Node] = str(round(float(dNdSvalue),4))
	return result_dic


def search_node(tree,clade_list):
	target_node_dic = {}
	for n in tree.iternodes("preorder"):
		if n.istip: continue
		front_names = set(get_front_names(n))
		Numdif = dif_list(front_names,clade_list)
		if Numdif != 0: continue
		else:
			Numtips = len(front_names)
			target_node_dic[n.label] = Numtips
	return target_node_dic


if __name__ =="__main__":
	if len(sys.argv) != 3:
		print "usage: python "+sys.argv[0]+" inDIR file_end"
		print "inDIR contain labeled treefile end with .edit and selection result file"
		print "file_end with .edit, selection results name e.g., cluster731"
		sys.exit()	
	inDIR,file_end = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	outresult = inDIR+"results.out"
	#outresult = inDIR+"dNdS.out"
	outfile = open(outresult,"w")
	outfile.write("clusterID\tTotalselect\tAna\tBese\tCac35\tCac39\tCac50\tDid25\tDid26\tDid27\tMollug7\tMont14\tMont15\tPortu71\tTali\n")
	#outfile.write("clusterID\tTotaldNdS0.1\tAna\tBese\tCac35\tCac39\tCac50\tDid25\tDid26\tDid27\tMollug7\tMont14\tMont15\tPortu71\tTali\n")
	for f in os.listdir(inDIR):
		if not f.endswith(file_end): continue
		clusterID = f.split("_")[0]
		outfile.write(clusterID+"\t")
		result_file = inDIR+clusterID
		# read result_file, collect selected nodes
		result_dic = read_p(result_file)
		#result_dic = read_dNdS(result_file)
		outfile.write(str(len(result_dic.keys()))+"\t")
		# read tree file
		infile = open(inDIR+f,"rU")
		treeline = infile.readline().strip()
		tree = tree_reader.read_tree_string(treeline)
		# collect target_node_dic
		for key in sorted(Clade_dict.keys()):
			print key
			Target_dic = search_node(tree,Clade_dict[key])
			count = 0
			for k in Target_dic.keys():
				if k in result_dic.keys():
					count += 1
			outfile.write(str(count)+"\t")
		outfile.write("\n")
		# wirte an edit tree with selected nodes labeled by p value
		for n in tree.iternodes():
			if n.istip: continue
			if n.label in result_dic.keys():
				n.label = result_dic[n.label]
			else:
				n.label = None
		edittree = tree.get_newick_repr(True)+";"
		with open(inDIR+clusterID+".tre","w") as outtree:
			outtree.write(edittree+"\n")
		outtree.close()
		infile.close()
	outfile.close()
		

	
			
	
		
