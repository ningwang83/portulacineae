#! /usr/bin/env python

import os,sys

WORKDIR = sys.argv[1]
WORKDIR = os.path.abspath(WORKDIR)+"/"

for i in os.listdir(WORKDIR):
	clusterID = i.split(".")[-1]
	if i.startswith("RAxML_fastTreeSH_Support"):
		os.rename(i,WORKDIR+clusterID+".SHsupport")
	elif i.startswith("RAxML_fastTree"):
		os.rename(i,WORKDIR+clusterID+".fastTree")


