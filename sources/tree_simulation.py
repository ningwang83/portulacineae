#! /usr/bin/env python

"""
Simulating gene trees with a species tree that different edges have different population size
"""

import os,sys
import random
import dendropy# must be Dendropy 4.2.0
from dendropy.simulate import treesim
from dendropy.model import reconcile
from dendropy.interop import seqgen
from dendropy.datamodel.charmatrixmodel import CharacterMatrix
import glob
import subprocess


def raxml(DIR,cleaned,num_cores,model):
	assert cleaned.endswith(".phy"),\
		"raxml infile "+cleaned+" not ends with .phy"
	ID = cleaned.split(".")[0]
	tree = os.path.join(DIR,ID+".raxml_bs.tre")
	raw_tree = "RAxML_bipartitions."+cleaned
	if not os.path.exists(tree) and not os.path.exists(raw_tree):
		inphy = os.path.join(DIR,cleaned)
		cmd = ["raxml","-f","a","-T",str(num_cores),"-p",str(random.randint(1,1000000)),"-s",\
			inphy,"-n",cleaned,"-m","GTRCAT","-q",model,"-x",str(random.randint(1,1000000)),\
			"-#","100"]#bootstrap for 100 iterations
		print " ".join(cmd)
		p = subprocess.Popen(cmd,stdout=subprocess.PIPE)
		out = p.communicate()
		assert p.returncode == 0,"Error raxml"+out[0]
		
	os.rename(raw_tree,tree)
	os.rename("RAxML_bootstrap."+cleaned, os.path.join(DIR,ID+".raxml_bs.trees"))
	for file in glob.glob("RAxML_*"):
		os.remove(file)
		
	try:
		os.remove(DIR+cleaned+".reduced")
	except: pass # no need to worry about extra intermediate files

	return tree



path = os.getcwd()
"""taxa = dendropy.TaxonNamespace(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k'])
t = treesim.birth_death_tree(0.8, 0.1, taxon_namespace=taxa)
print(t.as_ascii_plot(plot_metric='length'))
species_tree = os.path.join(path,"species.tre")
speciesfile = open(species_tree,"w")
speciesfile.write(t.as_string(schema="newick"))
speciesfile.close()"""

t = dendropy.Tree.get_from_path('species.tre','newick')
print(t.as_ascii_plot(plot_metric='length'))

argument = sys.argv[1]
popsize = open(path+"/popsize","w")

#small pop for tip and large pop for ancestor
for i in range(50):
	if argument == "smalltip":
		tippop = random.randint(10,10000)
		interpop = tippop*random.randint(1000,10000)
	elif argument == "largetip":
		interpop = random.randint(10,10000)
		tippop = interpop*random.randint(1000,10000)
	elif argument == "consistent":
		tippop = random.randint(10000,100000)
		interpop = tippop
	print i, tippop, interpop
	popsize.write(str(i)+"\t"+str(tippop)+"\t"+str(interpop)+"\n")
	for node in t.preorder_node_iter():
		if node.is_leaf() or node.parent_node is None:
			node.edge.pop_size = tippop
		else:
			node.edge.pop_size = interpop

	genes_to_species = dendropy.TaxonNamespaceMapping.create_contained_taxon_mapping(
						containing_taxon_namespace=t.taxon_namespace,
						num_contained=1)

	#generate multispecies coalescent trees, 50 genes per file
	gene_name = "outgenetrees_simulates_"+str(i)+".tre"
	gene_outfile = open(os.path.join(path,gene_name),"w")
	for j in range(50):
		gene_tree = treesim.contained_coalescent_tree(containing_tree=t,
		gene_to_containing_taxon_map=genes_to_species)
		#print(gene_tree.as_ascii_plot(plot_metric='length'))
		#print(gene_tree.as_string(schema='newick'))
		gene_outfile.write(gene_tree.as_string(schema='newick'))
	gene_outfile.close() 
	# generate sequences for gene trees, 1000bp each default
	gene_trees = dendropy.TreeList.get(path=os.path.join(path,gene_name),schema="newick")
	s = seqgen.SeqGen()
	s.scale_branch_lens = 0.01
	s.char_model = seqgen.SeqGen.GTR
	s.gamma_shape = random.uniform(0.4,0.7)
	s.state_freqs = [0.28, 0.23, 0.25, 0.24]
	s.general_rates = [1.0, 5.0, 1.0, 1.0, 5.0, 1.0]
	d1 = s.generate(gene_trees)
	print(len(d1.char_matrices))
	concatematrix = CharacterMatrix.concatenate(d1.char_matrices)
	temp = "concatenate_"+str(i)+".temp"
	with open(os.path.join(path,temp),"w") as outfile:
		outfile.write(concatematrix.as_string("phylip"))
	outfile.close()
	concate_name = "concatenate_"+str(i)+".phy"
	concatefile = open(os.path.join(path,concate_name),"w")
	with open(os.path.join(path,temp),"rU") as infile:
		First = True
		for line in infile:
			line = line.strip()
			if First:
				concatefile.write(line+"\n")
				First = False
			else:
				IDname = line.split("\t")[0].split(" ")[0]
				seq = line.split("\t")[-1].split(" ")[-1]
				concatefile.write(IDname+"\t"+seq+"\n")
	infile.close()
	concatefile.close()
	os.remove(os.path.join(path,temp))
	
	# run partitioned raxml
	raxml(path,concate_name,9,"partition.model")
popsize.close()
