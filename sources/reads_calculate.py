#! /usr/bin/env python

import os,sys
from subprocess import call
from assemble import fq_dictionary

if __name__ =="__main__":
	if len(sys.argv) != 3:
		print "usage: python reads_calculate.py DIRlist file_end"
		print "DIRlist separated by ','"
		sys.exit()
	
	DIRs,file_end = sys.argv[1:3]
	# Collect all the fastq.gz files in a dictionary
	Dic_fastq = fq_dictionary(DIRs.split(","),\
			read1_identifier="R1",\
			read2_identifier="R2",\
			infile_end=file_end)
	for key in Dic_fastq:
		print key
		cmd = "zcat "+key+" | wc -l" # this is the line number of each file, which divided by 4 can result the number of sequences. 
		call(cmd,shell=True)
		"""linenumber=0
		infile = open(key,"rU")
		for line in infile:
			linenumber += 1
		reads = linenumber/4
		print key, reads
		inflie.close()"""



