#! /usr/bin/env python

"""
the out file is the compare node duplication clusters outfile, output from compare_clade_dup.py
.out file could be also results from get_dup_clade.py
convert the clusterID to AthaID so can be used in the Panther Go analyses
"""

import os,sys

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python "+sys.argv[0]+" inDIR source_file file_end"
		print "file_end with .out, source_file is the blastn file"
		sys.exit(0)
	
	inDIR,source_file,file_end = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	source = os.path.join(os.getcwd(),source_file)
	
	#read in source file and built cluster to AthaID dictionary
	sourcefile = open(source,"rU")
	clusterIDdic = {} # key is the clusterID, and value is the Athaid
	for l in sourcefile:
		clusterID = l.strip().split("\t")[0].split(".")[0]
		Athaid = l.strip().split("\t")[2]
		Athaid = Athaid.split("@")[1]
		if not clusterID in clusterIDdic.keys():
			clusterIDdic[clusterID] = Athaid
		else: continue #only extract the top hit AthaID
	sourcefile.close()

	for i in os.listdir(inDIR):
		if not i.endswith(file_end): continue
		fileid = i.split('.')[0]
		outname = inDIR+fileid+".AthaID"
		outfile = open(outname,"a")
		infile = open(inDIR+i,"rU")
		for line in infile:
			#if len(line.strip()) < 1: continue
			lineID = line.strip().split("\t")[0]
			clusterstr = line.strip().split("\t")[-1]
			clusterlist = clusterstr.split(",")
			AthaIDlist = []
			print lineID
			for c in clusterlist:
				if not c in clusterIDdic.keys(): 
					print c
					continue
				AthaIDlist.append(clusterIDdic[c])
			outfile.write(lineID+"\t"+",".join(set(AthaIDlist))+"\n")
		infile.close()
		outfile.close()	

