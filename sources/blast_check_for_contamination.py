#! /usr/bin/env python

""" use .pep.fa/.cds.fa file to make blast database and use rbcL and matK protein sequences or cytb DNA data to check contamination and the export a .rawblast file with outfmt 6 that containing the taxonID@seqID in the second column (line.strip().split("\t")[1]), pull that out and get the sequences from the original fasta file, output into a file, which will be used later on ncbi looking for the nearest balst hit."""

import os,sys
from subprocess import call
from cluster_totree import read_fasta


if len(sys.argv) != 7:
	print "python blast_check_for_contamination.py DIR file_ending query evalue maxseq datatype"
	print "evalue: e.g. 1e-3,10, maxseq: the max number of target sequences. query: rbcL, matK or cytb"
	print "file_ending: either .pep.fa or .cds.fa, which are the output after fix_check_name"
	print "datatype:must be exactly nucl or prot"
	print "Usage: check contamination using query gene, output the genes for ncbi blast"
	sys.exit(0)
	# minimum number of sequences should be bigger than the minimum taxa in the cluster files
DIR,file_ending,query,evalue,maxseq,datatype = sys.argv[1:7]

DIR = os.path.abspath(DIR)+"/"

if query == "rbcL": querydata = '/home/ningwum/Documents/RNA_seq_UM/Cactaceae/contamination_test/rbcl_cactaceae'
elif query == "matK": querydata = '/home/ningwum/Documents/RNA_seq_UM/Cactaceae/contamination_test/matK_cactaceae'
else: querydata = '/home/ningwum/Documents/Transcriptome_bird/cytb'

BLast = "blastp " if datatype == "prot" else "blastn "

for i in os.listdir(DIR):
	if not i.endswith(file_ending): continue
	taxonID = i.split(".")[0]
	INname = DIR+taxonID+".rawblast."+query
	OUTname = DIR+taxonID+".targetseq."+query
	dbname = DIR+i+".phr" # indicate the makedatadb have been finished
	
	if not os.path.exists(dbname):
		makedatabase = "makeblastdb -in "+DIR+i+" -out "+DIR+i+" -dbtype "+datatype
		call(makedatabase,shell=True)
	
	blastquery = BLast+"-db "+DIR+i+" -query "+querydata+" -evalue "+evalue+" -num_threads 1 -max_target_seqs "+maxseq \
	+" -out "+INname+" -outfmt 6"	
	if not os.path.exists(INname):
		call(blastquery,shell=True)
	
	SeqDic = read_fasta(DIR+i)
	if not os.path.exists(OUTname):
		infile = open(INname,"rU")
		outfile = open(OUTname,"a")
	
		listID = []
		for line in infile:
			ID = line.strip().split('\t')[1]
			if ID in listID: continue
			listID.append(ID)
			outfile.write(">"+ID+"\n"+SeqDic[ID]+"\n")
	else: continue
	




