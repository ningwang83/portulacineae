#! /usr/bin/env python

""" extract few species sequences from the alignments to conduct the dfoil test"""

import os,sys

#run1
"""taxa_Cac = ["Ceyu18","Opbr17","Lely","Pegr","PhaexSFB"] # Cactaceae
taxa_BD = ["Ceyu18","GrakuSFB","DBG19880583021G","AncoSFB","PhaexSFB"] # Basellacea+Didiereaceae
taxa_PCA = ["Ceyu18","Opbr17","LDEL","GrakuSFB","PhaexSFB"] #PCA"""

#run2
taxa_Cac = ["Ceyu18","Maau13","Lely","Pegr","PhaexSFB"] # Cactaceae
taxa_BD = ["Ceyu18","Maau13","DBG19880583021G","AncoSFB","PhaexSFB"] # Basellacea+Didiereaceae
taxa_PCA = ["Ceyu18","Maau13","LDEL","GrakuSFB","PhaexSFB"] #PCA


def read_fasta(fastafile):
	infile = open(fastafile,"r")
	seqDict = {} #list of sequence objects
	templab = ""
	tempseq = ""
	First = True
	for i in infile:
		if i[0] == ">":
			if First: First = False
			else:
				seqDict[templab] = tempseq # include an empty key and value.
			templab = i.strip()[1:]
			tempseq = ""
		else:
			tempseq = tempseq + i.strip()
	infile.close()
	seqDict[templab] = tempseq
	return seqDict

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "python "+sys.argv[0]+" fasDIR outDIR file_end option(BD,Cac,PCA)"
		sys.exit(0)
	inDIR,outDIR,file_end,option = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	if not os.path.exists(outDIR):	
		os.mkdir(outDIR)
	outDIR = os.path.abspath(outDIR)+"/"
	
	if option == "Cac":
		taxa_list = taxa_Cac
	elif option == "BD":
		taxa_list = taxa_BD
	else:
		taxa_list = taxa_PCA
	
	for aln in os.listdir(inDIR):
		if not aln.endswith(file_end): continue
		cluster = aln.split("_")[0]
		seqDic = read_fasta(inDIR+aln)
		outseq = []
		for k in seqDic.keys():
			if k in taxa_list:
				outseq.append(k)
		if len(outseq) ==  len(taxa_list):
			outfile = open(outDIR+cluster,"w")
			for i in taxa_list:
				outfile.write(">"+i+"\n"+seqDic[i]+"\n")
			outfile.close()

				

