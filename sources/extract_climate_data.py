#! /usr/bin/env python

import os,sys
import tree_reader
from node import Node
import tree_utilities

#climate data tree is different from the diversification tree, as including less taxa

MRCA_Dict = {}

MRCA_Dict["Molluginaceae"] = ["Molluginaceae_Mollugo_verticillata", "Molluginaceae_Pharnaceum_elongatum"]
MRCA_Dict["Portulacineae"] = ["Didiereaceae_Ceraria_fruticulosa", "Montiaceae_Phemeranthus_brevifolius"]

#MRCA_Dict["Montiaceae_17"] = ["Montiaceae_Montiopsis_andicola", "Montiaceae_Montiopsis_umbellata"] # climate data
#MRCA_Dict["Montiaceae_17sis"] = ["Montiaceae_Cistanthe_grandiflora", "Montiaceae_Cistanthe_arenaria"] # climate data, no Node 17

MRCA_Dict["Montiaceae_19"] = ["Montiaceae_Hectorella_caespitosa", "Montiaceae_Montia_chamissoi"]
MRCA_Dict["Montiaceae_Calandrinia"] = ["Montiaceae_Calandrinia_compacta", "Montiaceae_Calandrinia_affinis"]

MRCA_Dict["Cactaceae"] = ["Cactaceae_Pereskia_aureiflora", "Cactaceae_Cereus_aethiops"]
MRCA_Dict["Anacam"] = ["Anacampserotaceae_Talinopsis_frutescens", "Anacampserotaceae_Anacampseros_filamentosa"]
MRCA_Dict["Portulacaceae"] = ["Portulacaceae_Portulaca_quadrifida", "Portulacaceae_Portulaca_grandiflora"]
MRCA_Dict["Didi27"] = ["Didiereaceae_Alluaudia_comosa", "Didiereaceae_Didierea_trollii"]
MRCA_Dict["Didi26"] = ["Didiereaceae_Portulacaria_afra", "Didiereaceae_Ceraria_fruticulosa"]

# Taxa on diversification tree, not in the climate data tree
#MRCA_Dict["Montiaceae_17"] = ["Montiaceae_Calyptriquium_umbellatum", "Montiaceae_Calyptriquium_pygmaeum"] # diversification tree
#MRCA_Dict["Montiaceae_17near"] = ["Montiaceae_Cistanthe_calycina","Montiaceae_Cistanthe_densiflora"] # diversification tree
#MRCA_Dict["Montiaceae_Calandrinia"] = ["Montiaceae_Calandrinia_compacta", "Montiaceae_Calandrinia_caespitosa_x_Calandrinia_compact"]


if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python extract_climate_data.py inDIR file_end outname"
		sys.exit(0)
	
	inDIR,file_end,outname = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	curDIR = os.getcwd()+"/"
	
	# read pruned trees and write node.note to files
	outfile = open(curDIR+outname,"w")
	keys = sorted(MRCA_Dict.keys())
	outfile.write("tree\t"+"\t\t".join(keys)+"\n")
	for tre in os.listdir(inDIR):
		if not tre.endswith(file_end): continue
		outfile.write(tre+"\t")
		infile = open(inDIR+tre,"rU")
		curroot = tree_reader.read_tree_string(infile.readline().strip())
		for i in keys:
			#print MRCA_Dict[i]
			mrca = tree_utilities.get_mrca_wnms(MRCA_Dict[i],curroot)
			note = mrca.note
			if mrca.parent != None:
				parent = mrca.parent
				pnote = parent.note
			else:
				pnote = "NA"
			outfile.write(note+"\t"+pnote+"\t")
		outfile.write("\n")
	outfile.close()



