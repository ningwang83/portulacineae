#! /usr/bin/env python

""" use this script to prune trees to leave only taxa wanted. """

import os,sys
from node import Node
import tree_reader

taxa_list = ["Basellaceae", "Cactaceae", "Didiereaceae", "Limeaceae", "Molluginaceae", "Montiaceae", "Phytolaccaceae", "Talinaceae", "Anacampserotaceae", "Portulacaceae", "Stegnospermataceae"]

def get_front_labels(node):
	"""given a node, return a list of front tip labels"""
	leaves = node.leaves()
	return [i.label for i in leaves]

def overlap(target_list,taxa_list):
	ol = 0
	for i in target_list:
		family = target_list.split("_")[0]
		if family in taxa_list:
			ol += 1
	return ol

def diff(target_list,taxa_list):
	dif = 0
	for i in target_list:
		family = i.split("_")[0]
		if not family in taxa_list:
			#print i
			dif += 1
	return dif
	
	
def reroot(oldroot, newroot):
	v = [] #path to the root
	n = newroot
	while 1:
		v.append(n)
		if not n.parent: break
		n = n.parent
	v.reverse()
	for i, cp in enumerate(v[:-1]):
		node = v[i+1]
		# node is current node; cp is current parent
		#print node.label, cp.label
		cp.remove_child(node)
		node.add_child(cp)
		cp.length = node.length
		cp.label = node.label
	return newroot

def remove_kink(node,curroot):
	#smooth the kink created by prunning
	if node == curroot and len(curroot.children) == 2:
		#move the root away to an adjacent none-tip
		if curroot.children[0].istip: #the other child is not tip
			curroot = reroot(curroot,curroot.children[1])
		else: curroot = reroot(curroot,curroot.children[0])
	# if root is a kink
	if node == curroot and len(curroot.children) == 1:
		kink = node
		curroot = reroot(curroot,curroot.children[0])
		curroot.remove_child(kink)
		node = curroot
		return node, curroot
	#---node---< all nodes should have one child only now
	length = node.length + (node.children[0]).length
	par = node.parent
	kink = node
	node = node.children[0]
	#parent--kink---node<
	par.remove_child(kink)
	par.add_child(node)
	node.length = length
	return node,curroot

"""def remove_kink(node,curroot):
	'''
	smooth the kink created by prunning
	to prevent creating orphaned tips
	after prunning twice at the same node
	refer to pyphlawd prune_branches_from_raxmltre.py for a better function to remove_kink
	'''
	if node == curroot and len(curroot.children) == 2:
		#move the root away to an adjacent none-tip
		if curroot.children[0].istip: #the other child is not tip
			curroot = reroot(curroot,curroot.children[1])
		else: curroot = reroot(curroot,curroot.children[0])
	#---node---< all nodes should have one child only now
	length = node.length + (node.children[0]).length
	par = node.parent
	kink = node
	node = node.children[0]
	#parent--kink---node<
	par.remove_child(kink)
	par.add_child(node)
	node.length = length
	return node,curroot"""

def remove_tips(root,taxa_list):
	for i in root.leaves():
		label = i.label
		family = label.split("_")[0]
		if not family in taxa_list:
			print i.label
			par = i.prune()
	return root


if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python prune_tree.py inDIR input_treefile outtree_name"
		print "make sure the input trees have tip names consistent with the taxa list"
		print "provide the full path of input treefile"
		sys.exit(0)

	inDIR,intree,outtree = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	outfile = open(inDIR+outtree,"w")
	# read the input trees
	infile = open(intree,"rU")
	curroot = tree_reader.read_tree_string(infile.readline().strip())
	# check out if all taxa in the tree
	"""root_taxa = get_front_labels(curroot)
	differents = diff(taxa_list,root_taxa)
	assert differents == 0, "some taxa are not in the tree"
	"""
	# prune tree
	while True and curroot != None:
		print curroot.get_newick_repr()
		totaltaxa = get_front_labels(curroot)
		dif_leave = diff(totaltaxa,taxa_list)
		if dif_leave == 0:
			break
		else: 
			curroot = remove_tips(curroot,taxa_list)
	# remove kinks on the tree!
	while True:
		count = 0
		for i in curroot.iternodes():
			if not i.istip and len(i.children) == 1:
				count += 1
				node,curroot = remove_kink(i,curroot)
				break
		if count == 0:
			break
	newroot = curroot.get_newick_repr(True,True)+";"
	outfile.write(newroot+"\n")
	outfile.close()
	infile.close()
			
