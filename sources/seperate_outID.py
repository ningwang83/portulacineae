#! /usr/bin/env python

import os,sys

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "python "+sys.argv[0]+" infile"
		print "separate outIDs into groups"
		sys.exit(0)

	inputname = sys.argv[1]
	infile = open(inputname,"rU")
	First = True
	for line in infile:
		if line.startswith(">"):
			#print line			
			if First:
				First = False
				name = line.strip()[1:]
				#print name
				outfile = open(name+".outID","w")
			else:
				outfile.close()
				name = line.strip()[1:]
				#print name
				outfile = open(name+".outID","w")							
		else:
			outfile.write(line)
	outfile.close()
