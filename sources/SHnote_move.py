import sys,os
import tree_reader


def move_SHnotes(treefile):
    inf = open(treefile,"r")
    oneline = inf.readline().strip()
    tree = tree_reader.read_tree_string(oneline)
    for i in tree.iternodes():
        if len(i.children) > 0:
            i.label = i.note
    inf.close()
    return tree.get_newick_repr(True)+";"
    
if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "python SHnote_move.py inDIR file_end"
        sys.exit(0)
    inDIR,file_end = sys.argv[1:]
    inDIR = os.path.abspath(inDIR)+"/"

    for tre in os.listdir(inDIR):
        if not tre.endswith(file_end): continue
        outname = inDIR+tre+".edit.tre"
        edittree = move_SHnotes(inDIR+tre)
        with open(outname,"a") as outfile:
            outfile.write(edittree+"\n")
        outfile.close()
