#! /usr/bin/env python
""" use this script to fix and check names from TransDecoder
get together all transcript.Trinity.fasta file from Trinity into one directory and run TransDecoder.py
after that result two files used this script: .transdecoder.pep and .transdecoder.cds files
"""

import os,sys
import re
from re import match

def fixname(inDIR,outDIR):
	inDIR = os.path.abspath(inDIR)+"/"
	#if inDIR[-1] != "/": inDIR = inDIR+"/"
	if outDIR != ".": os.mkdir(outDIR) 
	outDIR = os.path.abspath(outDIR)+"/"
	
	for i in os.listdir(inDIR):
		taxonID = i.split(".")[0]
		if i.endswith("transdecoder.pep"):
			outname = taxonID+".pep.fa"
		elif i.endswith("transdecoder.cds"):
			outname = taxonID+".cds.fa"
		else: continue
		print i,taxonID
		
		infile = open(inDIR+i,"rU")
		outfile = open(outDIR+outname,"w")
		for line in infile:
			if line[0] == ">":
				newid = line.split(" ")[0]
				newid = newid.split(".")[-1]
				outfile.write(">"+taxonID+"@"+newid+"\n")
			else: outfile.write(line)
		outfile.close()
		infile.close()

def checkname(DIR,file_ending):
	DIR = os.path.abspath(DIR)+"/"
	for i in os.listdir(DIR):
		if not i.endswith(file_ending): continue
		taxonID = i.split(".")[0]
		print i,taxonID
		infile = open(DIR+i,"rU")
		seqIDs = []
		for line in infile:
			if line[0] != ">": continue #only look at seq names
			taxon,seqID = line.strip()[1:].split("@")
			assert taxon == taxonID,taxon+" taxon id do not match file name "+taxonID
			assert match("^[a-zA-Z0-9_]+$",seqID),seqID+" contains special characters other than letter, digit and '_'"
			seqIDs.append(seqID)
		repeats = {j:seqIDs.count(j) for j in seqIDs} # make a dictionary, j is the key and number of count is the value
		print len(seqIDs),"sequences checked"
		for j in repeats:
			rep = repeats[j]
			if rep > 1:
				print j,"repeats",rep,"times"
		infile.close()

if __name__ =="__main__":
	if len(sys.argv) != 4:
		print "usage: python fix_check_names.py inDIR outDIR file_ending"
		print "outDIR: the name of output directory; file_ending: .fa"
		sys.exit()
	inDIR,outDIR,file_ending = sys.argv[1:]
	fixname(inDIR,outDIR)
	checkname(outDIR,file_ending)
